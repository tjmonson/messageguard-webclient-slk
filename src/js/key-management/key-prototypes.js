var Errors = require('../common/errors');

/**
 *
 * KeyScheme
 *
 * The KeyScheme is responsible for handling scheme-specific UI functionality
 * for the key manager (e.g., importing public/private keys, authenticating to
 * a key server).
 */

function KeySchemeBase() {
  this.usesEncryptedStorage = true;
}

// Optional init method called on startup if no keys are present.
KeySchemeBase.prototype.init = null;

KeySchemeBase.prototype.getUI = function () {
  throw new Errors.AbstractMethodError('Abstract method getUI not implemented');
};

KeySchemeBase.prototype.handleError = function () {
  throw new Errors.AbstractMethodError('Abstract method handleError not implemented');
};

KeySchemeBase.prototype.create = function () {
  throw new Errors.AbstractMethodError('Abstract method create not implemented');
};

KeySchemeBase.prototype.update = function () {
  throw new Errors.AbstractMethodError('Abstract method update not implemented');
};

KeySchemeBase.prototype.parse = function () {
  throw new Errors.AbstractMethodError('Abstract method parse not implemented');
};

/**
 *
 * KeySystem
 *
 * A KeySystem is an instantiation of a key management scheme that allows the
 * users to decrypt/sign data for a single identity and encrypt/verify data
 * for any number of identities. A KeySystem is responsible for performing
 * cryptographic operations with the keys it manages. Every KeySystem has a
 * fingerprint that uniquely identifies it.
 */

function KeySystemBase() {

}

KeySystemBase.serialize = function (keySystem) {
  throw new Errors.AbstractMethodError('Abstract method serialize not implemented');
};

/*jslint unparam: true*/

/**
 * Encrypts data for the provided identity. Returns the encrypted data along with
 * the fingerprint of the KeySystem that can decrypt it.
 *
 * @returns {null}
 */
KeySystemBase.prototype.encrypt = function (data, id) {
  throw new Errors.AbstractMethodError('Abstract method encrypt not implemented');
};

/**
 * Decrypts the provided data.
 *
 * @returns {null}
 */
KeySystemBase.prototype.decrypt = function (data, id) {
  throw new Errors.AbstractMethodError('Abstract method decrypt not implemented');
};

/**
 * Signs the provided data.
 *
 * @returns {null}
 */
KeySystemBase.prototype.sign = function (data, id) {
  throw new Errors.AbstractMethodError('Abstract method sign not implemented');
};

/**
 * Verifies that the provided signature is valid for the provided data.
 *
 * @returns {null}
 */
KeySystemBase.prototype.verify = function (data, id) {
  throw new Errors.AbstractMethodError('Abstract method verify not implemented');
};


// additional functions not in original design spec?

KeySystemBase.prototype.getFingerprint = function (id) {
  return null;
};

KeySystemBase.prototype.canEncrypt = function (id) {
  return null;
};

KeySystemBase.prototype.handleError = function (err) {
  return null;
};
/*jslint unparam: false*/

function KeyError() {

  // default dummy values for now
  this.scheme = '';
  this.system = '';
  this.message = '';
  this.details = []; // an array of bytes
}

/**
 *
 * KeyAttributes
 *
 * KeyAttributes contains information common to each key.
 * This includes display information like name and color.
 * It also includes storage specification, scheme name,
 * and the fingerprint of the key.
 */

function KeyAttributes() {

  // dummy default values for now
  this.id = null;
  this.fingerprint = '';
  this.name = '';
  this.color = 'black';
  this.scheme = '';
  this.canSelect = true;
  this.canHaveRecipients = true;
  this.storage = 'local';
  this.privateKeyDestroyed = false;
  this.publicKeyDestroyed = false;
}

/**
 * Serializes a KeyAttributes instance.
 * @returns {string} - serialized representation of an attributes instance.
 */
KeyAttributes.prototype.serialize = function () {
  return JSON.stringify({
                          id:                this.id ? this.id.serialize() : null,
                          fingerprint:       this.fingerprint,
                          name:              this.name,
                          color:             this.color,
                          scheme:            this.scheme,
                          canSelect:         this.canSelect,
                          canHaveRecipients: this.canHaveRecipients,
                          storage:           this.storage,
                          created:           this.created,
                          expired:           this.expired,
                          privateKeyUsed:    this.privateKeyUsed,
                          usesEncryptedStorage: this.usesEncryptedStorage,
                          privateKeyDestroyed:  this.privateKeyDestroyed,
                          created:           this.created,
                          lifespan:          this.lifespan,
                          threadId:          this.threadId,
                          publicOnly:        this.publicOnly,
                          publicKeyDestroyed: this.publicKeyDestroyed
                        });
};

/**
 * Parses a KeyAttributes instance from a string.
 *
 * @param {string} serialized - string representation of an attributes instance.
 * @returns {KeyAttributes}
 */
KeyAttributes.parse = function (serialized) {
  var options = JSON.parse(serialized);

  var attributes = new KeyAttributes();
  attributes.id = options.id ? Identifier.parse(options.id) : null;
  attributes.fingerprint = options.fingerprint;
  attributes.name = options.name;
  attributes.color = options.color;
  attributes.scheme = options.scheme;
  attributes.canSelect = options.canSelect;
  attributes.canHaveRecipients = options.canHaveRecipients;
  attributes.storage = options.storage;
  attributes.created = options.created;
  attributes.lifespan = options.lifespan;
  attributes.threadId = options.threadId;
  attributes.privateKeyDestroyed = options.privateKeyDestroyed;
  attributes.publicOnly = options.publicOnly;
  attributes.publicKeyDestroyed = options.publicKeyDestroyed;

  return attributes;
};

/**
 *
 * Identifier
 *
 * Contains data to identify the owner of a key.
 * Includes the type and value of the ID.
 */

function Identifier(value, type) {
  this.type = type || 'Email';
  this.value = value || '';
}

/**
 * Serializes an Identifier instance.
 * @returns {string} - serialized representation of an ID instance.
 */
Identifier.prototype.serialize = function () {
  return JSON.stringify({
                          type:  this.type,
                          value: this.value
                        });
};

/**
 * Parses an Identifier instance from a string.
 *
 * @param {string} serialized - string representation of an ID instance.
 * @returns {Identifier}
 */
Identifier.parse = function (serialized) {
  var options = JSON.parse(serialized);

  return new Identifier(options.value, options.type);
};

/**
 * Compares an Identifier instance with this one for equality.
 *
 * @param {Identifier} id - Identifier to compare.
 * @returns {boolean} - whether the two are equal.
 */
Identifier.prototype.equals = function (id) {
  return this.type === id.type && this.value === id.value;
};

//////////////////////////////////////////////


module.exports = {
  KeySchemeBase: KeySchemeBase,
  KeySystemBase: KeySystemBase,
  KeyAttributes: KeyAttributes,
  KeyError:      KeyError,
  Identifier:    Identifier
};