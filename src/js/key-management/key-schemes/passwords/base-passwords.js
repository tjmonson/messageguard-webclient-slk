/*
* Shared symmetric key that encrypts via AES
*/

var bcrypt = require('bcryptjs');

var KeyPrototypes = require('../../key-prototypes'),
    $             = require('../../../common/jquery-bootstrap'),
    encoder       = require('../../../common/encoder'),
    Encryptor     = require('../../../common/encryptor'),
    Errors        = require('../../../common/errors');


// ******************************
// 
// Key Scheme
// 
// ******************************

/**
 * Placeholder for a password.
 */
var _DEFAULT_SECRET_STRING_VAL = new Array(20).join('*');

var PasswordScheme = function (schemeName, keySystemType) {
  this.name = schemeName;
  this.keySystemType = keySystemType;
};

PasswordScheme.prototype = new KeyPrototypes.KeySchemeBase();

/**
 * Given a {PasswordSystem} and callback for setting save-state, produce
 * an element configured for editing the key system.
 *
 * @param {PasswordSystem} keySystem - system for editing. Might be null, in which case this is a new system editor.
 * @callback callbacks - object with callbacks to interact with UI manager.
 * @param {string} promptType - specifies what prompt type we should render this for.
 * @promise {JQuery} - element for editing the key system.
 */
PasswordScheme.prototype.getUI = function (keySystem, callbacks, promptType) {
  var editor = $('<div> \
                  <div class="encrypt-instructions" style="padding-bottom: 30px;font-size: 16px;"> \
                    Select a password with which to encrypt this message. \
                    <br /><br />\
                    <span style="color:red"> \
                      You will need to share this password with your intended recipient (for example, in person or through a phone call). \
                    </span> \
                  </div> \
                  <div class="decrypt-instructions" style="padding-bottom: 30px;font-size: 16px;"> \
                    Enter the password with which this message was encrypted. \
                    <br> \
                    <span style="font-weight: bold;font-style: italic;"> \
                      If you don\'t know the password, contact the sender. \
                    </span> \
                  </div> \
                  <input type="password" name="password" class="shared-secret form-control" style="margin-bottom:5px"> \
                  <input type="password" name="password-confirm" class="shared-secret-confirm form-control"> \
                  <div class="password-mismatch-error alert alert-danger" style="display:none;margin-top:10px"> \
                    Your passwords do not match. \
                  </div> \
                  </div>');

  var sharedSecretField = editor.find('.shared-secret'),
      sharedSecretConfirmField = editor.find('.shared-secret-confirm'),
      encryptInstructionField = editor.find('.encrypt-instructions'),
      decryptInstructionField = editor.find('.decrypt-instructions'),
      passwordMismatchField = editor.find('.password-mismatch-error');
  
  switch (promptType) {
    case 'encrypt':
      sharedSecretField.attr('placeholder', 'Enter the password to encrypt the message');
      sharedSecretConfirmField.attr('placeholder', 'Confirm password');
      encryptInstructionField.show();
      decryptInstructionField.hide();
      break;

    case 'decrypt':
      sharedSecretField.attr('placeholder', 'Enter the password to decrypt the message.');
      sharedSecretConfirmField.attr('placeholder', 'Confirm');
      encryptInstructionField.hide();
      decryptInstructionField.show();
      sharedSecretConfirmField.hide();
      break;

    case 'standard':
    default:
      sharedSecretField.attr('placeholder', 'Enter the password you will use to encrypt messages');
      sharedSecretConfirmField.attr('placeholder', 'Confirm');
      encryptInstructionField.show();
      decryptInstructionField.hide();
      break;
  }

  // Set default text in the password fields.
  editor.find('input[type="password"]').val(keySystem ? _DEFAULT_SECRET_STRING_VAL : '');

  // Submit on key down function.
  var submitOnEnter = function(e) {
    if (e.keyCode === 13) {
      callbacks.saveClick();
    }
  };
  
  if (promptType === 'decrypt') {
    sharedSecretField.on('input', function() {
      callbacks.isSaveable(sharedSecretField.val().length !== 0);
    }).trigger('input').keydown(submitOnEnter);
  } else {
    sharedSecretConfirmField.on('input', function() {
      debugger;
      var password = sharedSecretField.val(),
          password2 = sharedSecretConfirmField.val();

      if (password.length === 0 || password2.length === 0) {
        passwordMismatchField.hide();
        callbacks.isSaveable(false);
        return;
      }

      if (password === password2) {
        passwordMismatchField.hide();
        callbacks.isSaveable(true);
      } else {
        passwordMismatchField.show();
        callbacks.isSaveable(false);
      }
    }).trigger('input').keydown(submitOnEnter);
  }

  return Promise.resolve(editor);
};

PasswordScheme.prototype.handleError = function (keyError, editor, resolved) {
  // TODO
};

/**
 * Creates a new {PasswordSystem} based on the given editor element.
 * @param {JQuery} editor - element for creating the key system.
 * @promise {PasswordSystem} - created system
 */
PasswordScheme.prototype.create = function (editor, fingerprintsToMatch) {
  var password = editor.find('.shared-secret').val();

  // If we're not trying to match any fingerprints, just return a new key system.
  if (!fingerprintsToMatch || !fingerprintsToMatch.length) {
    return Promise.resolve(new this.keySystemType(password));
  }

  // Otherwise, try and match one of the provided fingerprints.
  var matchedFingerprint = null;

  for (var i = 0; i < fingerprintsToMatch.length; i++) {
    var fingerprint = fingerprintsToMatch[i];

    if (Encryptor.verifyPasswordHash(password, fingerprint)) {
      matchedFingerprint = fingerprint;
      break;
    }
  }

  if (!matchedFingerprint) {
    return Promise.reject(new Errors.KeyCreateFailedError('Password does not match any of the fingerprints.'));
  }

  var system = new this.keySystemType(),
      key    = Encryptor.passwordKeyFromPasswordAndHash(password, matchedFingerprint);

  system._setSecretKey(matchedFingerprint, key);

  return Promise.resolve(system);
};

/**
 * Updates a given system using a given key editor element.
 *
 * @param {PasswordSystem} keySystem - system to update.
 * @param {JQuery} editor - element used for updating.
 * @promise {*} - resolves when updating is completed.
 */
PasswordScheme.prototype.update = function (keySystem, editor) {
  var password = editor.find('.shared-secret').val();

  if (password !== _DEFAULT_SECRET_STRING_VAL) {
    keySystem._setSecret(password);
  }

  return Promise.resolve();
};

/**
 * Serializes a given system into a string representation.
 *
 * @param {PasswordSystem} keySystem - system to serialize.
 * @promise {string} - serialized representation.
 */
PasswordScheme.prototype.serialize = function (keySystem) {
  return Promise.resolve(JSON.stringify([
                                          keySystem.attributes.fingerprint,
                                          keySystem.secretKey
                                        ]));
};

/**
 * Parses a string representation into a system.
 *
 * @param {string} serializedSystem - string representation to parse.
 * @promise {PasswordSystem} - parsed system
 */
PasswordScheme.prototype.parse = function (serializedSystem) {
  var keyData = JSON.parse(serializedSystem);

  var hash   = keyData[0],
      secret = keyData[1];

  var system = new this.keySystemType();
  system._setSecretKey(hash, secret);

  return Promise.resolve(system);
};


// ******************************
// 
// Key System
// 
// ******************************

/**
 * Constructs a new key system.
 *
 * @param key - secret key parameter
 * @constructor
 */
var PasswordSystem = function (password) {
  this.attributes = new KeyPrototypes.KeyAttributes();
  this.attributes.canSelect = true;
  this.attributes.canHaveRecipients = false;

  if (password) {
    this._setSecret(password);
  }
};

PasswordSystem.prototype = new KeyPrototypes.KeySystemBase();

PasswordSystem.prototype._setSecret = function (password) {
  var hash = Encryptor.passwordToHash(password),
      key  = Encryptor.passwordKeyFromPasswordAndHash(password, hash);

  this._setSecretKey(hash, key);
};

/**
 * Sets the secret key, and updates the fingerprint.
 *
 * @param key - secret key parameter
 * @private
 */
PasswordSystem.prototype._setSecretKey = function (hash, key) {
  this.secretKey = key;
  this.attributes.fingerprint = hash;
};

/**
 * Returns whether or not this key can encrypt for a given ID
 *
 * Passwords can encrypt for any ID, so always resolves to true.
 *
 * @param {Object.<string, string>} id - id to check
 * @promise {Object.<string, *>} - whether or not this key can encrypt for the given ID.
 */
PasswordSystem.prototype.canEncrypt = function (id) {
  return Promise.resolve({
                           id:         id,
                           canEncrypt: true,
                           message:    ''
                         });
};

/**
 * Performs symmetric AES encryption on some data.
 *
 * @param {string} data - data to encrypt.
 * @promise {Object.<string, string>} - result of encryption, including the fingerprint for decryption.
 */
PasswordSystem.prototype.encrypt = function (data) {
  return Promise.resolve({
                           encrypted:   Encryptor.encrypt(data, this.secretKey),
                           fingerprint: this.attributes.fingerprint
                         });
};

/**
 * Performs symmetric AES decryption.
 *
 * @param {string} data - encrypted data.
 * @promise {string} - result of decryption.
 */
PasswordSystem.prototype.decrypt = function (data) {
  try {
    return Promise.resolve(Encryptor.decrypt(data, this.secretKey));
  } catch (e) {
    if (e instanceof Errors.EncryptionError) {
      throw new Errors.DecryptFailedError();
    } else {
      throw e;
    }
  }
};

/**
 * Performs signing using an HMAC, derived from the master secret.
 *
 * @param {string} data - data to sign.
 * @promise {string} - HMAC signature.
 */
PasswordSystem.prototype.sign = function (data) {
  return Promise.resolve('');
};

/**
 * Verifies a signature. Does nothing, since the Encryptor class handles
 * signature verification.
 *
 * @param {string} data - data to verify signature for.
 * @param {string} signature - signature to verify.
 * @promise {boolean} - whether or not the signature is valid.
 */
PasswordSystem.prototype.verify = function (data, signature) {
  return Promise.resolve(true);
};

module.exports = {
  scheme: PasswordScheme,
  system: PasswordSystem
};
