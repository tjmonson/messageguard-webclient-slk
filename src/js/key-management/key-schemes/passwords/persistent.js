/*
* Shared symmetric key that encrypts via AES
* Lives in persistent local storage.
*/

var PasswordBase = require('./base-passwords');

var SCHEME_NAME = 'Password';

function PersistentPasswordsSystem() {
  PasswordBase.system.apply(this, arguments);

  this.attributes.scheme = SCHEME_NAME;
}

PersistentPasswordsSystem.prototype = Object.create(PasswordBase.system.prototype);

var scheme = new PasswordBase.scheme(SCHEME_NAME, PersistentPasswordsSystem);

module.exports = scheme;
