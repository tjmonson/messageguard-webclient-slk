/*
* Shared symmetric key that encrypts via AES
*/

var PGP  = require('../../../../lib/openpgp/openpgp'),
    util = require('util');

var KeyPrototypes = require('../key-prototypes'),
    KeyManager    = require('../key-manager'),
    Errors        = require('../../common/errors'),
    encoder       = require('../../common/encoder'),
    Encryptor     = require('../../common/encryptor'),
    Time          = require('../../common/time');

Errors.registerErrorType(Errors.KeyManagerError, 'SLKError');
Errors.registerErrorType(Errors.SLKError, 'PublicKeyNotFoundError');

// ******************************
//
// Key Scheme
//
// ******************************

var SCHEME_NAME = 'SLK';

var UNUSED_LIFESPAN_DAYS = 30;
var USED_LIFESPAN_DAYS = 2;

var UNUSED_LIFESPAN_UTC = 1000 * 60 * 60 * 24 * UNUSED_LIFESPAN_DAYS;
var USED_LIFESPAN_UTC = 1000 * 60 * 60 * 24 * USED_LIFESPAN_DAYS;
var PERSIST_ADDITIONAL_LIFESPAN_UTC = USED_LIFESPAN_UTC;

var SLKScheme = function () {
  this.name = SCHEME_NAME;
};

SLKScheme.prototype = new KeyPrototypes.KeySchemeBase();

/**
 * Called to initialize SLK keys
 *
 * Gets a list of identities currently known to the key server, and
 * automatically creates keys for each identity.
 *
 * @promise {SLKSystem[]} - list of new SLK systems created
 * @reject error - AJAX error if encountered
 */
SLKScheme.prototype.init = function (callbacks) {

};

/**
 * Given a SLKSystem and callback for setting save-state, produce
 * an element configured for editing the key system.
 *
 * @param {SLKSystem} keySystem - system for editing. Might be null, in which case this is a new system editor.
 * @callback callbacks - object with callbacks to interact with UI manager.
 * @promise {JQuery} - element for editing the key system.
 */
SLKScheme.prototype.getUI = function (keySystem, callbacks) {
  return Promise.resolve(true);
};

/**
 * Verifies a given identity against the key server to ensure the
 * key server will grant access to the identity's private key.
 *
 * If verification is successful, the private key and public parameters will be returned.
 *
 * @param {Identifier} id
 * @promise {Object.<string, number[]>} - object containing the private key and public parameters.
 * @reject {Object.<string, string>} - error object
 * @private
 */
SLKScheme.prototype._verifyID = function (id) {
  return Promise.resolve(false);
};

/**
 * Retrieves a public key from the key server. Performs client-side caching.
 * Caching is not persistent, and will be cleared when the SLKSystem object is destroyed.
 *
 * @param {Identifier} id - object representing the identity to get a key for.
 * @promise {Object.<string, string>} - SLK public key for the given ID, along with its fingerprint.
 * @reject {*} - rejects if identity does not exist.
 * @private
 */
SLKScheme._getPublicKey = function (id) {

  return undefined;
};

SLKScheme.prototype.handleError = function (keyError, editor, resolved) {
  // TODO
};

SLKScheme.prototype.applyPersistToSystem = function(system) {
  var now = Time.getUTCTime();
  var nowPlusPersist = now + PERSIST_ADDITIONAL_LIFESPAN_UTC;
  var newLifespan = nowPlusPersist - system.attributes.created;
  system.attributes.lifespan = newLifespan;
};

SLKScheme.prototype.applySyncToSystem = function(system, createdUTC, lifespanUTC) {
  var now = Time.getUTCTime();
  var otherLifespanLeft = (createdUTC + lifespanUTC) - now;
  var systemCreatedNowDiff = now - system.attributes.created;

  system.attributes.lifespan = systemCreatedNowDiff + otherLifespanLeft;

};

/**
 * Given an element for an editor, create a new SLKSystem instance.
 *
 * @param {JQuery} editor
 */
SLKScheme.prototype.create = function (emailAddress, threadId) {
  return SLKSystem._generateSystem(new KeyPrototypes.Identifier(emailAddress, 'Email'), threadId);
};

SLKScheme.prototype.createPublic = function (keyData) {
  var system = new SLKSystem(keyData.publicKey, null);
  system.attributes.fingerprint = keyData.fingerprint;
  system.attributes.id = new KeyPrototypes.Identifier(keyData.id.value, keyData.id.type);
  system.attributes.created = keyData.created;
  system.attributes.lifespan = keyData.lifespan;
  system.attributes.privateKeyDestroyed = false;
  system.attributes.usesEncryptedStorage = false;
  system.attributes.threadId = keyData.threadId;
  system.attributes.publicOnly = true;
  return system;
};

/**
 * Given an element for an editor and an existing SLKSystem instance, update the instance.
 *
 * @param {SLKSystem} keySystem - system to update.
 * @param {JQuery} editor - element representing the new key.
 */
SLKScheme.prototype.update = function (keySystem, editor) {
  var id = new KeyPrototypes.Identifier(id_value, id_type);

  return SLKSystem._updateSystem(keySystem, id);
};

/**
 * Serializes a system into a string representation.
 *
 * @param {SLKSystem} keySystem - system to serialize.
 * @promise {string} - serialized representation.
 */
SLKScheme.prototype.serialize = function (system) {
  var serialized = JSON.stringify({
                                    publicKey:  system.publicKey,
                                    privateKey: system.privateKey
                                  });

  return Promise.resolve(serialized);
};

/**
 * Parses a SLKSystem instance from a serialized representation.
 *
 * @param {string} serializedSystem - serialized representation of a key system.
 * @promise {SLKSystem} - parsed system
 */
SLKScheme.prototype.parse = function (serializedSystem) {
  var parsed = JSON.parse(serializedSystem);

  var publicKey = parsed.publicKey;
  var privateKey = parsed.privateKey;

  return Promise.resolve(new SLKSystem(publicKey, privateKey));
};


// ******************************
//
// Key System
//
// ******************************

/**
 * Constructs a new SLKSystem instance.
 *
 * @param {Identifier} id - identifier for key
 * @param {string} publicKey - public key
 * @param {string} privateKey - private key
 * @constructor
 */
var SLKSystem = function (publicKey, privateKey) {
  this.attributes = new KeyPrototypes.KeyAttributes();
  this.attributes.scheme = SCHEME_NAME;
  this.attributes.canSelect = true;
  this.attributes.canHaveRecipients = true;
  this.attributes.privateKeyDestroyed = false;
  this.publicKey = publicKey;
  this.privateKey = privateKey;

  this._updateFingerprint();
};

/**
 * Generates a system for the given id and type.
 * Uploads the public key after key creation.
 *
 * @param {Identifier} id - id for key
 * @promise {SLKSystem} - new SLK key created
 */
SLKSystem._generateSystem = function (id, threadId) {
    return SLKSystem._generateKey(id).then(function (keys) {

        var system = new SLKSystem(keys.publicKey, keys.privateKey);
        system.attributes.publicKey = keys.publicKey;
        system.attributes.id = id;
        system.attributes.created = Time.getUTCTime();
        system.attributes.lifespan = UNUSED_LIFESPAN_UTC;
        system.attributes.expired = false;
        system.attributes.privateKeyDestroyed = false;
        system.attributes.privateKeyUsed = false;
        system.attributes.usesEncryptedStorage = false;
        system.attributes.publicOnly = false;
        system.attributes.threadId = threadId;
        system._updateFingerprint();
        return system;

    });
};

SLKSystem.prototype._updateCreated = function () {
  this.attributes.created = Time.getUTCTime();
};

SLKSystem.prototype.isExpired = function() {
  return Time.isExpired(this.attributes.created, this.attributes.lifespan);
};



/**
 * Updates a system for a new id and type.
 *
 * Generates a new system, then copies the critical values over and updates the fingerprint.
 *
 * @param {SLKSystem} system - system to update
 * @param {Identifier} id - id for new key
 */
SLKSystem._updateSystem = function (system, id) {
  return SLKSystem._generateSystem(id).then(function (newSystem) {
    system.attributes.id = newSystem.attributes.id;
    system.publicKey = newSystem.publicKey;
    system.privateKey = newSystem.privateKey;
    system._updateFingerprint();
  });
};

/**
 * Generates a random PGP key for a give id and type.
 *
 * @param {Identifier} id - id for new key.
 * @promise {Object.<string, string>} public and private keys.
 */
SLKSystem._generateKey = function (id) {
  var options = {
    numBits: 2048,
    userIds: [{name: id.type + ':' + id.value}]
  };

  return PGP.generateKey(options).then(function (keyPair) {
    var publicKey = keyPair.publicKeyArmored;
    var privateKey = keyPair.privateKeyArmored;

    return {
      publicKey:  publicKey,
      privateKey: privateKey
    };
  });
};

SLKSystem.prototype = new KeyPrototypes.KeySystemBase();

/**
 * Updates a key's fingerprint.
 * @private
 */
SLKSystem.prototype._updateFingerprint = function () {
  this.attributes.fingerprint = SLKSystem._getFingerprint(this.publicKey);
};

/**
 * Gets the fingerprint for a given public key.
 *
 * Performs SHA hashing
 *
 * @param {string} publicKey - key to generate fignerprint for.
 * @private
 */
SLKSystem._getFingerprint = function (publicKey) {
  return Encryptor.hash(publicKey);
};

/**
 * Uploads a public key to the key server.
 *
 * @param {Identifier} id - id for key.
 * @param {string} publicKey - public key to upload.
 * @promise {*} - resolves when upload complete.
 * @reject {*} - fails when not authorized.
 * @private
 */
SLKSystem._putPublicKey = function (id, publicKey) {
    return Promise.resolve(undefined);
};

/**
 * Checks whether this key system can encrypt for the given ID.
 * If a public key exists for this user, then encryption can proceed.
 *
 * @param {Identifier} id - id to check
 * @promise {Object.<string, *>} - whether this key can encrypt for the given ID.
 */
SLKSystem.prototype.canEncrypt = function (id) {
  return new Promise(function (resolve) {
    SLKScheme._getPublicKey(id).then(function () {
      resolve({
                id:         id,
                canEncrypt: true,
                message:    ''
              });
    }).catch(function (err) {
      // TODO - just throw an error?
      resolve({
                id:         id,
                canEncrypt: false//,
                //message:    PROMPT_INSTALL_MSG.replace(/DEST_EMAIL_ADDRESS/g, encodeURIComponent(id.value))
              });
    });
  });
};

/**
 * Performs PGP encryption on a given piece of data.
 *
 * @param {string} data - data to encrypt
 * @param {Identifier} id - id to encrypt for
 * @promise {Object.<string, string>} - result of encryption, including the fingerprint for the receiving side.
 */
SLKSystem.prototype.encrypt = function (data, id, publicSystem) {
  return new Promise(function(resolve) {
    return PGP.encrypt({
                         data:       data,
                         publicKeys: PGP.key.readArmored(publicSystem.publicKey).keys,
                         armor:      true
                       })
        .then(function (result) {
          resolve({
            encrypted:   result.data,
            fingerprint: publicSystem.attributes.fingerprint
          });
        });
  }).catch(function (e) {
    if (e instanceof Errors.KeyNotFoundError) {
      throw new Errors.NeedRecipientsPublicKeyError('MessageGuard needs the public key of recipient: ' + id.value +
                                                 ' before encrypting for them.', {destinationAddress: id.value});
    } else {
      throw new Errors.EncryptFailedError('Error encrypting for ID: ' + id.serialize() + ', ' + e.toString(), e);
    }
  });
};

/**
 * Performs PGP decryption.
 *
 * @param {string} data - data to be decrypted
 * @promise {string} - result of decryption
 */
SLKSystem.prototype.decrypt = function (data) {
  var privKeys = PGP.key.readArmored(this.privateKey);
  var privKey = privKeys.keys[0];

  var message = PGP.message.readArmored(data);

  return PGP.decrypt({
                       message:    message,
                       privateKey: privKey
                     }).then(function (result) {
    return result.data;
  });
};

/**
 * Performs PGP signing on a piece of data.
 *
 * @param {string} data - data to sign.
 * @promise {string} - signature data.
 */
SLKSystem.prototype.sign = function (data) {
  var priv = PGP.key.readArmored(this.privateKey);
  var privKey = priv.keys[0];

  var self = this;

  return PGP.sign({
                    data:        data,
                    privateKeys: privKey,
                    armor:       true
                  }).then(function (result) {
    return JSON.stringify({
                            message:   result.data,
                            signer_id: self.attributes.id.serialize()
                          });
  });
};

/**
 * Performs PGP verification on a signature.
 *
 * @param {string} data - original data that was signed. In this implementation, unnecessary.
 * @param {string} signature - signature data for verification.
 * @promise {boolean} - whether or not the signature is valid.
 */
SLKSystem.prototype.verify = function (signature, signerSystem) {

  var cleartextMessage = PGP.cleartext.readArmored(signature.message);

  return new Promise(function (resolve) {
    PGP.verify({
                  message:    cleartextMessage,
                  publicKeys: PGP.key.readArmored(signerSystem.publicKey).keys
                }).then(function (verificationResult) {
      if (!verificationResult.signatures || !verificationResult.signatures.length) {
        resolve(false);
        return;
      }

      for (var i = 0; i < verificationResult.signatures.length; i++) {
        if (!verificationResult.signatures[i].valid) {
          resolve(false);
          return;
        }
      }

      resolve(true);
    });
  });
};

module.exports = new SLKScheme();
