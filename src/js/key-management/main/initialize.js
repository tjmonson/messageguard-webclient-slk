/**
 * Key management initialization file.
 */

var $          = require('../../common/jquery-bootstrap'),
    KeyManager = require('../key-manager'),
    KeySchemes = require('../key-schemes'),
    KeyStorage = require('../key-storage'),
    urls       = require('../../common/urls');

var emailRegex = /(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/i;

if(navigator.platform == "Win32") {
  $('input[type=email][name=email').val("princenosnow@gmail.com");
}
else {
    $('input[type=email][name=email').val("monson@isrl.byu.edu");
}

/**
 * Shows the welcome message. Returns a promise that resolves as soon as the modal is dismissed.
 */
function showStartupModal() {
  return new Promise(function (resolve) {
    $(function () {
      $('#startup-modal').modal('show');
      $('#startup-modal .next-btn').click(function () {
        $('#startup-modal').modal('hide');
        resolve();
      });
    });
  });
}

function showEmailModal() {
  return new Promise(function (resolve) {
    $('#email-modal .next-btn').click(function () {
      var emailInput = $('input[type=email][name=email]').val();
      if(emailRegex.test(emailInput)) {
        $('#email-modal').modal('hide');
        resolve(emailInput);
      }
      else {
        $('#email-modal-alert').show();
      }
    });
    $('#email-modal').modal('show');
  });
}

$("#expire-keys-btn").click(function() {
  KeyManager.expireAllKeys().then(function(count) {
    var result = $("#expiration-result");
    result.text(result.text().replace('@count', '' + count));
    result.show();
    $("#expiration-result").show();
    if (urls.isExtension && window.opener && window.opener.kango) {
      window.opener.kango.dispatchMessage('refresh_gmail_tabs');
    }
  }).catch(function() {
    $("#expiration-error").show();
  });
});

/**
 * When initialization is complete, attempt to dispatch an 'initialization_complete'
 * message to the opener's kango object. This will let the extension backend know that
 * initialization is complete.
 */

var isCompleted = false;

function onComplete() {
  isCompleted = true;
  $('.close-btn').show();
  KeyManager.setExtensionInitialized().then(function() {
    if (urls.isExtension && window.opener && window.opener.kango) {
      window.opener.kango.dispatchMessage('initialization_complete');
    }
  });
}

$('.close-btn').hide().click(function () {
  window.close();
});

window.addEventListener('beforeunload', function (e) {
  if (!isCompleted) {
    var confirmationMessage = 'MessageGuard has not completed initialization. Please continue before closing.';
    e.returnValue = confirmationMessage;     // Gecko, Trident, Chrome 34+
    return confirmationMessage;              // Gecko, WebKit, Chrome <34
  }
});

if (!KeyStorage.needsMasterPassword()) {
  // We'll be skipping the master password modal.
  $('.initialization-message').hide();

  if (KeySchemes.hasSchemeWithInitFunction()) {
    $('.no-master-password-message').show();
  } else {
    // We'll also be skipping initialization.
    $('.nothing-to-do-message').show();

    // No 'next' button to click.
    $('#startup-modal .modal-footer .next-btn').hide();
    onComplete();
  }
}


// Show the welcome message. Then, if necessary, prompt for a master password.
// Then intialize keys. Finally, show the complete modal and notify the extension
// backend that initialization has completed.

KeyManager.extensionInitialized().then(function(isInitialized) {
  if(!isInitialized) {
    showStartupModal().then(function () {
      return KeyManager.ensureKeyStoragePasswordPresent();
    }).then(function() {
      return new Promise(function (resolve) {
        
        $('#key-initialization-modal .error-message').hide();
        $('#key-initialization-modal .loading-message').show();
        $('#key-initialization-modal').modal('show');
        window.setTimeout(resolve, 1000);
      });  
    }).then(function () {
      $('#key-initialization-modal').modal('hide');
      $('#initialization-complete-modal').modal('show');

      onComplete();
    });
  }
  else {
    $('#expiration-modal').modal('show');
    isCompleted = true;
  }
});
