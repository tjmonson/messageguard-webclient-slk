/**
 * Standalone task handler. Used if worker threads are not supported.
 *
 * Directly registers receiver tasks to the task request handler, without forwarding.
 */

var EventNames         = require('../event-names'),
    ReceiverTasks      = require('./receiver-tasks'),
    TaskRequestHandler = require('./task-request-handler');

module.exports = {
  init: function () {
    EventNames.mapNames(function (eventLabel, eventName) {
      var handler = ReceiverTasks[eventName];
      TaskRequestHandler.registerHandler(eventName, handler);
    });
  }
};
