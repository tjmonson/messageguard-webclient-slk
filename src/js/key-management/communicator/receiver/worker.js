/**
 * Main worker thread module. Handles passing tasks to the key manager tasks module, and relaying responses
 * back to the main thread.
 *
 * Also configures the worker storage objects shim with a function that can issue requests to the main thread's
 * storage objects.
 */
var LocalEventEmitter = new (require('events'))();

var Errors               = require('../../../common/errors'),
    UUID                 = require('../../../common/uuid'),
    WorkerStorageObjects = require('../../../common/storage/storage-objects/worker-objects'),
    ReceiverTasks        = function () { return require('./receiver-tasks'); }; // Delaying require('./receiver-tasks')
                                                                                // until after injectRequester() is
                                                                                // called below.

/**
 * This lets the worker storage objects know how to issue requests to the main thread.
 * It's necessary to avoid any circular dependencies.
 *
 * The provided function is called when a worker storage object needs access to its corresponding
 * native storage object.
 *
 * @param storageType    - string indicating what storage object is needed.
 *                         (local, session, cookies, or eventEmitter)
 * @param requestDetails - object containing parameters to pass to the native storage object.
 * @returns Promise      - resolved when the reply from the main thread is received.
 */
WorkerStorageObjects.injectRequester(function (storageType, requestDetails) {
  requestDetails.storageType = storageType;

  var replyUUID = UUID();

  return new Promise(function (resolve, reject) {
    // The replyUUID here is emitted on from within 'onmessage' below, so when a storage response is received, this
    // promise will resolve.
    LocalEventEmitter.once(replyUUID, function (reply) {
      if (reply.error) {
        reject(reply.error);
      } else {
        resolve(reply.result);
      }
    });

    postWorkerMessage(replyUUID, 'storage_request', requestDetails);
  });
});

/**
 * Posts a message to the main thread. Collects replyUUID, messageType, and message into one object to post across.
 */
function postWorkerMessage(replyUUID, messageType, message) {
  postMessage({
                replyUUID: replyUUID,
                msgType:   messageType,
                msg:       message
              });
}

// Critical that ReceiverTasks is not actually required until after injectRequester()
// is called above, in order to inject the required functionality into
// StorageObjects before anything required by receiver-tasks might need storage access.
ReceiverTasks = ReceiverTasks();

onmessage = function (e) {
  var replyUUID = e.data.replyUUID,
      taskName  = e.data.taskName,
      task      = e.data.task;

  // If this is the response to a storage request, emit on the replyUUID so the promise
  // returned from the injected requester function can resolve.
  if (e.data.msgType && e.data.msgType == 'storage_response') {
    LocalEventEmitter.emit(replyUUID, e.data.msg);
    return;
  }

  // We're receiving a storage event from the main thread; pass it on
  // to the event object.
  if (e.data.msgType && e.data.msgType == 'storage_event') {
    var callbackUUID = e.data.msg.callbackUUID,
        eventData    = e.data.msg.eventData;

    WorkerStorageObjects.eventEmitter.processForwardedEvent(callbackUUID, eventData);
    return;
  }

  // Now we're sure we're receiving a task request, so prep for that.

  /**
   * Function that posts a message back to the main thread with the results of the task.
   */
  var onComplete = postWorkerMessage.bind(null, replyUUID, 'task_response');

  /**
   * Function that posts an error back to the main thread.
   * @param e - error from handler. If it's not an MGError, wrap it.
   */
  var onError = function (e) {
    if (!(e instanceof Errors.MGError)) {
      e = new Errors.MGError(e.toString());
    }

    onComplete({error: e.serialize()});
  };

  /**
   * Function that posts a successful response back to the main thread.
   * @param result
   */
  var onSuccess = function (result) {
    onComplete({result: result});
  };

  if (taskName in ReceiverTasks) {
    try {
      ReceiverTasks[taskName](task).then(onSuccess).catch(onError);
    } catch (e) {
      onError(e);
    }
  } else {
    onError(new Errors.MGError('Illegal task name: ' + taskName));
  }
};
