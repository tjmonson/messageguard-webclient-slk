/**
 * Enumerates all labels for any tasks that the key manager might need to perform for the overlay.
 */

var names = {
  ENCRYPT_DATA:                   'encrypt_data',
  DECRYPT_DATA:                   'decrypt_data',
  ENCRYPT_DRAFT:                  'encrypt_draft',
  DECRYPT_DRAFT:                  'decrypt_draft',
  ENCRYPT_KEY:                    'encrypt_key',
  ENCRYPT_REQUESTED_KEY:          'encrypt_requested_key',
  DECRYPT_KEY:                    'decrypt_key',
  CAN_ENCRYPT:                    'can_encrypt',
  GET_KEY_INFO:                   'get_key_info',
  ENSURE_MASTER_PASSWORD_PRESENT: 'ensure_master_password_present',
  INITIALIZE_KEYS:                'initialize_keys',
  HEARTBEAT:                      'heartbeat',
  KEYS_UPDATED:                   'keys_updated',
  MASTER_PASSWORD_INITIALIZED:    'master_password_initialized',
  GENERATE_SLK:                   'generate_slk',
  GET_SLK:                        'get_slk',
  STORE_SLK:                      'store_slk',
  HAS_SYMMETRIC_KEY:              'has_symmetric_key',
  STORE_SYMMETRIC_KEY:            'store_symmetric_key',
  GET_ENCRYPTED_SYMMETRIC_KEY:    'get_encrypted_symmetric_key',
  STORE_KEY_REQUEST:              'store_key_request',
  HAS_MADE_KEY_REQUEST:           'has_made_key_request',
  STORE_KEY_RESPONSE:             'store_key_response',
  HAS_SENT_KEY_RESPONSE:           'has_made_key_response',
  ASSOCIATE_THREAD_IDS:           'associate_thread_ids',
  GET_ASSOCIATED_THREAD_ID:       'get_associated_thread_id',
  DESTROY_SLK:                    'destroy_slk',
  DESTROY_PUBLIC_SLK:             'destroy_public_slk',
  STORE_THREAD_METADATA:          'store_thread_metadata',
  GET_THREAD_METADATA:            'get_thread_metadata',
  GET_EXPIRED_DATA:               'get_expired_data',
  PERSIST_SLK:                    'persist_slk',
  SYNC_SLK_EXPIRATION:            'sync_slk_expiration',
  REMOVE_SLK_COMPLETELY:          'remove_slk_completely',
  THREAD_HAS_DESTROYED_PUBLIC_KEYS: 'thread_has_destroyed_public_keys',
  GET_MULTIPLE_EXPIRATION_DATA:     'get_multiple_expiration_data',
  HAS_DESTROYED_PUBLIC_SLK:         'has_destroyed_public_slk',
  SET_HAS_DESTROYED_PUBLIC_SLK:     'set_has_destroyed_public_slk'
};

module.exports = {
  /**
   * Calls a handler function for every name and label in the 'names' object.
   * @param handler - function to call for every key-name pair.
   */
  mapNames: function (handler) {
    Object.keys(names).forEach(function (key) {
      handler(key, names[key]);
    });
  }
};

/**
 * Populate the exports object with the names and labels of the events.
 */
module.exports.mapNames(function (key, name) {
  module.exports[key] = name;
});
