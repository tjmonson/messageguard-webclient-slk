/**
 * This module handles key manager tasks by forwarding details to the key manager iframe,
 * using a storage event emitter.
 *
 * In order to prevent storage events from crossing over into other windows' key managers,
 * the windowUUID url parameter is used to only emit events for this page's key manager iframe.
 */

var UUID          = require('../../common/uuid'),
    urls          = require('../../common/urls'),
    Errors        = require('../../common/errors'),
    EventEmitter  = require('../../common/storage/storage-objects').eventEmitter,
    EventNames    = require('./event-names'),
    KeyAttributes = require('../key-prototypes').KeyAttributes,
    Identifier    = require('../key-prototypes').Identifier;

var options    = urls.getUrlOptions(),
    windowUUID = options.window_uuid || '';

var MAX_TIMEOUT = 10 * 1000; // No operation should take more than ten seconds.

/**
 * Emits a task using localStorage events, and waits for a reply.
 * A reply comes in via an event fired for the 'reply_uuid' event,
 * which is randomly generated here.
 *
 * Events fired from here are received in the js/overlays/key-management.js module.
 *
 * @param {string} destinationEvent - name of event to fire task for.
 * @param {Object.<string, string>} task - task to fire.
 * @promise {*} - result of task.
 * @reject {Errors.MGError} - error if present.
 */
function emitTask(destinationEvent, task, timeout) {
  task = task || {};
  task.replyUUID = UUID();

  return new Promise(function (resolve, reject) {
    EventEmitter.emit(windowUUID + '_' + destinationEvent, task);

    var replyTimeoutID = null;

    /**
     * Function called when reply is received. Separately declared so it can be removed
     * from the eventEmitter in case of a timeout.
     *
     * @param msg - message to pass back to the requester.
     */
    var onComplete = function (msg) {
      // If there is an active timeout for this request, clear it.
      if (replyTimeoutID) {
        window.clearTimeout(replyTimeoutID);
        replyTimeoutID = null;
      }

      // If we received an error, parse it and reject with it. Otherwise, resolve with the result.
      if (msg.details.err) {
        var error = Errors.parse(msg.details.err);
        reject(error);
      } else {
        resolve(msg.details.result);
      }
    };

    // The replyUUID event will be emitted from within task-request-handler on the key manager side.
    EventEmitter.once(task.replyUUID, onComplete);

    // If we're given a timeout, set a timer. When it goes off, de-register onComplete from the event emitter,
    // and reject with a timeout error.

    if (typeof timeout == 'undefined' || timeout > 0) {
      replyTimeoutID = window.setTimeout(function () {
        EventEmitter.off(task.replyUUID, onComplete);
        reject(new Errors.OperationTimedOutError('Operation timed out for ' + destinationEvent));
      }, timeout || MAX_TIMEOUT);
    }
  });
}

module.exports = {

  /**
   * Task for encrypting arbitrary text with an encryption key.
   *
   * @param messageContents - plaintext to encrypt.
   * @param messageKey      - key used to encrypt contents.
   */
  encryptData: function (messageContents, messageKey) {
    return emitTask(EventNames.ENCRYPT_DATA, {
      contents: messageContents,
      key:      messageKey
    });
  },

  /**
   * Task for decrypting ciphertext.
   *
   * @param messageContents - ciphertext to decrypt.
   * @param messageKey      - key used to decrypt ciphertext.
   */
  decryptData: function (messageContents, messageKey) {
    return emitTask(EventNames.DECRYPT_DATA, {
      contents: messageContents,
      key:      messageKey
    });
  },

  /**
   * Task for encrypting arbitrary text with the draft encryption key.
   *
   * @param messageContents - plaintext to encrypt.
   */
  encryptDraft: function (messageContents) {
    return emitTask(EventNames.ENCRYPT_DRAFT, {
      contents: messageContents
    });
  },

  getExpiredData: function(emailAddress) {
    return emitTask(EventNames.GET_EXPIRED_DATA, emailAddress, -1);
  },

  generateSLK: function (emailAddress, threadId, keyData) {
    var data = {emailAddress: emailAddress, threadId: threadId};
    if(keyData)
      data.keyData = keyData;
    return emitTask(EventNames.GENERATE_SLK, data, -1);
  },

  getSLK: function (emailAddress, threadId) {
    return emitTask(EventNames.GET_SLK, {emailAddress: emailAddress, threadId: threadId});
  },

  destroySLK: function (emailAddress, threadId) {
    return emitTask(EventNames.DESTROY_SLK, {emailAddress: emailAddress, threadId: threadId});
  },

  destroyPublicSLK: function (emailAddress, threadId) {
    return emitTask(EventNames.DESTROY_PUBLIC_SLK, {emailAddress: emailAddress, threadId: threadId});
  },

  hasDestroyedPublicSLK: function (emailAddress, threadId) {
    return emitTask(EventNames.HAS_DESTROYED_PUBLIC_SLK, {emailAddress: emailAddress, threadId: threadId});
  },

  setHasDestroyedPublicSLK: function (emailAddress, threadId) {
    return emitTask(EventNames.SET_HAS_DESTROYED_PUBLIC_SLK, {emailAddress: emailAddress, threadId: threadId});
  },

  removeSLKCompletely: function (emailAddress, threadId) {
    return emitTask(EventNames.REMOVE_SLK_COMPLETELY, {emailAddress: emailAddress, threadId: threadId});
  },

  threadHasDestroyedPublicKeys: function (emailAddress, threadId) {
    return emitTask(EventNames.THREAD_HAS_DESTROYED_PUBLIC_KEYS, {emailAddress: emailAddress, threadId: threadId});
  },

  persistSLK: function(emailAddress, threadId) {
    return emitTask(EventNames.PERSIST_SLK, {emailAddress: emailAddress, threadId: threadId});
  },

  syncSLKExpiration: function(emailAddress, threadId, createdUTC, lifespanUTC) {
    return emitTask(EventNames.SYNC_SLK_EXPIRATION, {
      emailAddress: emailAddress,
      threadId: threadId,
      createdUTC: createdUTC,
      lifespanUTC: lifespanUTC
    });
  },
 
  getMultipleExpirationData: function(emailAddress, threadIds) {
    return emitTask(EventNames.GET_MULTIPLE_EXPIRATION_DATA, {
      emailAddress: emailAddress,
      threadIds: threadIds
    });
  },

  storeThreadMetadata: function(metadata) {
    return emitTask(EventNames.STORE_THREAD_METADATA, metadata);
  },

  getThreadMetadata: function(threadId) {
    return emitTask(EventNames.GET_THREAD_METADATA, threadId);
  },

  storeSLK: function (keyData) {
    return emitTask(EventNames.STORE_SLK, keyData);
  },

  hasSymmetricKey: function (messageId, emailAddress) {
    return emitTask(EventNames.HAS_SYMMETRIC_KEY, {messageId: messageId, emailAddress: emailAddress});
  },

  storeSymmetricKey: function (messageId, messageKey, generatorEmail) {
    return emitTask(EventNames.STORE_SYMMETRIC_KEY, {
      messageId: messageId,
      messageKey: messageKey,
      generatorEmail: generatorEmail
    });
  },

  getEncryptedSymmetricKey: function(messageId, email) {
    return emitTask(EventNames.GET_ENCRYPTED_SYMMETRIC_KEY, {messageId: messageId, email: email});
  },

  storeKeyRequest: function(messageId, email) {
    return emitTask(EventNames.STORE_KEY_REQUEST, {messageId: messageId, email: email});
  },

  hasMadeKeyRequest: function(messageId, email) {
    return emitTask(EventNames.HAS_MADE_KEY_REQUEST, {messageId: messageId, email: email});
  },

  storeKeyResponse: function(messageId, email) {
    return emitTask(EventNames.STORE_KEY_RESPONSE, {messageId: messageId, email: email});
  },

  hasSentKeyResponse: function(messageId, email) {
    return emitTask(EventNames.HAS_SENT_KEY_RESPONSE, {messageId: messageId, email: email});
  },

  associateThreadIds: function(urlThreadId, threadId) {
    return emitTask(EventNames.ASSOCIATE_THREAD_IDS, {urlThreadId: urlThreadId, threadId: threadId})
  },

  getAssociatedThreadId: function(urlThreadId) {
    return emitTask(EventNames.GET_ASSOCIATED_THREAD_ID, urlThreadId, -1);
  },

  /**
   * Task for decrypting ciphertext with the draft encryption key.
   *
   * @param messageContents - ciphertext to decrypt.
   */
  decryptDraft: function (messageContents) {
    return emitTask(EventNames.DECRYPT_DRAFT, {
      contents: messageContents
    });
  },

  /**
   * Task for encrypting a message key for an array of IDs.
   *
   * @param {string} messageKey - message key to encrypt.
   * @param {string} fingerprint - fingerprint of key to use.
   * @param {Identifier[]} ids - array of IDs to encrypt for.
   * @promise {Object<string, *>[]} - result of encryption, includes encrypted result and fingerprint for each
   *     recipient.
   */
  encryptMessageKey: function (messageKey, fingerprint, ids, threadId) {
    ids = ids.map(function (id) {
      return id.serialize();
    });

    return emitTask(EventNames.ENCRYPT_KEY, {
      fingerprint: fingerprint,
      messageKey:  messageKey,
      ids:         ids,
      threadId: threadId
    });
  },

  encryptRequestedKey: function (messageId, requester, sender, threadId) {
    return emitTask(EventNames.ENCRYPT_REQUESTED_KEY, {
      messageId: messageId, 
      requester: requester,
      sender:    sender,
      threadId: threadId
    });
  },

  /**
   * Task for decrypting a message key.
   *
   * @param {string} encryptedMessageKey - encrypted key to decrypt.
   * @param {string} fingerprint - key fingerprint to use to decrypt.
   * @promise {string} - decrypted message key
   */
  decryptMessageKey: function (encryptedMessageKey, fingerprint, threadId) {
    return emitTask(EventNames.DECRYPT_KEY, {
      fingerprint:         fingerprint,
      encryptedMessageKey: encryptedMessageKey,
      threadId: threadId
    }).then(function (result) {
      result.keyAttributes = KeyAttributes.parse(result.keyAttributes);
      return result;
    });
  },

  /**
   * Task for getting information on whether a key can encrypt for a given set of IDs.
   *
   * @param {string} fingerprint - key fingerprint to check
   * @param {Identifier[]} ids - array of IDs to check.
   * @promise {Object<string, *>[]} array of results: {id: {Identifier}, canEncrypt: {boolean}, message: {string}}
   */
  canEncrypt: function (fingerprint, ids) {
    ids = ids.map(function (id) {
      return id.serialize();
    });

    return emitTask(EventNames.CAN_ENCRYPT, {
      fingerprint: fingerprint,
      ids:         ids
    }).then(function (results) {
      return results.map(function (result) {
        result.id = Identifier.parse(result.id);
        return result;
      });
    });
  },

  /**
   * Task for getting an array of key attributes from storage.
   *
   * @promise {KeyAttributes[]} - array of key attributes.
   */
  getKeyInfo: function () {
    return emitTask(EventNames.GET_KEY_INFO).then(function (data) {
      return data.map(function (stringed) {
        return KeyAttributes.parse(stringed);
      });
    });
  },

  /**
   * Task for testing whether the key manager is awake.
   *
   * @promise {} - resolves when key manager responds.
   * @throws {Errors.KeyManagerError} - if maxTime is reached.
   */
  ensureKeyManagerAlive: function (maxTime) {
    maxTime = maxTime || 20000;
    var perHeartbeatTimeout = 500;
    var maxAttempts = maxTime / perHeartbeatTimeout;

    return new Promise(function (resolve, reject) {
      var numTries = -1;
      var tryAgain = function () {
        if (++numTries >= maxAttempts) {
          return reject(new Errors.KeyManagerError('Key Manager not alive after ' + (maxTime / 1000) + ' seconds.'));
        }

        emitTask(EventNames.HEARTBEAT, {}, perHeartbeatTimeout)
            .then(resolve)
            .catch(tryAgain);
      };

      tryAgain();
    });
  },

  /**
   * Task for ensuring that the master password is present on the key manager side.
   */
  ensureMasterPasswordPresent: function () {
    return emitTask(EventNames.ENSURE_MASTER_PASSWORD_PRESENT, null, -1);
  },

  /**
   * Task for performing key initialization.
   */
  initializeKeys: function () {
    return emitTask(EventNames.INITIALIZE_KEYS, null, -1);
  }
};
