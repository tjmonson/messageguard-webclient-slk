var $            = require('../common/jquery-bootstrap'),
    Encryptor    = require('../common/encryptor'),
    Errors       = require('../common/errors'),
    KeyUIManager = require('./key-ui-manager'),
    KeyStorage   = require('./key-storage'),
    KeySchemes   = require('./key-schemes'),
    Storage      = require('../common/storage'),
    Time         = require('../common/time'),
    Identifier   = require('./key-prototypes').Identifier;

/**
 * Constructor. Loads up the uiManagers, and initializes all key schemes.
 *
 * KeyScheme.prototype.init is called for each scheme for which we have no keys in storage.
 * Any keys created are added to storage.
 *
 * @constructor
 */
function KeyManager() {
  this.uiManagers = KeySchemes.getNames().reduce(function (managerMap, name) {
    managerMap[name] = new KeyUIManager(KeySchemes.getScheme(name));
    return managerMap;
  }, {});
}

/**
 * Performs key initialization for each of the schemes.
 *
 * @returns Promise - resolved when all keys are initialized.
 */
KeyManager.prototype.initializeKeys = function () {
  return Promise.all(KeySchemes.getNames().map(function (schemeName) {
    if (typeof KeySchemes.getScheme(schemeName).init === 'function') {

      var callbacks = {
        getAllSchemeAttributes: KeyStorage.getAttributesByScheme.bind(KeyStorage, schemeName),
        addKey:                 KeyStorage.add.bind(KeyStorage)
      };

      return KeySchemes.getScheme(schemeName).init(callbacks);
    }
  }));
};

/**
 * Creates the "Add new key" button UI element for a scheme.
 *
 * @param uiManager
 * @returns {*|jQuery|HTMLElement}
 */
KeyManager.prototype.setupUI = function () {

  var _makeAddButton = function (uiManager) {
    var button = $('<button class="add-key-button fa fa-plus"></button>');
    button.click(function () {
      uiManager.addKeySystem('standard');
    });
    return button;
  };

  /**
   * Sets up the tab for the given key scheme.
   *
   * @param {KeyUIManager} uiManager
   * @param {number} index of tab, used for ids.
   */
  var setupTab = function (uiManager, index) {
    var systemTypeTab = $('<li class="key-system-tab" role="presentation"><a role="tab" data-toggle="tab"></a></li>');
    systemTypeTab.find('a').attr('href', '#key-scheme-' + index).text(uiManager.scheme.name);

    var systemTypeSection = $(
        '<div id="key-scheme-' + index + '" class="key-system-section tab-pane fade in"></div>');

    systemTypeSection.append(_makeAddButton(uiManager));
    systemTypeSection.append($('<h1></h1>').text('Add new key (' + uiManager.scheme.name + ')'));

    return uiManager.setupUI().then(function (ui) {
      systemTypeSection.append(ui);

      systemTypeTab.appendTo('#manager-tabs');
      systemTypeSection.appendTo('#manager-tab-content');
    });
  };

  return Promise.all(Object.keys(this.uiManagers).map(function (manager, i) {
    var uiManager = this.uiManagers[manager];
    return setupTab(uiManager, i);
  }, this)).then(function () {
    $('#manager-tabs .key-system-tab:eq(0)').addClass('active');
    $('#manager-tab-content .key-system-section:eq(0)').addClass('active');
  });
};

/**
 * Prompts the user to create key matching the given fingerprint.
 *
 * @param {string} schemeName - name of scheme to create key for
 * @param {string} promptType - 'decrypt', 'encrypt', or 'standard'
 * @param {boolean} fullKeyOptions - whether to show the name and color chooser.
 * @param {string} fingerprint - if supplied, created key must match.
 * @promise {KeySystem} - key system created.
 * @reject {Object<string, string>} - error message if scheme was not found.
 */
KeyManager.prototype.createKey = function (schemeName, promptType, fullKeyOptions, fingerprints) {

  var uiManager = this.uiManagers[schemeName];

  if (uiManager) {
    return uiManager.addKeySystem(promptType, fullKeyOptions, fingerprints);
  } else {
    return Promise.reject(new Errors.KeyManagerError('No scheme found by that name.'));
  }
}

KeyManager.prototype.handleError = function () {
  // TODO
};

/**
 * Gets a key system given a fingerprint.
 *
 * @param {string} fingerprint - key fingerprint to find.
 * @promise {KeySystem} - key system with given fingerprint.
 */
KeyManager.prototype.getKeySystem = function (fingerprint) {
  return KeyStorage.get(fingerprint);
};

/**
 * Gets a key system given an Id.
 *
 * @param {KeyPrototypes.Identifier} id - id of the key to find.
 * @promise {KeySystem} - key system with given id.
 */
KeyManager.prototype.getKeySystemByIds = function (id, threadId) {
  var self = this;
  return new Promise(function(resolve, reject) {
    KeyStorage.getAllAttributes().then(function(attributesArray) {
      var found = false;
      for(var i = 0; i < attributesArray.length; i++) {
        var attributes = attributesArray[i];
        if(attributes.id.value == id.value && attributes.threadId == threadId) {
          found = true;
          self.getKeySystem(attributes.fingerprint).then(function(system) {
            system.attributes.publicKey = system.publicKey;
            resolve(system);
          });
        }
      }
      if(!found) {
        reject(new Errors.KeyNotFoundError('The key with the given ids: ' + id.serialize() + ' ' + threadId + ' was not found.'));
      }
    });
  });
};

KeyManager.prototype.storeSymmetricKey = function(messageKey, messageId, email, encrypted) {
  return Storage.local.setItem(messageId, JSON.stringify({messageKey: messageKey, email: email, encrypted: encrypted}));
};

KeyManager.prototype.retrieveSymmetricKey = function(messageId, email) {
  return Storage.local.getItem(messageId).then(function(entry) {
      return JSON.parse(entry).messageKey;
  }).catch(function(e) {
    return e;
  });
};

KeyManager.prototype.deleteSymmetricKey = function(messageId) {
  return Storage.local.removeItem(messageId);
};

KeyManager.prototype.hasSymmetricKey = function(messageId, email) {
  return Storage.local.getItem(messageId).then(function(entry) {
    if(entry && JSON.parse(entry).email == email) {
      return true
    }
    return false;
  }).catch(function(e) {
    return false;
  });
};

KeyManager.prototype.associateThreadIds = function (urlThreadId, threadId) {
  return Storage.local.setItem('thread-id-association:' + urlThreadId, threadId);
};

KeyManager.prototype.getAssociatedThreadId =  function (urlThreadId) {
  return Storage.local.getItem('thread-id-association:' + urlThreadId);
};

KeyManager.prototype.setExtensionInitialized = function () {
  return Storage.local.setItem('extension-initialized', 'true');
};

KeyManager.prototype.extensionInitialized =  function () {
  return Storage.local.getItem('extension-initialized').then(function(entry) {
    if(entry)
      return true;
    else
      return false;
  });
};

KeyManager.prototype.storeKeyRequest = function(messageId, email) {
  return Storage.local.setItem('key-request-made:' + messageId, email);
};

KeyManager.prototype.hasMadeKeyRequest = function(messageId, email) {
  return Storage.local.getItem('key-request-made:' + messageId).then(function(entry) {
    if(email == entry) 
      return true;
    else
      return false;
  }).catch(function(error) {
    return false;
  });
};

KeyManager.prototype.storeKeyResponse = function(messageId, email) {
  return Storage.local.setItem('key-response-sent:' + messageId, email);
};

KeyManager.prototype.hasSentKeyResponse = function(messageId, email) {
  return Storage.local.getItem('key-response-sent:' + messageId).then(function(entry) {
    if(email == entry) 
      return true;
    else
      return false;
  }).catch(function(error) {
    return false;
  });
};

KeyManager.prototype.hasDestroyedPublicSLK = function (emailAddress, threadId) {
  return Storage.local.getItem('has-destroyed-public-slk:' + emailAddress + ':' + threadId);

};

KeyManager.prototype.setHasDestroyedPublicSLK =  function (emailAddress, threadId) {
  return Storage.local.setItem('has-destroyed-public-slk:' + emailAddress + ':' + threadId, 'true');
};

/**
 * Gets all attributes from storage.
 *
 * @promise {KeyAttributes[]} - all key system attributes.
 */
KeyManager.prototype.getKeyAttributes = function () {
  return KeyStorage.getAllAttributes();
};

/**
 * Gets all selectable systems from key storage.
 *
 * @promise {KeySystem[]} - all selectable key systems.
 */
KeyManager.prototype.getSelectableSystems = function () {
  return KeyStorage.getAll().then(function (systems) {
    return systems.filter(function (keySystem) {
      return keySystem.attributes.canSelect;
    });
  });
};

KeyManager.prototype.generateSLK = function(emailAddress, threadId, keyData) {

  return KeySchemes.getScheme('SLK').create(emailAddress, threadId).then(function (newKey) {
    if(keyData)
      KeySchemes.getScheme('SLK').applySyncToSystem(newKey, keyData.created, keyData.lifespan);
    return KeyStorage.add(newKey).then(function() {
      return newKey.attributes;
    });
  });
};

KeyManager.prototype.destroySLK = function(emailAddress, threadId) {
  
  var self = this;
  var id = new Identifier();
  id.value = emailAddress;
  id.type = 'Email';
  return this.getKeySystemByIds(id, threadId).then(function(slkSystem) {
    if(slkSystem) {
      slkSystem.privateKey = 'null';
      //slkSystem.publicKey = null;
      //slkSystem.attributes.publicKeyDestroyed = true;
      slkSystem.attributes.privateKeyDestroyed = true;
      return KeyStorage.update(slkSystem).then(function() {
        return self.getThreadMetadata(slkSystem.attributes.threadId);
      });
    }
  });
};

KeyManager.prototype.destroyPublicSLK = function(emailAddress, threadId) {
  
  var id = new Identifier();
  id.value = emailAddress;
  id.type = 'Email';
  return this.getKeySystemByIds(id, threadId).then(function(slkSystem) {
    if(slkSystem) {
      slkSystem.privateKey = 'null';
      slkSystem.publicKey = 'null';
      slkSystem.attributes.publicKeyDestroyed = true;
      return KeyStorage.update(slkSystem);
    }
  });
};

KeyManager.prototype.removeSLKCompletely = function(emailAddress, threadId) {
  
  var id = new Identifier();
  id.value = emailAddress;
  id.type = 'Email';
  return this.getKeySystemByIds(id, threadId).then(function(slkSystem) {
    if(slkSystem) {
      return KeyStorage.remove(slkSystem);
    }
  });
};

KeyManager.prototype.threadHasDestroyedPublicKeys = function(emailAddress, threadId) {
  var self = this;
  return new Promise(function(resolve) {
    KeyStorage.getAllAttributes().then(function(attributesArray) {
      var found = false;
      for(var i = 0; i < attributesArray.length; i++) {
        var attributes = attributesArray[i];
        if(attributes.threadId == threadId && (attributes.publicKeyDestroyed || attributes.privateKeyDestroyed)) {
          found = true;
          resolve(true);
        }
      }
      if(!found) {
        resolve(false);
      }
    });
  });
};

KeyManager.prototype.persistSLK = function(emailAddress, threadId) {
  
  var id = new Identifier();
  id.value = emailAddress;
  id.type = 'Email';
  return this.getKeySystemByIds(id, threadId).then(function(slkSystem) {
    if(slkSystem) {
      KeySchemes.getScheme('SLK').applyPersistToSystem(slkSystem);
      return KeyStorage.update(slkSystem);
    }
  });
};

KeyManager.prototype.syncSLKExpiration = function(emailAddress, threadId, createdUTC, lifespanUTC) {
  var id = new Identifier();
  id.value = emailAddress;
  id.type = 'Email';
  return this.getKeySystemByIds(id, threadId).then(function(slkSystem) {
    if(slkSystem) {
      KeySchemes.getScheme('SLK').applySyncToSystem(slkSystem, createdUTC, lifespanUTC);
      return KeyStorage.update(slkSystem);
    }
  });
};

KeyManager.prototype.getMultipleExpirationData = function(emailAddress, threadIds) {
  var self = this;
  var keyAttributes = {};
  return new Promise(function(resolve) {
    KeyStorage.getAllAttributes().then(function(attributesArray) {
      for(var i = 0; i < attributesArray.length; i++) {
        var attributes = attributesArray[i];
        if(attributes.id.value == emailAddress && threadIds.includes(attributes.threadId)) {
          attributes.publicKey = '';
          keyAttributes[attributes.threadId] = attributes;
        }
      }
      resolve({
        keyAttributes: keyAttributes,
        originalThreadIds:  threadIds
      });
    });
  });
};



KeyManager.prototype.expireAllKeys = function() {

  var self = this;
  var expirePromises = [];
  var expiredCount = 0;

  return new Promise(function(resolve, reject) {
    KeyStorage.getAllAttributes().then(function(attributesArray) {

      attributesArray.forEach(function(attributes) {
        expirePromises.push(self.getKeySystem(attributes.fingerprint).then(function(system) {
            system.attributes.lifespan = 0;
            expiredCount++;
            return KeyStorage.update(system);
          })
        );
      })
    }).then(function() {
      Promise.all(expirePromises).then(function() {resolve(expiredCount)});
    }).catch(function(error) {
      reject(new Error('Error in getExpiredData', error));
    });
  });
};

KeyManager.prototype.getExpiredData = function (emailAddress) {

  var self = this;
  var expiredData = [];
  var metadataPromises = [];

  return new Promise(function(resolve, reject) {
    KeyStorage.getAllAttributes().then(function(attributesArray) {
      attributesArray.forEach(function(attributes) {
        if(attributes.id.value == emailAddress && !attributes.publicOnly && !attributes.privateKeyDestroyed && 
              Time.isExpired(attributes.created, attributes.lifespan)) {
          var data = {};
          data.created = attributes.created;
          metadataPromises.push(self.getThreadMetadata(attributes.threadId).then(function(metadata) {
            data.metadata = metadata;
            expiredData.push(data);
          }));
        }
      });
    }).then(function() {
      Promise.all(metadataPromises).then(function() {
        resolve(expiredData);
      });
    }).catch(function(error) {
      reject(error);
    });
  });
};

KeyManager.prototype.storeThreadMetadata = function(metadata) {
  return Storage.local.getItem('thread-metadata:' + metadata.threadId).then(function(entry) {
    if(entry) {
      var newEntry = JSON.parse(entry);
      if(!newEntry.urlThreadId) {
        newEntry.urlThreadId = metadata.urlThreadId;
        return Storage.local.setItem('thread-metadata:' + metadata.threadId, JSON.stringify(newEntry));
      }
    }
    else {
      return Storage.local.setItem('thread-metadata:' + metadata.threadId, JSON.stringify(metadata));
    }
  });
};

KeyManager.prototype.getThreadMetadata = function(threadId) {
  return Storage.local.getItem('thread-metadata:' + threadId).then(function(entry) {
      return JSON.parse(entry);
  }).catch(function(e) {
    return e;
  });
};

KeyManager.prototype.getSLK = function(emailAddress, threadId) {
  var id = new Identifier();
  id.value = emailAddress;
  id.type = 'Email';
  return this.getKeySystemByIds(id, threadId).then(function(system) {
    system.attributes.publicKey = system.publicKey;
    return system.attributes;
  });
};

KeyManager.prototype.storeSLK = function(keyData) {
  var id = new Identifier();
  id.value = keyData.id.value;
  id.type = keyData.id.type;
  var system = KeySchemes.getScheme('SLK').createPublic(keyData);

  return KeyStorage.add(system);

};

KeyManager.prototype.encryptDraft = function (message) {
  return KeyStorage.getDraftEncryptionKey().then(function (key) {
    return Encryptor.encrypt(message, key);
  });
};

KeyManager.prototype.decryptDraft = function (encryptedMessage) {
  return KeyStorage.getDraftEncryptionKey().then(function (key) {
    return Encryptor.decrypt(encryptedMessage, key);
  });
};

KeyManager.prototype.addEventListener = function () {
  // TODO
};

KeyManager.prototype.ensureKeyStoragePasswordPresent = function () {
  return KeyStorage.ensureMasterPasswordPresent();
};

KeyManager.prototype.checkStorageMasterPassword = function (password) {
  return KeyStorage.checkMasterPassword(password);
};

KeyManager.prototype.setStorageMasterPassword = function (password) {
  return KeyStorage.setMasterPassword(password);
};

KeyManager.prototype.promptMasterPassword = function (originalError) {
  var self = this;

  var modal           = $('#master-password-modal'),
      modalHeader     = modal.find('.master-password-header').text(''),
      promptField     = modal.find('.master-password-prompt').text(''),
      passwordField   = modal.find('.master-password').off().val(''),
      passwordConfirm = modal.find('.master-password-confirm').off().val(''),
      submitButton    = modal.find('.create-btn').off().prop('disabled', false),
      errorContainer  = modal.find('.alert').hide(),
      resetCheckbox   = modal.find('.alert .alert-text #master-password-reset').off().prop('checked', false);

  var passwordFields = passwordField.add(passwordConfirm);

  var headerText, promptText;

  if (originalError instanceof Errors.MasterPasswordMissing) {
    headerText = 'Set up your master password';
    promptText =
        'You must set up a master password in order to protect your encryption keys. ' +
        'You will need to re-enter this password whenever you restart your browser.' +
        '<br><br><span style="color:red">If you have created a MessageGuard.io account, ' +
        'this master password <i>should be different from that account\'s password.</i></span>';
  } else if (originalError instanceof Errors.MasterPasswordNeedsReentering) {
    headerText = 'Re-enter your master password';
    promptText = 'You need to re-enter your master password in order to unlock MessageGuard.';
  } else {
    headerText = 'Enter your master password to decrypt your keys';
    promptText = 'You must enter a master password to unlock MessageGuard.';
  }

  modalHeader.html(headerText);
  promptField.html(promptText);

  return new Promise(function (resolve) {
    modal.modal('show');

    passwordFields.keypress(function (e) {
      if (e.which == 13 && !submitButton.is(':disabled')) {
        submitButton.click();
      }
    });

    passwordFields.on('input', function () {
      var isSaveable = passwordField.val() == passwordConfirm.val() && passwordField.val() != '';
      submitButton.prop('disabled', !isSaveable);
    }).trigger('input');

    submitButton.click(function () {
      var password = passwordField.val();
      errorContainer.hide();
      submitButton.prop('disabled', true);
      self.checkStorageMasterPassword(password).then(function (isValid) {
        if (isValid) {
          return resolve(password);
        }

        if (resetCheckbox.is(':checked')) {
          KeyStorage.clearAll().then(function () {
            resolve(password);
          });
        }

        errorContainer.show();
        submitButton.prop('disabled', false);
      });
    });
  }).then(function (password) {
    return self.setStorageMasterPassword(password);
  }).then(function () {
    modal.modal('hide');
  });
};

module.exports = new KeyManager();