var KeyManager = require('./key-manager'),
    Identifier =  require('./key-prototypes').Identifier,
    ReceiverTasks = function () { return require('./communicator/receiver/receiver-tasks'); };

ReceiverTasks = ReceiverTasks();

var id = new Identifier();
id.type = "Email";
//id.value = "monson@isrl.byu.edu";
id.value = "monson@isrl.byu.edu";

var fingerprint;

//var keyData = '{"scheme":"SLK","encryptedData":"WyI5TDdHZHlaWDI3dlpZbnh4WHpzOHdBPT0iLCJLcE1jU3QyQkRsSFZvb0phTlkxVm1wVVBJU1laOWdWeGdGbTBDdG5uaWVXRWo1dWFrbzUvOWc4aDZTUUZmMTVwUVk2UlJKS1BoRFpXTURPVThrVXduT0pWRC85dXpYME42Wkx5Nk5nOFB3ek1Xa05DIl0=","encryptedKeys":[],"messageIds":[{"messageId":"c001d092-2d92-44f1-bd0e-41225b5e2383","email":"monson@isrl.byu.edu"},{"messageId":"51ee4100-ea91-4e4c-bdc7-620949b88a64","email":"princenosnow@gmail.com"}],"sender":"princenosnow@gmail.com","keyData":{"id":{"type":"Email","value":"princenosnow@gmail.com"},"fingerprint":"eMiXkP/ClKlNhNnnBcNTG1tiOwQ=","name":"","color":"black","scheme":"SLK","canSelect":true,"canHaveRecipients":true,"storage":"local","publicKey":"-----BEGIN PGP PUBLIC KEY BLOCK-----\r\nVersion: OpenPGP.js v2.2.0\r\nComment: http://openpgpjs.org\r\n\r\nxsBNBFk5eOoBCACOq+0Fl1gfS8LggpAbOnWuOaGnsPENlHl1kUFDbBB5e/VO\nevxxtfuQdhtNzQLTnoTMfUqtuRVLk3Ec6L1MmUH/kUZOahpKJJw8hrQRyLRu\nimpSurZ+RdMpkTcZYGljbC/wnoXZJhpV3n5i/6ggjsAD/OWcDXSzC8Zjt7eR\nzcUXwfNiYkdL0/+Zx6V9njq1JKdTCdEo0m1gvN9ynBkxOa/pLunb3mmGc9yq\nxM/v+UVphzOpI/5oFLpSZQQ1bGCn2jvUd/1D/2gFpIrUhTngLVrxeSsbc6iR\n2oqS7e6iw+TL0IIO/Ewm4/74j/yACwAvwIvFPScqwykm3luMMDJTedDHABEB\nAAHNH0VtYWlsOnByaW5jZW5vc25vd0BnbWFpbC5jb20gPD7CwHUEEAEIACkF\nAlk5ePMGCwkHCAMCCRCLxSRLAXh06wQVCAIKAxYCAQIZAQIbAwIeAQAAYLwH\n/i7RvD8iV10viFcU+hWdGrJAWQnAJB6k5Pn9zO7q/BYn/czWH8qmPTWeR1be\n/oUuyVPaqutPCo2GtpxreYVO0SFXprQpyouJXTSdK18vrr96aI4nT/TcJ1sh\n/7A0kn2FwOBXj8+XkGsRcpO7vQ6izdQbUi8d84+BgLrbZrhLEdhABMzEY01y\ni8RNfkIepifgnwTgI+CnGU9OYAgcDfk3Y1/MtIuwDkzlhA80tb3Wr3EsRKGW\n4rhdlvl+ytl/gyAhFQ2wqty37rRaZtvKONRe3A2wVDqk/X5rHDdQTOccL79z\nDCAMhMqRXe9i0JWI5ZuNl/xv7dAQ9tUAO8qRjYB9W3bOwE0EWTl47wEIAIGx\ndp4wKnFVd1Q94q4FTfyDErxdj+TyelA35+T5ul/XgPuSm3Wbi2lSSC8ENInS\nG5+9EApluPmb+2aAmfjquSa/dcfNipTKzy9RMJSyQl+5ClhA5aScWpWM05qg\nFl9dyUqqWa2I/FWRdn0BDgxr4MpEl/P0rZySVt2/9LW+KOasT3PvOPMZ+HZJ\nndUKBUGwL7Qi2A7WtpoRSrQcK7dqSpfAIn0WeE8JslcABf27m22AFqU+V/Bg\ngJTg3hPHYKG0ljG02SHdytoroJhWwuUMy/6qkWhRTK43NZXM3zjcg9SBmsV7\n3MbW7iGDQvXF1pX9vO4Iq7LFY7Ldusmig5+3BGEAEQEAAcLAXwQYAQgAEwUC\nWTl48wkQi8UkSwF4dOsCGwwAAFp+B/9KaTcvYf6a9vxIgHdCwgiCIlwRfSrO\nhQQKVdNhU6Dn5DqP2BnzRcKWAN2RjPhmYxkS+UTMcIfKKLVb5tIhFVxJc9BX\nW5xy9/f9NK97h91tkISyEMAySggslZLyqaKFFrt5kJG4xvzF60yHb0XqYcel\nm6ffgkTeRZTWQ7b4LqzJ4GooN5FDS2HvWZNt8SETWRCYU6wjF7BZ1MW3Nwg9\nfKqTbuIKoQJly3gyv47mU2bc2M1zIHrtOjCAQ/J99vMGFa/rPke6rbOkvBjb\naLsZMnXYJVHY7Dr5JRV/8oa5vfudgMzXTRyecOYQmMTL4AFlLMeWAXpO4SQy\nJmRzJR9IK8QV\r\n=iu6m\r\n-----END PGP PUBLIC KEY BLOCK-----\r\n\r\n","created":1496938738979,"lifespan":1440014,"expired":false,"privateKeyUsed":false,"usesEncryptedStorage":false,"publicOnly":false,"threadId":"f0e2e9c2-c14d-4db7-b553-609344311d7b"}}';
//keyData = JSON.parse(keyData);

/*KeyManager.generateSLK("princenosnow@gmail.com", "fcf13307-b11a-4c67-be1c-5a7d715bbe5c").then(function(princeAttributes) {
    console.log(princeAttributes);
    return KeyManager.generateSLK("monson@isrl.byu.edu", "fcf13307-b11a-4c67-be1c-5a7d715bbe5c").then(function(monsonAttributes) {
        return KeyManager.syncSLKExpiration("monson@isrl.byu.edu", "fcf13307-b11a-4c67-be1c-5a7d715bbe5c", princeAttributes.created, princeAttributes.lifespan);
    }).then(function() {
        return KeyManager.getMultipleExpirationData('monson@isrl.byu.edu', ["fcf13307-b11a-4c67-be1c-5a7d715bbe5c"]);
    });
}).then(function(expirationData) {
  console.log(expirationData);  
});*/

KeyManager.generateSLK("princenosnow@gmail.com", "fcf13307-b11a-4c67-be1c-5a7d715bbe5c").then(function(princeAttributes) {
    console.log(princeAttributes);
    return KeyManager.generateSLK("monson@isrl.byu.edu", "fcf13307-b11a-4c67-be1c-5a7d715bbe5c").then(function(monsonAttributes) {
        return KeyManager.expireAllKeys();
    }).then(function(count) {
        console.log(count);
    });
});

/*KeyManager.hasDestroyedPublicSLK("abc", "123").then(function(has) {
    console.log(has);
    return KeyManager.setHasDestroyedPublicSLK("abc", "123");
}).then(function() {
    return KeyManager.hasDestroyedPublicSLK("abc", "123");
}).then(function(has) {
    console.log(has);  
});*/

/*KeyManager.getExpiredData("princenosnow@gmail.com").then(function(expiredData) {
    console.log(expiredData);
}).catch(function(error) {
    return error;
});*/



