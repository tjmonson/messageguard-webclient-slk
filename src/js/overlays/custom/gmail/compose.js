/**
 * Generic compose class
 * @module
 * @requires bower_components/tether
 * @requires common/jquery-bootstrap
 * @requires overlays/overlay-base
 * @requires common/message-types
 * @requires overlays/wysiwyg
 */

"use strict";

window.Tether = require('tether');

var $              = require('../../../common/jquery-bootstrap'),
    MessageType    = require('../../../common/message-type'),
    UUID    = require('../../../common/uuid'),
    GenericCompose = require('../../generic-compose'),
    KeyManager  = require('../../../key-management/communicator/emitter');

/**
 * Controls the generic compose interface
 * @constructor
 * @alias module:overlays/generic-compose
 */
function GmailCompose() {
  this.saveDraftInterval = 5000;
  this.lastSavedDraft = '';
  this.threadId = UUID();
  this.slkAttributes = null;
}

GmailCompose.prototype = new GenericCompose();
GmailCompose.prototype.constructor = GmailCompose;

/**
 * Initialize and begin operating
 */
GmailCompose.prototype.init = function () {
  GenericCompose.prototype.init.call(this);
  var self = this;
  this.shouldSaveDraft = true;
  this.editor = this.getEditor();

  this.registerMessageHandler(MessageType.SEND_CONTENTS, true, function (data) {
    console.log(data);
    self.composeData = data;
    self.userId = data.userId;

    if (!data.allowCancel) {
      $('.cancel-btn').remove();
    }

    self.keyManagerReady.then(function() {
      return KeyManager.getAssociatedThreadId(data.urlThreadId);
    }).then(function(threadId) {
        
      self.hideMaskedMessage();

      if (threadId) {
        self.threadId = threadId;
        self.getEncryptionKeyPromise = KeyManager.getSLK(data.userId, self.threadId).then(function(slkAttributes) {
          self.slkAttributes = slkAttributes;
          return KeyManager.threadHasDestroyedPublicKeys(data.userId, self.threadId);
        }).then(function(threadHasDestroyedPublicKeys) {
          console.log(threadHasDestroyedPublicKeys);
          if(threadHasDestroyedPublicKeys) {
            self.cannotComposeMode();
          }
        });
        return;
      }
      else {
        self.getEncryptionKeyPromise = KeyManager.generateSLK(data.userId, self.threadId).then(function(slkAttributes) {
          self.slkAttributes = slkAttributes;
        });
      }

      if (data.package) {
        self.keyManagerReady.then(function () {
          return self.performDecryption(data.package);
        }).then(function (decryptedPackage) {
          self.setEditorContent(data.body.replace(data.templateItem, decryptedPackage));
          self.updateView();

          self.hideMaskedMessage();
          window.setTimeout(self.saveDraft.bind(self), self.saveDraftInterval);
        }).catch(function (error) {
          self.showMaskedMessage(error, false, true, true);
        });
      } else {
        self.setEditorContent(data.body);
        self.updateView();

        //Setup save draft interval

        window.setTimeout(self.saveDraft.bind(self), self.saveDraftInterval);
      }
    });
  });

  this.registerMessageHandler(MessageType.FOCUS_CHANGED, false, function (data) {
    this.getEditor().focus();
  });

  this.getEditor().keydown(function (e) {
    if ((e.keyCode || e.which) == 9 && e.shiftKey) {
      e.preventDefault();
      self.postMessage(MessageType.FOCUS_CHANGED);
    }
  });

  this.registerMessageHandler(MessageType.TUTORIAL_MASK_PLACE, false, function () {
    self.placeKeyButtonTutorialMask();
  });

  this.registerMessageHandler(MessageType.TUTORIAL_MASK_REMOVE, false, function () {
    self.removeKeyButtonTutorialMask();
  });
};

/**
 * We modify this method to change message showing and hiding.
 * @private
 */
GmailCompose.prototype._waitForKeyManager = function () {
  var self = this;
  return GenericCompose.prototype._waitForKeyManager.call(this).then(function () {
    self.showMaskedMessage('Initializing &hellip;', true);
  });
};

GmailCompose.prototype.cannotComposeMode = function () {
  console.log("Entering cannot compose mode.");
  this.shouldSaveDraft = false;
  $('#large').hide();
  $('#compose-error').show();
  $("#compose-message").text("You can no longer send encrypted messages on this thread.");
  this.postMessage(MessageType.CANNOT_COMPOSE, null);
  this.postMessage(MessageType.SIZING_INFO, $('#compose-error').height());
};

/**
 * Set the editor's content. Look for quotes that we want to minimze.
 * @param {string} content Editor's content.
 */
GmailCompose.prototype.setEditorContent = function (content) {
  var contentElement = $('<div>' + content + '</div>');
  $('.gmail_extra', contentElement).remove();
  GenericCompose.prototype.setEditorContent.call(this, contentElement.html());
};

/**
 * Posts a GET_RECIPIENTS message to the overlay manager asking for an array of recipients
 * @return {Promise} A promise that will resolve with an array of recipient strings
 */
GmailCompose.prototype.getRecipients = function () {
  var self = this;
  if(this.metadata)
    return Promise.resolve(this.metadata);
  else {
    return new Promise(function (resolve) {
      self.registerMessageHandler(MessageType.GET_RECIPIENTS, true, function (recipients) {
        resolve(recipients);
      });
      self.postMessage(MessageType.GET_RECIPIENTS);
    });
  }
};

GmailCompose.prototype.getMetadata = function () {
  var self = this;
  return new Promise(function (resolve) {
    self.registerMessageHandler(MessageType.GET_METADATA, true, function (metadata) {
      resolve(metadata);
    });
    self.postMessage(MessageType.GET_METADATA);
  });
};

GmailCompose.prototype.storeMetadata = function (metadata) {
  var self = this;
  metadata.threadId = this.threadId;
  return self.keyManagerReady.then(function () {
    return KeyManager.storeThreadMetadata(metadata);
  });
};

GmailCompose.prototype.removeGeneratedSLK = function () {
  var self = this;

  if(self.composeData.isEncryptedReply)
    return Promise.resolve();

  return this.getEncryptionKeyPromise.then(function(attributes) {
    return KeyManager.removeSLKCompletely(self.userId, self.threadId);
  });
};

GmailCompose.prototype.placeKeyButtonTutorialMask = function () {
  $('#key-button, #key-dropdown').filter(':visible')
      .addClass('tutorial-focus')
      .after('<div class="tutorial-mask"></div>');
};

GmailCompose.prototype.removeKeyButtonTutorialMask = function () {
  $('.tutorial-focus').removeClass('tutorial-focus');
  $('.tutorial-mask').remove();
};

GmailCompose.prototype.saveDraft = function () {
  if(!this.shouldSaveDraft)
    return;
  var editorContent = this.getEditorContent();

  // Only save the draft if the content has changed.
  if (this.lastSavedDraft === editorContent || !this.canSaveDraft) {
    if (this.canSaveDraft) {
      window.setTimeout(this.saveDraft.bind(this), this.saveDraftInterval);
    }

    return;
  }

  // Save the fraft.
  var self = this;
  this.encryptDraft(editorContent).then(function (encryptedData) {
    if (self.canSaveDraft) {
      self.postMessage(MessageType.SAVE_DRAFT, {
        package: encryptedData,
        threadId: self.threadId
      });
      window.setTimeout(self.saveDraft.bind(self), self.saveDraftInterval);
    }
  }).catch(function () { });
};

GmailCompose.prototype.bindUIActions = function () {
  GenericCompose.prototype.bindUIActions.call(this);
  var self = this;
  $('#discard-draft-btn').click(function () {
    self.postMessage(MessageType.DISCARD_DRAFT);
  });
};

var overlay = new GmailCompose();
overlay.init();
