/**
 * Generic read class.
 * @module
 * @requires bower_components/tether
 * @requires common/jquery-bootstrap
 * @requires overlays/overlay-base
 * @requires common/message-type
 */
'use strict';

window.Tether = require('tether');

var $           = require('../../../common/jquery-bootstrap'),
    GenericRead = require('../../generic-read'),
    MessageType = require('../../../common/message-type'),
    sanitizer   = require('../../../common/html-sanitizer'),
    encoder   = require('../../../common/encoder'),
    KeyManager        = require('../../../key-management/communicator/emitter');

/**
 * Controls the generic read interface
 * @constructor
 * @alias module:overlays/generic-read
 */
function GmailResponse() {

}

GmailResponse.prototype = new GenericRead();
GmailResponse.prototype.constructor = GmailResponse;

/**
 * Initialize and begin operating
 */
GmailResponse.prototype.init = function () {
  var self = this;

  GenericRead.prototype.init.call(this);

  this.registerMessageHandler(MessageType.SIZING_INFO, false, function () {
    self.postMessage(MessageType.SIZING_INFO, self.getHeight());
  });

  this.uuid = this.getQueryStringParam('uuid');
  //this.updateView();
  this.bindUIActions();

  //REGISTER MESSAGE HANDLERS

  // Add tooltips
  $('[title]').tooltip({
                         container: 'body'
                       }).on('click', function () {
    $(this).tooltip('hide');
  });

  this.registerMessageHandler(MessageType.TUTORIAL_MASK_PLACE, false, function () {
    self.placeKeyButtonTutorialMask();
  });

  this.registerMessageHandler(MessageType.TUTORIAL_MASK_REMOVE, false, function () {
    self.removeKeyButtonTutorialMask();
  });
};

GmailResponse.prototype._showOverlay = function () {
  GenericRead.prototype._showOverlay.apply(this, arguments);
  this.postMessage(MessageType.SIZING_INFO, this.getHeight());
};

GmailResponse.prototype.sendContentsHandler = function(data) {
  var self = this;

  this.showMaskedMessage('Processing request &hellip;', {showSpinner: true});

  if(data.userId == data.sender) {
    $("#response-message").text("Access response");
      $("#no-action-necessary").text("No action necessary. This message was sent automatically by MessageGuard.");
      $("#no-action-necessary").show();
    this.hideMaskedMessage();
    self.postMessage(MessageType.SIZING_INFO, self.getHeight());
    return;
  }
  console.log(data);

  self.keyManagerReady.then(function () {
    return KeyManager.storeSymmetricKey(data.messageId, data.encryptedMessageKey, data.sender);
  }).then(function() {
    $("#response-message").text("Access granted! You can now access the encrypted message " + 
      data.sender + " sent you earlier.");
    $("#no-action-necessary").text("No action necessary. This message was processed automatically by MessageGuard.");
    $("#no-action-necessary").show();
    self.hideMaskedMessage();
    self.postMessage(MessageType.REFRESH_READ_OVERLAYS);
    self.postMessage(MessageType.SIZING_INFO, self.getHeight());
    console.log("Symmetric key successfully stored.");
  });
};

GmailResponse.prototype.placeKeyButtonTutorialMask = function () {
  if ($('#large').is(':visible')) {

    $('.key-btn').filter(':visible')
        .addClass('tutorial-focus')
        .after('<div class="tutorial-mask"></div>');

  } else if ($('#small').is(':visible')) {

    $('.key-btn').filter(':visible').parent().parent().addClass('tutorial-focus');
    $('#large, #small').filter(':visible').append('<div class="tutorial-mask"></div>');

  }
};

GmailResponse.prototype.unwrapMGPackage = function(data) {
  return JSON.parse(encoder.convert(data.replace('|mg|', ''), 'base64', 'utf8'));
};

GmailResponse.prototype.removeKeyButtonTutorialMask = function () {
  $('.tutorial-focus').removeClass('tutorial-focus');
  $('.tutorial-mask').remove();
};

GmailResponse.prototype.getHeight = function() {
  return $("#response").height() || 0;
};

/**
 * Binds updateView to window resize UI action
 */
GmailResponse.prototype.bindUIActions = function () {
  //$(window).resize(this.updateView);
};

var overlay = new GmailResponse();
overlay.init();
