/**
 * Generic read class.
 * @module
 * @requires bower_components/tether
 * @requires common/jquery-bootstrap
 * @requires overlays/overlay-base
 * @requires common/message-type
 */
'use strict';

window.Tether = require('tether');

var $           = require('../../../common/jquery-bootstrap'),
    GenericRead = require('../../generic-read'),
    MessageType = require('../../../common/message-type'),
    sanitizer   = require('../../../common/html-sanitizer'),
    encoder   = require('../../../common/encoder'),
    KeyManager        = require('../../../key-management/communicator/emitter');

/**
 * Controls the generic read interface
 * @constructor
 * @alias module:overlays/generic-read
 */
function GmailRequest() {

}

GmailRequest.prototype = new GenericRead();
GmailRequest.prototype.constructor = GmailRequest;

/**
 * Initialize and begin operating
 */
GmailRequest.prototype.init = function () {
  var self = this;

  GenericRead.prototype.init.call(this);

  this.registerMessageHandler(MessageType.SIZING_INFO, false, function () {
    self.postMessage(MessageType.SIZING_INFO, self.getHeight());
  });

  this.uuid = this.getQueryStringParam('uuid');
  //this.updateView();
  this.bindUIActions();

  //REGISTER MESSAGE HANDLERS

  // Add tooltips
  $('[title]').tooltip({
                         container: 'body'
                       }).on('click', function () {
    $(this).tooltip('hide');
  });

  this.registerMessageHandler(MessageType.TUTORIAL_MASK_PLACE, false, function () {
    self.placeKeyButtonTutorialMask();
  });

  this.registerMessageHandler(MessageType.TUTORIAL_MASK_REMOVE, false, function () {
    self.removeKeyButtonTutorialMask();
  });

  
  this.showMaskedMessage('Processing request &hellip;', {showSpinner: true});

  $("#details-link").click(function() {
    var details = $("#details");
    var link = $('#details-link');
    if(details.is(':visible')) {
      details.hide();
      link.text('Show more details...');
    }
    else {
      details.show();
      link.text('Hide details');
    }
    self.postMessage(MessageType.SIZING_INFO, self.getHeight());
  });
};

GmailRequest.prototype._showOverlay = function () {
  GenericRead.prototype._showOverlay.apply(this, arguments);
  this.postMessage(MessageType.SIZING_INFO, this.getHeight());
};

GmailRequest.prototype.sendContentsHandler = function(data) {
  var self = this;
  self.requestData = data;

  if(data.userId == data.requester) {
    this.hideMaskedMessage();
    $('#request-message').text("Access Request");
    $("#no-action-necessary").text("No action necessary. This message was sent automatically by MessageGuard.");
    $("#no-action-necessary").show();
    $('#details-link').hide();
    self.postMessage(MessageType.SIZING_INFO, self.getHeight());
    return;
  }

  KeyManager.associateThreadIds(data.urlThreadId, self.requestData.keyData.threadId);

  $('#resend-response-btn').click(function() {
    self.sendResponse(true);
  });

  self.keyManagerReady.then(function () {
    return KeyManager.hasSentKeyResponse(data.messageId, data.requester);
  }).then(function(hasSentResponse) {
    if (hasSentResponse) {
      self.hideMaskedMessage();
      $('#details').text(data.requester + " received your encrypted message and sent this message " + 
        "to request access to it. MessageGuard automatically sent a message containing data granting " + data.requester + 
        " access to secure messages on this thread.");
      $("#no-action-necessary").text("No action necessary. This message was processed automatically by MessageGuard.");
      $("#no-action-necessary").show();
      $('#request-message').text("Access request from " + data.requester);
      $('#resend-bar').hide();
      self.postMessage(MessageType.SIZING_INFO, self.getHeight());
    }
    else {
      return KeyManager.storeSLK(data.keyData).then(function() {
        self.sendResponse(false);
      });     
    }
  });
};

GmailRequest.prototype.sendResponse = function(fromClickAction) {
  var data = this.requestData;
  var self = this;
    
  KeyManager.hasSymmetricKey(data.messageId, data.requester).then(function(hasKey) {
    if(hasKey) {
      console.log(data);
      return KeyManager.encryptRequestedKey(data.messageId, data.requester, data.userId, data.keyData.threadId);
    }
    else
      throw new Error("No symmetric key avaiable to send to fulfill key request.");
  }).then(function(encryptedData) {
    encryptedData.requester = data.requester;
    encryptedData.sender = data.userId;
    encryptedData.messageId = data.messageId;
    encryptedData.threadId = data.keyData.threadId;
    self.postMessage(MessageType.SEND_REQUEST_RESPONSE, encryptedData);

    window.setTimeout(function() {
      $('#details').text(data.requester + " received your encrypted message and sent this message " + 
        "to request access to it. MessageGuard automatically sent a message containing data granting " + data.requester + 
        " access to secure messages on this thread.");
      $("#no-action-necessary").text("No action necessary. This message was processed automatically by MessageGuard.");
      $("#no-action-necessary").show();
      $('#request-message').text("Access request from " + data.requester);
      $('#resend-bar').hide();
      self.hideMaskedMessage();
      self.postMessage(MessageType.SIZING_INFO, self.getHeight());
    }, 1000);

    return KeyManager.storeKeyResponse(data.messageId, data.requester);

  }).catch(function(e) {
    $('#request-message').text("There was a problem processing this request.");
    window.setTimeout(function() {
      self.hideMaskedMessage();
          self.postMessage(MessageType.SIZING_INFO, self.getHeight());
    }, 1000);
    console.log(e);
  });
};

GmailRequest.prototype.placeKeyButtonTutorialMask = function () {
  if ($('#large').is(':visible')) {

    $('.key-btn').filter(':visible')
        .addClass('tutorial-focus')
        .after('<div class="tutorial-mask"></div>');

  } else if ($('#small').is(':visible')) {

    $('.key-btn').filter(':visible').parent().parent().addClass('tutorial-focus');
    $('#large, #small').filter(':visible').append('<div class="tutorial-mask"></div>');

  }
};

GmailRequest.prototype.unwrapMGPackage = function(data) {
  return JSON.parse(encoder.convert(data.replace('|mg|', ''), 'base64', 'utf8'));
};

GmailRequest.prototype.removeKeyButtonTutorialMask = function () {
  $('.tutorial-focus').removeClass('tutorial-focus');
  $('.tutorial-mask').remove();
};

GmailRequest.prototype.setToRequestDisplay = function() {
  $("#large").hide();
  $("#small").hide();
  $("#request").show();

  $('#request-send-btn').click(this.sendRequestClickAction.bind(this));
};

GmailRequest.prototype.getHeight = function () {
  return $('#request:visible').height() || 0;
};

GmailRequest.prototype.requestNoClickAction = function() {

};

/**
 * Binds updateView to window resize UI action
 */
GmailRequest.prototype.bindUIActions = function () {
  //$(window).resize(this.updateView);
};

var overlay = new GmailRequest();
overlay.init();
