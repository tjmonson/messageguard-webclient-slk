/**
 * SLK
 * @module
 * @requires bower_components/tether
 * @requires common/jquery-bootstrap
 * @requires overlays/overlay-base
 * @requires common/message-type
 */
'use strict';

var $           = require('../../../common/jquery-bootstrap'),
    communicatorMixin = require('../../../common/communicator-mixin'),
    KeyManager        = require('../../../key-management/communicator/emitter'),
    MessageType = require('../../../common/message-type'),
    sanitizer   = require('../../../common/html-sanitizer'),
    Errors      = require('../../../common/errors'),
    UUID      = require('../../../common/uuid');

/**
 * Controls the generic read interface
 * @constructor
 * @alias module:overlays/generic-read
 */
function SLKOverlay() {
  this.instanceUUID = UUID(); // for debugging
  communicatorMixin.call(this);
}

SLKOverlay.prototype.constructor = SLKOverlay;

/**
 * Initialize and begin operating
 */
SLKOverlay.prototype.init = function () {
    var self = this;

    window.addEventListener('message', function () {
        self.messagePort = event.ports[0];
        self.messagePort.onmessage = self.handleMessage.bind(self);
    }, false);

    var readyResolve = null;
    this.keyManagerReady = new Promise(function (resolve) {
        readyResolve = resolve;
    });

    this._waitForKeyManager().then(readyResolve);

    this.packager = undefined;

    // register message handlers
    this.registerMessageHandler(MessageType.GET_THREAD_EXPIRATION, false, this.getThreadExpiration);
    this.registerMessageHandler(MessageType.DESTROY_THREAD_KEY, false, this.destroyThreadKey);
    this.registerMessageHandler(MessageType.GET_EXPIRED_DATA, false, this.getExpiredData);
    this.registerMessageHandler(MessageType.PERSIST_THREAD_KEY, false, this.persistThreadKey);
    this.registerMessageHandler(MessageType.GET_MULTIPLE_EXPIRATION_DATA, false, this.getMultipleExpirationData);
};

SLKOverlay.prototype.getThreadExpiration = function(data) {

    var self = this;
    this.keyManagerReady.then(function() {
        return KeyManager.getAssociatedThreadId(data.urlThreadId);
    }).then(function(associatedThreadId) {
        if(associatedThreadId) {
            KeyManager.getSLK(data.userId, associatedThreadId).then(function(slkAttributes) {
                self.postMessage(MessageType.SEND_THREAD_EXPIRATION, {
                    created: slkAttributes.created,
                    privateKeyDestroyed: slkAttributes.privateKeyDestroyed,
                    lifespan: slkAttributes.lifespan,
                    currentURL: data.currentURL,
                    threadId: associatedThreadId
                });
            });
        }
        else 
           self.postMessage(MessageType.SEND_THREAD_EXPIRATION, null); 
    });
};

SLKOverlay.prototype.getMultipleExpirationData = function(data) {
    var self = this;
    this.keyManagerReady.then(function() {
        return KeyManager.getMultipleExpirationData(data.userId, data.threadIds);
    }).then(function(expirationData) {
        self.postMessage(MessageType.SEND_MULTIPLE_EXPIRATION_DATA, expirationData);
    });
};

SLKOverlay.prototype.destroyThreadKey = function(data) {

    var self = this;
    this.keyManagerReady.then(function() {
        return KeyManager.destroySLK(data.userId, data.threadId);
    }).then(function(threadMetadata) {
        data.threadMetadata = threadMetadata;
        self.postMessage(MessageType.THREAD_KEY_DESTROYED, data);
    });
};

SLKOverlay.prototype.getExpiredData = function(emailAddress) {
    var self = this;
    
    return this.keyManagerReady.then(function() {
        return KeyManager.getExpiredData(emailAddress);
    }).then(function(expiredData) {
        self.postMessage(MessageType.SEND_EXPIRED_DATA, expiredData);
    });
};

SLKOverlay.prototype.persistThreadKey = function(data) {
    var self = this;
    this.keyManagerReady.then(function() {
        return KeyManager.persistSLK(data.userId, data.threadId);
    }).then(function() {
        self.postMessage(MessageType.THREAD_KEY_PERSISTED, data);  
    });
};

SLKOverlay.prototype._waitForKeyManager = function() {
    var self = this;

  return KeyManager.ensureKeyManagerAlive().then(function () {

    return new Promise(function (resolve) {
      var tryInitialize = function () {
        return KeyManager.initializeKeys().then(resolve);
      };

      var tryCatch = function () {
        tryInitialize().catch(function (error) {
            console.log(error)
        });
      };

      tryCatch();
    });
  }).then(function () {
    self.keyManagerActive = true;
  });
};



var overlay = new SLKOverlay();
overlay.init();
