/**
 * Generic read class.
 * @module
 * @requires bower_components/tether
 * @requires common/jquery-bootstrap
 * @requires overlays/overlay-base
 * @requires common/message-type
 */
'use strict';

window.Tether = require('tether');

var $           = require('../../../common/jquery-bootstrap'),
    GenericRead = require('../../generic-read'),
    MessageType = require('../../../common/message-type'),
    sanitizer   = require('../../../common/html-sanitizer'),
    encoder   = require('../../../common/encoder'),
    KeyManager        = require('../../../key-management/communicator/emitter');

/**
 * Controls the generic read interface
 * @constructor
 * @alias module:overlays/generic-read
 */
function GmailDestroyKey() {

}

GmailDestroyKey.prototype = new GenericRead();
GmailDestroyKey.prototype.constructor = GmailDestroyKey;

/**
 * Initialize and begin operating
 */
GmailDestroyKey.prototype.init = function () {
  var self = this;

  GenericRead.prototype.init.call(this);

  this.registerMessageHandler(MessageType.SIZING_INFO, false, function () {
    self.postMessage(MessageType.SIZING_INFO, self.getHeight());
  });

  this.uuid = this.getQueryStringParam('uuid');
  //this.updateView();
  this.bindUIActions();

  //REGISTER MESSAGE HANDLERS

  // Add tooltips
  $('[title]').tooltip({
                         container: 'body'
                       }).on('click', function () {
    $(this).tooltip('hide');
  });

  this.registerMessageHandler(MessageType.TUTORIAL_MASK_PLACE, false, function () {
    self.placeKeyButtonTutorialMask();
  });

  this.registerMessageHandler(MessageType.TUTORIAL_MASK_REMOVE, false, function () {
    self.removeKeyButtonTutorialMask();
  });

  document.querySelector('#details-link').addEventListener('click', function() {
      var details = document.querySelector("#details");
      var link = document.querySelector('#details-link');
      if(details.style.display == "block") {
          details.style.display = "none";
          link.innerText = 'What does this mean?';
      }
      else {
          details.style.display = "block";
          link.innerText = 'Hide details';
      }
      self.postMessage(MessageType.SIZING_INFO, self.getHeight());
  });

  this.showMaskedMessage('Processing &hellip;', {showSpinner: true});
};

GmailDestroyKey.prototype._showOverlay = function () {
  GenericRead.prototype._showOverlay.apply(this, arguments);
  this.postMessage(MessageType.SIZING_INFO, this.getHeight());
};

GmailDestroyKey.prototype.sendContentsHandler = function(data) {
  var self = this;
  console.log(data);
  var sender = data.destructionData.destroyedBy;
  if(data.userId == sender) {
    this.showSameSenderText();
    return;
  }

  this.keyManagerReady.then(function() {
    return KeyManager.hasDestroyedPublicSLK(sender, data.destructionData.threadId);
  }).then(function(hasDestroyedPublicSLK) {
    if(!hasDestroyedPublicSLK) {
      return KeyManager.setHasDestroyedPublicSLK(sender, data.destructionData.threadId).then(function() {
        return KeyManager.destroyPublicSLK(sender, data.destructionData.threadId);
      });
    }
  }).then(function() {
    self.showReceivedText(sender);
  }).catch(function(error) {
    self.showReceivedText(sender);
  });
};

GmailDestroyKey.prototype.showSameSenderText = function() {
  $('#destroy').show();
  $('#destroy-message').text('You revoked your access to this secure email thread.');
  $('#point-one').text('You can no longer read messages from this secure thread.');
  $('#point-two').text('You can no longer send encrypted messages on this thread.');
  $('#point-three').text('Others with access to this secure thread may be able to read the secure messages until they revoke their own access.');
  this.postMessage(MessageType.SIZING_INFO, this.getHeight());
  this.hideMaskedMessage(); 
};

GmailDestroyKey.prototype.showReceivedText = function(sender) {
  $('#destroy').show();
  $('#destroy-message').text(sender + " has revoked their access to this secure email thread.");

  $('#point-one').text(sender + ' can no longer read messages from this secure thread.');
  $('#point-two').text('You can no longer send encrypted messages to ' + sender + ' on this thread.');
  $('#point-three').text('You can read secure messages on this thread until you revoke your own access to it.');

  this.hideMaskedMessage();  
  this.postMessage(MessageType.SIZING_INFO, this.getHeight());
};

GmailDestroyKey.prototype.placeKeyButtonTutorialMask = function () {
  if ($('#large').is(':visible')) {

    $('.key-btn').filter(':visible')
        .addClass('tutorial-focus')
        .after('<div class="tutorial-mask"></div>');

  } else if ($('#small').is(':visible')) {

    $('.key-btn').filter(':visible').parent().parent().addClass('tutorial-focus');
    $('#large, #small').filter(':visible').append('<div class="tutorial-mask"></div>');

  }
};

GmailDestroyKey.prototype.unwrapMGPackage = function(data) {
  return JSON.parse(encoder.convert(data.replace('|mg|', ''), 'base64', 'utf8'));
};

GmailDestroyKey.prototype.removeKeyButtonTutorialMask = function () {
  $('.tutorial-focus').removeClass('tutorial-focus');
  $('.tutorial-mask').remove();
};

GmailDestroyKey.prototype.getHeight = function() {
  return $("#destroy").height() || 0;
};

/**
 * Binds updateView to window resize UI action
 */
GmailDestroyKey.prototype.bindUIActions = function () {
  //$(window).resize(this.updateView);
};

var overlay = new GmailDestroyKey();
overlay.init();
