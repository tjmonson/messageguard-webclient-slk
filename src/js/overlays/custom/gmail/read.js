/**
 * Generic read class.
 * @module
 * @requires bower_components/tether
 * @requires common/jquery-bootstrap
 * @requires overlays/overlay-base
 * @requires common/message-type
 */
'use strict';

window.Tether = require('tether');

var $           = require('../../../common/jquery-bootstrap'),
    GenericRead = require('../../generic-read'),
    MessageType = require('../../../common/message-type'),
    sanitizer   = require('../../../common/html-sanitizer'),
    encoder   = require('../../../common/encoder'),
    KeyManager        = require('../../../key-management/communicator/emitter');


var noSLKErrorMessage = "No SLK for this thread, so decryption should not be attempted.";
var privateKeyDestroyed = "SLK private key destroyed";
var encrytedEmailMessage = 'This message is encrypted. Click the button below to send an access request.';
var encrytedEmailMessageResend = 'This message is encrypted. Click the button below to resend an access request.';
/**
 * Controls the generic read interface
 * @constructor
 * @alias module:overlays/generic-read
 */
function GmailRead() {

}

GmailRead.prototype = new GenericRead();
GmailRead.prototype.constructor = GmailRead;

/**
 * Initialize and begin operating
 */
GmailRead.prototype.init = function () {
  var self = this;

  GenericRead.prototype.init.call(this);

  this.registerMessageHandler(MessageType.SIZING_INFO, false, function () {
    self.postMessage(MessageType.SIZING_INFO, self.getHeight());
  });

  this.uuid = this.getQueryStringParam('uuid');
  //this.updateView();
  this.bindUIActions();

  //REGISTER MESSAGE HANDLERS

  // Add tooltips
  $('[title]').tooltip({
                         container: 'body'
                       }).on('click', function () {
    $(this).tooltip('hide');
  });

  this.registerMessageHandler(MessageType.TUTORIAL_MASK_PLACE, false, function () {
    self.placeKeyButtonTutorialMask();
  });

  this.registerMessageHandler(MessageType.TUTORIAL_MASK_REMOVE, false, function () {
    self.removeKeyButtonTutorialMask();
  });

  this.registerMessageHandler(MessageType.DESTROY_THREAD_KEY, false, this.destroyThreadKey);
  this.registerMessageHandler(MessageType.PERSIST_THREAD_KEY, false, this.persistThreadKey);
};

GmailRead.prototype._showOverlay = function () {
  GenericRead.prototype._showOverlay.apply(this, arguments);
  this.postMessage(MessageType.SIZING_INFO, this.getHeight());
};

GmailRead.prototype.sendContentsHandler = function(data) {
  console.log(data);
  var self = this;

  self.mgData = self.unwrapMGPackage(data.package);
  data.threadMetadata.threadId = self.mgData.keyData.threadId;
  data.threadMetadata.recipients = self.mgData.recipients;
  self.mgData.userId = data.userId;
  self.mgData.messageId = self.extractUserMessageId(self.mgData);

  self.keyManagerReady.then(function () {
    return KeyManager.storeThreadMetadata(data.threadMetadata);
  }).then(function() {
    return KeyManager.associateThreadIds(data.urlThreadId, self.mgData.keyData.threadId);
  }).then(function() {
    return KeyManager.getSLK(data.userId, self.mgData.keyData.threadId);
  }).then(function(slkAttributes) {
    self.sendExpirationInfo(slkAttributes);
    if(!slkAttributes.publicOnly && slkAttributes.privateKeyDestroyed) {
      throw new Error(privateKeyDestroyed);
    }
    else {
      return self.performDecryption(data);
    }
  }).then(function (decryptedData) {
    console.log("Successfully decrypted!");
    $("#request").hide();
    $("#large").show();
    $('body').css('background-color', '#2C3E50');
    self.showMaskedMessage(data.package, {
      fadeOut:        true,
      fadeSpeed:      1000,
      contentWrapper: '<span style="display: block; padding: 50px 10px 10px 10px; line-height: 1.5; font-size: 14px; word-wrap: break-word;" />'
    });
    self.setDisplayContent(decryptedData);
    window.setTimeout(function() {
      self.hideMaskedMessage();
      self.postMessage(MessageType.SIZING_INFO, self.getHeight());
    }, 1000);
  }).catch(function (error) {

    if(error.message == privateKeyDestroyed) {
      $("#large").hide();
      $("#small").hide();
      $("#request").hide();
      $('#slk-destroyed').show();
      self.postMessage(MessageType.CANNOT_COMPOSE, null);
      self.postMessage(MessageType.SIZING_INFO, self.getHeight());
      return;
    }

    return KeyManager.hasMadeKeyRequest(self.mgData.messageId, self.mgData.sender).then(function(hasMadeRequest) {
      
      if (hasMadeRequest) {
        self.setToRequestDisplay(encrytedEmailMessageResend);
        $('#request-send-btn').text("Resend Decryption Request");
        self.hideMaskedMessage();
        return Promise.resolve();
      }
      else {
        return KeyManager.storeSLK(self.mgData.keyData).then(function() {
          self.setToRequestDisplay(encrytedEmailMessage);
          self.hideMaskedMessage();
          self.postMessage(MessageType.SIZING_INFO, self.getHeight());
        });
      }
    }).then(function() {
      if (error.name == "KeyNotFoundError") {
        self.generateSLKPromise = KeyManager.generateSLK(self.mgData.userId, self.mgData.keyData.threadId, self.mgData.keyData).then(function(slkAttributes) {
          self.sendExpirationInfo(slkAttributes);
        });
      }      
    });
  });
};

GmailRead.prototype.sendExpirationInfo = function (slkAttributes) {
  this.postMessage(MessageType.KEY_EXPIRATION_DATA, slkAttributes);   
};


GmailRead.prototype.placeKeyButtonTutorialMask = function () {
  if ($('#large').is(':visible')) {

    $('.key-btn').filter(':visible')
        .addClass('tutorial-focus')
        .after('<div class="tutorial-mask"></div>');

  } else if ($('#small').is(':visible')) {

    $('.key-btn').filter(':visible').parent().parent().addClass('tutorial-focus');
    $('#large, #small').filter(':visible').append('<div class="tutorial-mask"></div>');

  }
};

GmailRead.prototype.unwrapMGPackage = function(data) {
  return JSON.parse(encoder.convert(data.replace('|mg|', ''), 'base64', 'utf8'));
};

GmailRead.prototype.removeKeyButtonTutorialMask = function () {
  $('.tutorial-focus').removeClass('tutorial-focus');
  $('.tutorial-mask').remove();
};

GmailRead.prototype.setToRequestDisplay = function(message) {

  var self = this;

  $("#large").hide();
  $("#small").hide();
  $("#request").show();
  $('#read-message').text(message);
  $('body').css('background-color', 'white');
  $('#request-send-btn').click(this.sendRequestClickAction.bind(this));

  $("#details").text($("#details").text().replace(new RegExp("@contact", 'g'), self.mgData.sender));

  $("#details-link").click(function() {
    var details = $("#details");
    var link = $('#details-link');
    if(details.is(':visible')) {
      details.hide();
      link.text('Show more details...');
    }
    else {
      details.show();
      link.text('Hide details');
    }
    self.postMessage(MessageType.SIZING_INFO, self.getHeight());
  });

};

GmailRead.prototype.sendRequestClickAction = function() {
  var self = this;

  this.showMaskedMessage('Preparing request &hellip;', {showSpinner: true});

  this.keyManagerReady.then(function() {
    return self.generateSLKPromise;
  }).then(function() {
    return KeyManager.hasMadeKeyRequest(self.mgData.messageId, self.mgData.sender);
  }).then(function(hasMadeRequest) {
    if (hasMadeRequest) {
      self.sendRequest();
    }
    else {
      console.log("Storing key request: " + self.mgData.messageId + " " + self.mgData.sender);
      return KeyManager.storeKeyRequest(self.mgData.messageId, self.mgData.sender).then(self.sendRequest.bind(self));
    }

  });
};

GmailRead.prototype.sendRequest = function() {

  this.showMaskedMessage('Sending request &hellip;', {showSpinner: true});
  var self = this;
  return KeyManager.getSLK(self.mgData.userId, self.mgData.keyData.threadId).then(function(slkAttributes) {
    var requestData = {
      messageId: self.mgData.messageId,
      originalSender: self.mgData.sender,
      keyData: slkAttributes
    };

    //Ask the overlay to send the request data
    self.postMessage(MessageType.REQUEST_DECRYPTION_KEY, requestData);

    //Hide the request view
    window.setTimeout(function() {
      $("#read-message").text(encrytedEmailMessageResend);
      $('#request-send-btn').text("Resend Decryption Request");
      self.hideMaskedMessage();
      self.postMessage(MessageType.SIZING_INFO, self.getHeight());
    }, 1000);
  });
};

GmailRead.prototype.extractUserMessageId = function(mgData) {
  var foundId;
  mgData.messageIds.forEach(function(item) {
    if(item.email == mgData.userId)
      foundId = item.messageId;
  });
  return foundId;
};

GmailRead.prototype.destroyThreadKey = function(data) {

    var self = this;
    this.keyManagerReady.then(function() {
        return KeyManager.destroySLK(data.userId, data.threadId);
    }).then(function(metadata) {
        self.postMessage(MessageType.THREAD_KEY_DESTROYED, data);
    });
};

GmailRead.prototype.persistThreadKey = function(data) {
    var self = this;
    this.keyManagerReady.then(function() {
        return KeyManager.persistSLK(data.userId, data.threadId);
    }).then(function() {
        self.postMessage(MessageType.THREAD_KEY_PERSISTED, data);  
    });
};

/**
 * Binds updateView to window resize UI action
 */
GmailRead.prototype.bindUIActions = function () {
  //$(window).resize(this.updateView);
};

var overlay = new GmailRead();
overlay.init();
