/**
 * Abstract base overlay class.
 * @module
 * @requires common/communicator-mixin
 */
"use strict";

var communicatorMixin = require('../common/communicator-mixin'),
    AESPackager       = require('./packager/aes-packager'),
    encoder           = require('../common/encoder'),
    Errors            = require('../common/errors'),
    EventEmitter      = require('../common/storage/storage-objects').eventEmitter,
    EventNames        = require('../key-management/communicator/event-names'),
    KeyManager        = require('../key-management/communicator/emitter'),
    KeyAttributes     = require('../key-management/key-prototypes').KeyAttributes,
    UUID              = require('../common/uuid'),
    urls              = require('../common/urls'),
    $                 = require('../common/jquery-bootstrap');

/**
 *
 * @constructor
 * @alias module:frontend/overlay-base
 */
function OverlayBase() {
  this.packager = new AESPackager();
  this.instanceUUID = UUID(); // for debugging

  /**
   * Mix the communictor class into this class
   * @mixin
   */
  communicatorMixin.call(this);
}

/**
 * Utility function
 * @protected
 */
OverlayBase.prototype.getQueryStringParam = function (key) {
  key = key.replace(/[*+?^$.\[\]{}()|\\\/]/g, "\\$&"); // escape RegEx meta chars
  var match = location.search.match(new RegExp("[?&]" + key + "=([^&]+)(&|$)"));
  return match && decodeURIComponent(match[1].replace(/\+/g, " "));
};

/**
 * Initialize and begin operating.
 */
OverlayBase.prototype.init = function () {
  this.showMaskedMessage('Initializing &hellip;', {showSpinner: true});

  var self = this;

  this.keyInfo = [];

  window.addEventListener('message', function () {
    self.messagePort = event.ports[0];
    self.messagePort.onmessage = self.handleMessage.bind(self);
  }, false);

  var readyResolve = null;
  this.keyManagerReady = new Promise(function (resolve) {
    readyResolve = resolve;
  });

  this._waitForKeyManager().then(readyResolve).catch(function (e) {
    self.showMaskedMessage('Error initializing: ' + e.toString(), {isError: true});
  });

  this.keyManagerReady.then(function () {
    EventEmitter.on(EventNames.KEYS_UPDATED, function (e) {

      self.keyInfo = e.details.attributes.map(function (stringed) {
        return KeyAttributes.parse(stringed);
      });

      self.keysUpdated();
    });

    self.keysUpdated();
  });
};

OverlayBase.prototype._waitForKeyManager = function () {
  var self = this;

  return KeyManager.ensureKeyManagerAlive().then(function () {
    return self._ensureMasterPasswordPresent();
  }).then(function () {
    self.showMaskedMessage('Initializing &hellip;', {showSpinner: true});

    return new Promise(function (resolve) {
      var tryInitialize = function () {
        return KeyManager.initializeKeys().then(resolve);
      };

      var tryCatch = function () {
        tryInitialize().catch(function (error) {
          var errHTML = $(
              '<div><div class="err-text"></div><button type="button" class="retry-btn btn btn-info">Retry</button></div>');
          errHTML.find('.err-text').html(error.message);

          errHTML.find('.retry-btn').click(tryCatch);

          self.showMaskedMessage(errHTML, false, true, false);
        });
      };

      tryCatch();
    });
  }).then(function () {
    return KeyManager.getKeyInfo();
  }).then(function (info) {
    self.hideMaskedMessage();
    self.keyInfo = info;
    self.keyManagerActive = true;
  });
};

OverlayBase.prototype._ensureMasterPasswordPresent = function () {
  var self = this;

  return KeyManager.ensureMasterPasswordPresent().catch(function (error) {
    if (!(error instanceof Errors.MasterPasswordNotFoundError)) {
      throw error;
    }

    return self._promptForMasterPassword(error);
  });
};

OverlayBase.prototype._promptForMasterPassword = function (error) {
  var messageHTML;

  if (error instanceof Errors.MasterPasswordMissing) {
    messageHTML =
        'To use MessageGuard you must set up a master password.<br/ ><br/ >Click MG_LINK to set up master password.';
  } else if (error instanceof Errors.MasterPasswordNeedsReentering) {
    messageHTML = 'Please enter your master password.<br/ ><br/ >Click MG_LINK to enter master password.';
  } else {
    messageHTML = 'Please enter your master password.<br/ ><br/ >Click MG_LINK to enter master password.';
  }

  messageHTML = messageHTML.replace(/MG_LINK/g, '<a href="#" id="master-password-link">here</a>');
  var message = $('<div>' + messageHTML + '</div>');

  message.find('#master-password-link').click(function (e) {
    e.preventDefault();
    var kmUrl = urls.getKeyManagerUrl('index.html', {
      prompt_master_password: true,
      original_err_name:      error.name,
      reply_uuid:             EventNames.MASTER_PASSWORD_INITIALIZED
    });
    window.open(kmUrl);
    return false;
  });

  this.showMaskedMessage(message, {isError: true});

  var self = this;
  return new Promise(function (resolve) {
    EventEmitter.once(EventNames.MASTER_PASSWORD_INITIALIZED, function (e) {
      if (e.details.replyUUID) {
        EventEmitter.emit(e.details.replyUUID, {command: 'close'});
      }
      resolve();
    });
  });
};

OverlayBase.prototype.encryptDraft = function (draft) {
  return KeyManager.encryptDraft(draft);
};

OverlayBase.prototype.decryptDraft = function (encryptedDraft) {
  return KeyManager.decryptDraft(encryptedDraft);
};

/**
 * Called when the overlay needs to bind actions to UI events
 * @abstract
 */
OverlayBase.prototype.bindUIActions = function () {
  throw new Errors.AbstractMethodError();
};

/**
 * Called when the overlay needs to update views dynamically
 * @abstract
 */
OverlayBase.prototype.updateView = function () {
  throw new Errors.AbstractMethodError();
};

/**
 * Called when the encryption process needs a list of message recipients
 * @abstract
 */
OverlayBase.prototype.getRecipients = function () {
  throw new Errors.AbstractMethodError();
};

/**
 * Signals that the key info has been updated
 * @abstract
 */
OverlayBase.prototype.keysUpdated = function () {
  throw new Errors.AbstractMethodError();
};

OverlayBase.prototype.getNoKeyAvailableMessage = function (options) {
  options = options || {};

  var promptType = options.forEncryption ? 'encrypt' : 'decrypt';

  var windowOptions = {};

  if (options.scheme) {
    windowOptions.scheme = options.scheme;
  }

  if (options.fingerprints) {
    windowOptions.fingerprints = JSON.stringify(options.fingerprints);
  }

  var link = this.getAddKeyLink(promptType, windowOptions);

  var messageHTML;

  if (options.forEncryption) {
    messageHTML =
        '<span> \
          No encryption key selected. \
          <br/ ><br/ > \
          Click <a id="key-create-link">here</a> to add a key. \
        </span>';
  } else {
    messageHTML =
        '<span> \
          You don\'t have the key needed to decrypt this message. \
          <br/ ><br/ > \
          Click <a id="key-create-link">here</a> to add the appropriate key. \
          Otherwise contact the sender and ask them to re-encrypt the message for you. \
        </span>';
  }

  var messageElement = $(messageHTML);

  messageElement.find('#key-create-link').replaceWith(link.linkElement.text('here'));

  return {
    messageElement: messageElement,
    promise:        link.promise
  };
};

OverlayBase.prototype.getAddKeyLink = function (promptType, options, callback) {
  options = options || {};
  options.prompt_create_key = true;
  options.prompt_type = promptType;
  options.reply_uuid = UUID();

  var link = $('<a>Add key</a>').attr('href', '#').addClass('key-create-link');
  var kmUrl = urls.getKeyManagerUrl('index.html', options);

  link.click(function (e) {
    e.preventDefault();
    window.open(kmUrl);
    return false;
  });

  if (callback && typeof callback == 'function') {
    var receiveKeyCB = function (e) {
      if (e.details.err) {
        callback(e.details.err);
      } else {
        EventEmitter.emit(e.details.replyUUID, {command: 'close'});
        callback(null, KeyAttributes.parse(e.details.attributes));
      }
    };

    EventEmitter.on(options.reply_uuid, receiveKeyCB);

    // Function to call when link is destroyed, to clean up the event listener.
    var onDestroy = function () {
      EventEmitter.off(options.reply_uuid, receiveKeyCB);
    };

    return {
      linkElement: link,
      onDestroy:   onDestroy
    };
  } else {
    var promise = new Promise(function (resolve, reject) {
      EventEmitter.once(options.reply_uuid, function (e) {
        if (e.details.err) {
          reject(Errors.parse(e.details.err));
        } else {
          EventEmitter.emit(e.details.replyUUID, {command: 'close'});
          resolve(KeyAttributes.parse(e.details.attributes));
        }
      });
    });

    return {
      linkElement: link,
      promise:     promise
    };
  }
};

/**
 * Show a message that masks the overlay.
 * @param {string|Error} message The message to show.
 * @param {Object} options? Additional options for the message.
 * @param {string} options.contentWrapper Alternate content-wrapper for the message. Needs to be the string for an
 *    HTML span.
 * @param {boolean} options.showSpinner Whether to show a spinner with the message.
 * @param {boolean} options.isClosable Whether the message is clossable by the user.
 * @param {boolean} options.isError Whether this message essage is for an error. Gives a red background.
 * @param {boolean} options.fadeIn Should this message be faded in?
 * @param {boolean} options.fadeOut Should this message be faded out?
 * @param {int|string} options.fadeSpeed The speed at which the message should be faded.
 */
OverlayBase.prototype.showMaskedMessage = function (message, options) {
  var isSmallEditor = $('#small:visible').length;

  // Setup the content.
  var content = $(options.contentWrapper || '<span class="center-horizontal" />');

  // Show different spinners at different sizes;
  if (options.showSpinner) {
    if (isSmallEditor) {
      content.append($('<i class="fa fa-circle-o-notch fa-spin" style="padding: 1px;" />&nbsp;'));
    } else {
      content.append($('<i class="fa fa-circle-o-notch fa-spin fa-3x fa-fw" /><br /><br />'));
    }
  }

  // Special handling for errors.
  if (message instanceof Error) {
    console.error('Showing an error message');
    console.error(message);
    console.error(message.stack);
    message = message.toString();
  }
  content.append(message);

  if (options.isClosable) {
    var closeButton = $(
        '<button style="color:black;" type="button" class="btn btn-defalt">Go Back</button>'
    );
    closeButton.click(this.hideMaskedMessage.bind(this));

    if (isSmallEditor) {
      closeButton.css('margin-left', '0.2em');
      closeButton.addClass('btn-xs');
    } else {
      content.append('<br />')
      content.append('<br />')
    }

    content.append(closeButton);
  }

  // Display the masked message.
  var messageMask = $("#message-mask");
  messageMask[0].className = options.isError ? 'error-message' : '';

  if (options.fadeIn) {
    // We don't hide first, meaning fading only happens if there is not a message being replaced.
    messageMask.empty().append(content).fadeIn(options.fadeSpeed);
  } else if (options.fadeOut) {
    messageMask.empty().append(content).show().fadeOut(options.fadeSpeed);
  } else {
    messageMask.empty().append(content).show();
  }

  this._fitMessageText();
};

/**
 * Hide the current masked message.
 */
OverlayBase.prototype.hideMaskedMessage = function () {
  $("#message-mask").empty().hide();
};

/**
 * Try to ensure that the message text fits within the orverlay.
 * @private
 */
OverlayBase.prototype._fitMessageText = function () {
  var MIN_FONT_SIZE = 12;

  var messageMask = $("#message-mask");
  var messageContainer = messageMask.children('span.center-horizontal');
  if (!messageContainer.length) {
    return;
  }

  var fontSize = parseInt(messageContainer.css('font-size').replace(/px$/, ''), 10);

  while (fontSize > MIN_FONT_SIZE && messageContainer.height() > messageMask.height()) {
    fontSize -= 2;
    messageContainer.css('font-size', fontSize + 'px');
  }
};

module.exports = OverlayBase;
