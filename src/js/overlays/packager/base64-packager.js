/**
 * Generic compose class
 * @module
 * @requires packager/packager-base
 * @requires common/encoder
 */

"use strict";

var PackagerBase = require('./packager-base'),
    encoder      = require('../../common/encoder');

/**
 * Controls the generic compose interface
 * @constructor
 * @alias module:overlays/generic-compose
 */
function Base64Packager() {

}

Base64Packager.prototype = new PackagerBase();
Base64Packager.prototype.constructor = Base64Packager;

/**
 * Initialize
 */
Base64Packager.prototype.init = function () {

};

/**
 * Encrypts the given data
 * @param {string} data. The data to encrypt.
 * @abstract
 */
Base64Packager.prototype.encryptData = function (data) {
  return Promise.resolve(encoder.convert(data, 'utf8', 'base64'));
};

/**
 * Decrypts the given data.
 * @param {string} data. The data to decrypt. This data does not contain an '|mg|'
 * @abstract
 */
Base64Packager.prototype.decryptData = function (data) {
  return Promise.resolve(encoder.convert(data, 'base64', 'utf8'));
};

/**
 * Checks on the packager
 * @abstract
 */
Base64Packager.prototype.check = function (data) {

};

module.exports = Base64Packager;
