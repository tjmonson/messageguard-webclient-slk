/**
 * Abstract base overlay class.
 * @module
 */

"use strict";

var async = require('async');

var KeyManager = require('../../key-management/communicator/emitter'),
    encoder    = require('../../common/encoder'),
    Errors     = require('../../common/errors'),
    Identifier = require('../../key-management/key-prototypes').Identifier;

/**
 * @constructor
 * @alias module:frontend/overlay-base
 */
function PackagerBase() {

}

/**
 * Initialize
 */
PackagerBase.prototype.init = function () {
  throw new Errors.AbstractMethodError();
};

/**
 * Encrypts the given data
 * @param {string} data. The data to encrypt.
 */
PackagerBase.prototype.encrypt = function (data, keyAttributes, ids, userId, slkAttributes, recipients) {
  var fingerprint = keyAttributes.fingerprint;
  var scheme = keyAttributes.scheme;
  keyAttributes.id = new Identifier(keyAttributes.id.value, keyAttributes.id.type);

  var self = this;

  var toEncrypt = JSON.stringify({
                                   data:   data,
                                   fromID: keyAttributes.id ? keyAttributes.id.serialize() : null
                                 });

  return this.encryptData(toEncrypt).then(function (encryptionPackage) {
    var encryptionKey = encryptionPackage.keyMaterial;
    var encryptedData = encryptionPackage.encrypted;

    // If the key we're encrypting with does not use recipients, just encrypt the data once
    // and all endpoints should be able to decrypt.
    if (!keyAttributes.canHaveRecipients) {
      ids = [new Identifier()];
    }
    return KeyManager.encryptMessageKey(encryptionKey, fingerprint, ids, slkAttributes.threadId).then(function (result) {
      return self.stringifyEncryptedPackage(scheme, encryptedData, result.encryptedMessageKeys, result.messageIds, userId, slkAttributes, recipients);
    });
  });
};

/**
 * Encrypts the given data
 * @param {string} data. The data to encrypt.
 * @abstract
 */
PackagerBase.prototype.encryptData = function (data) {
  throw new Errors.AbstractMethodError();
};

/**
 * Decrypts the given data
 * @param {string} data. The data to decrypt.
 */
PackagerBase.prototype.decrypt = function (data) {
  var dataObj;

  try {
    dataObj = this.parseEncryptedPackage(data.package);
  } catch (e) {
    return Promise.reject(new Errors.PackagerError('Unable to parse encrypted package data.', e));
  }

  var self = this;

  return KeyManager.getKeyInfo().then(function (keyAttributesArray) {
    // Extract array of fingerprints.
    var availableSystems = keyAttributesArray.map(function (attributes) {
      return {fingerprint: attributes.fingerprint, email: attributes.id.value, threadId: attributes.threadId};
    });

    // Filter encrypted keys by those for which we have a fingerprint.
    var candidateEncryptedKeys = [];

    dataObj.encryptedKeys.forEach(function(encryptedKey) {
      availableSystems.forEach(function(system) {
        if(encryptedKey.fingerprint == system.fingerprint &&
          system.email == data.userId && system.threadId == dataObj.keyData.threadId)
          candidateEncryptedKeys.push(encryptedKey);
      });
    });

    // If there are no such encrypted keys, search for a stored symmetric key obtained through an exchange.
    // If no symmetric key is found, error out
    var matchingMessageId;
    dataObj.messageIds.forEach(function(item) {
      if(item.email == data.userId)
        matchingMessageId = item.messageId;
    });

    var handleKeyDecryptResult = function (result) {

      return self.decryptData({
                                keyMaterial: result.keyMaterial,
                                encrypted:   dataObj.encryptedData
                              }).then(function (decryptedData) {

        var decryptedData = JSON.parse(decryptedData);

        return {
          data:          decryptedData.data,
          fromID:        decryptedData.fromID ? Identifier.parse(decryptedData.fromID) : null,
          keyAttributes: result.keyAttributes
        };
      });
    };
    if (matchingMessageId) {
      return KeyManager.getEncryptedSymmetricKey(matchingMessageId, data.sender).then(function(encryptedKey) {
        return KeyManager.getSLK(data.userId, dataObj.keyData.threadId).then(function(ltkAttributes) {
          return [{
            fingerprint: ltkAttributes.fingerprint,
            encrypted:   encryptedKey
          }];
        });
      }).then(function(encryptedKeys) {
        return self.tryKeyDecrypt(encryptedKeys, dataObj.keyData.threadId);
      }).then(handleKeyDecryptResult);
    }
    else if (candidateEncryptedKeys.length != 0){
      return self.tryKeyDecrypt(candidateEncryptedKeys, dataObj.keyData.threadId).then(handleKeyDecryptResult);
    }
    else {
      throw new Errors.NoMatchingFingerprintsError('', {
        scheme:              dataObj.scheme,
        messageFingerprints: dataObj.encryptedKeys.map(function (key) {
          return key.fingerprint;
        })
      });
    }

  });
}

PackagerBase.prototype.stringifyEncryptedPackage = function (scheme, encryptedData, encryptedKeys, messageIds, userId, keyData, recipients) {
  var message = JSON.stringify({
                                 scheme:        scheme,
                                 encryptedData: encryptedData,
                                 encryptedKeys: encryptedKeys,
                                 messageIds: messageIds,
                                 sender: userId,
                                 keyData: keyData,
                                 recipients: recipients
                               });
  return '|mg|' + encoder.convert(message, 'utf8', 'base64');
};

PackagerBase.prototype.parseEncryptedPackage = function (stringified) {
  if (stringified.indexOf('|mg|', 0) !== 0) {
    throw new Errors.MalformedMessageError();
  }

  var parsed = JSON.parse(encoder.convert(stringified.substring('|mg|'.length), 'base64', 'utf8'));

  if ('scheme' in parsed && 'encryptedData' in parsed && 'encryptedKeys' in parsed &&
      'keyData' in parsed) {
    return parsed;
  } else {
    throw new Errors.MalformedMessageError('Required properties not found');
  }
};

PackagerBase.prototype.tryKeyDecrypt = function (encryptedKeys, threadId) {
  return new Promise(function (resolve, reject) {
    var idx = -1;
    var tryNext = function (e) {
      if (e) {
        if (!(e instanceof Errors.KeyManagerError)) {
          return reject(e);
        }
      }

      if (++idx >= encryptedKeys.length) {
        return reject(new Errors.AllDecryptAttemptsFailedError());
      }

      KeyManager.decryptMessageKey(encryptedKeys[idx].encrypted, encryptedKeys[idx].fingerprint, threadId)
          .then(resolve)
          .catch(tryNext);
    };

    tryNext();
  });
};

/**
 * Decrypts the given data
 * @param {string} data. The data to encrypt.
 * @abstract
 */
PackagerBase.prototype.decryptData = function (data) {
  throw new Errors.AbstractMethodError();
};

/**
 * Checks on the packager
 * @abstract
 */
PackagerBase.prototype.check = function (data) {
  throw new Errors.AbstractMethodError();
};

module.exports = PackagerBase;
