/**
 * Modifies the current element so that it can be overlayed with other content. Generic implementation that uses the
 * Shadow DOM where possible. When not available, the item will have its display set to none, otherwise the two
 * implementations are near identical.
 * @module
 */
"use strict";

/**
 * Creates an overlayer which can overlay content over the managed item.
 * @param {!Element} item Item that is being overlayed.
 * @param {boolean} clipToWindow Should this overlay be clipped to the visible window.
 * @alias module:frontend/overlayer
 */
function Overlayer(item, clipToWindow) {

  /**
   * Item that is being overlayed.
   * @type {!Element}
   * @private
   */
  this._item = item;

  /**
   * The items original display attribute.
   * @type {string}
   * @private
   */
  this._itemDisplay = null;

  /**
   * The items original display priority.
   * @type {string}
   * @private
   */
  this._itemDisplayPriority = null;

  /**
   * A ShadowRoot that will replace the item.
   * If the Shadow DOM is not available, it is just this elements parent.
   * @type {ShadowRoot}
   * @private
   */
  this._shadowRoot = null;

  /**
   * The <shadow> Element in the ShadowRoot that displays the item.
   * If the Shadow DOM is not available it is just the item.
   * @type {Element}
   * @private
   */
  this._shadowItem = null;

  /**
   * Whether we are using a shadow root. Used to handle cases where the Shadow DOM is not supported.
   * @type {boolean}
   * @private
   */
  this._usingShadowRoot = false;

  /**
   * Element that is being overlayed.
   * @type {Element}
   * @private
   */
  this._overlay = null;

  /**
   * The position of the overlay in relation to the overlayed item.
   * @type {Number}
   * @private
   */
  this._overlayPosition = null;

  /**
   * Style element.
   * @type {Element}
   * @private
   */
  this._styleElement = null;

  /**
   *
   * @type {boolean}
   * @private
   */
  this._clipToWindow = clipToWindow || false;
}

/**
 * Positions that an overlay can be played.
 * @readonly
 * @enum {Number}
 */
Overlayer.OverlayPositions = {
  TopLeft:     1,
  Top:         2,
  TopRight:    3,
  Left:        4,
  Center:      5,
  Right:       6,
  BottomLeft:  7,
  Bottom:      8,
  BottomRight: 9,
  Fill:        10
};

/**
 * Styles to copy from the overlayed element to the overlaying element.
 * @type {string[]}
 */
Overlayer.StylesToCopy = [
  'position',
  'top',
  'left',
  'right',
  'bottom',
  'visibility',
  'display'
];

/**
 * Schedule an update of the overlayers. Declared now so that it can be referenced.
 */
Overlayer._scheduleUpdate = function () {
  if (!Overlayer._updateScheduled && Overlayer._overlayers.length) {
    Overlayer._updateScheduled = true;
    window.setTimeout(function () {
      Overlayer._updateScheduled = false;
      Overlayer._stopObserver();
      for (var i = 0, len = Overlayer._overlayers.length; i < len; i++) {
        Overlayer._overlayers[i]._updateOverlay();
      }
      Overlayer._startObserver();
    }, 0);
  }
};

/**
 * Set of overlayers currently on the page.
 * @type {Arrays.<module:frontend/Overlayer>}
 * @private
 */
Overlayer._overlayers = [];

/**
 * Observer that watches for changes in overlayed item.
 * @type {!MutationObserver}
 * @private
 */
Overlayer._observer = new MutationObserver(Overlayer._scheduleUpdate);

/**
 * Stops the observer.
 * @private
 */
Overlayer._stopObserver = function () {
  Overlayer._observer.disconnect();
};

/**
 * Starts the observer.
 * @private
 */
Overlayer._startObserver = function () {
  Overlayer._observer.observe(document.documentElement, {
    childList:     true,
    subtree:       true,
    attributes:    true,
    characterData: true
  });
};

// Start listening for event.
Overlayer._startObserver();
window.addEventListener('resize', Overlayer._scheduleUpdate);

/**
 * Whether an update has been scheduled for the overlays.
 * @private
 */
Overlayer._updateScheduled = false;


/**
 * Setup the overlayer so it is ready to overlay.
 * @param {string} [style] Optional style element that should be added before an overlay to style it.
 */
Overlayer.prototype.setup = function (style) {
  Overlayer._stopObserver();
  // Try to instantiate a ShadowRoot to use for overlaying.

  if (window.Element.prototype.createShadowRoot) {
    try {
      this._shadowRoot = this._item.createShadowRoot();
      this._shadowItem = document.createElement('shadow');
      this._shadowRoot.appendChild(this._shadowItem);
      this._usingShadowRoot = true;
    } catch (e) {
      /* This can be used to wrap unwrappabable elements in the Shadow DOM by first putting
         them into a div. Since this breaks the immediate child selector and the overlay method
         is fast enough, we currently disable this hack.
      var newParent = document.createElement('div');
      this._item.parentNode.insertBefore(newParent, this._item);
      newParent.appendChild(this._item);
      this._shadowRoot = newParent.createShadowRoot();
    */
    }
  }

  if (!this._usingShadowRoot) {
    this._shadowRoot = this._item.parentNode;
    this._shadowItem = this._item;
  }

  // Append the style to the shadow root.
  if (style) {
    var styleElement = document.createElement('style');
    styleElement.type = 'text/css';
    styleElement.appendChild(document.createTextNode(style));

    // If we aren't in shadow-dom, only add one <style> element.
    if (!this._usingShadowRoot) {
      var styleID = 'message-guard-overlay-style';
      if (document.getElementById(styleID) === null) {
        // Haven't added this yet to the DOM.
        styleElement.id = styleID;
        document.head.appendChild(styleElement);
      }
      this._item.classList.add('messageGuardOverlay');
    } else {
      this._styleElement = styleElement;
      this._shadowRoot.insertBefore(this._styleElement, this._shadowItem);
    }
  }

  // Register this overlayer.
  Overlayer._overlayers.push(this);
  Overlayer._startObserver();
};


/**
 * Called when the item has been moved.
 */
Overlayer.prototype.onItemMoved = function () {
  // Moving is handled automatically by the ShadowRoot.
  if (this._usingShadowRoot) return;

  // If the parent has been changed, update it and the overlay if attached.
  if (this._shadowRoot !== this._item.parentNode) {
    this._shadowRoot = this._item.parentNode;
    if (this._overlay) {

      this._shadowRoot.appendChild(this._overlay);
      Overlayer._startObserver();
    }
  }

  // Schedule an update with the overlayer.
  Overlayer._scheduleUpdate();
};

/**
 * Set the overlay's style.
 * @param property Property to set.
 * @param value Value of the property.
 */
Overlayer.prototype._updateOverlayStyle = function (property, value) {
  this._overlay.style.setProperty(property, value, 'important');
};

/**
 * Place the given overlay.
 * @param overlay Overlay to place.
 * @param overlayPosition Position to place it relative to the page.
 */
Overlayer.prototype.placeOverlay = function (overlay, overlayPosition) {
  // This handles an edge case where the item was not in the page's DOM when setup() was called.
  // We try to fix this problem at this stage, and if we cannot then we can't place an overlay.
  if (!this._usingShadowRoot && this._shadowRoot == null) {
    this._shadowRoot = this._shadowItem.parentNode;
    if (this._shadowRoot == null) {
      return;
    }
  }

  // Remove the existing overlay.
  if (this._overlay) this.removeOverlay();

  this._overlay = overlay;
  this._overlayPosition = overlayPosition;

  // Add the overlay.
  this._shadowRoot.insertBefore(this._overlay, this._shadowItem);

  // Handle when the Shadow DOM is not available. Store the original display of the item before we change it.
  if (!this._usingShadowRoot && this._overlayPosition === Overlayer.OverlayPositions.Fill) {
    this._itemDisplay = this._item.style.display;
    this._itemDisplayPriority = this._item.style.getPropertyPriority('display');
  }

  // Update the overlayer.
  Overlayer._scheduleUpdate();
};

/**
 * TODO - this function is rather long. Probably should be split into smaller more testable functions
 * Update the overlay to remain graphically consistent.
 * @private
 */
Overlayer.prototype._updateOverlay = function () {
  if (!this._overlay) return;

  // Show the _item to get its computed style.
  if (this._overlayPosition === Overlayer.OverlayPositions.Fill) {
    // Handle when the Shadow DOM is not available.
    if (!this._usingShadowRoot) {
      // Update the display style if it has changed, otherwise revert the _item to its normal display style.
      if (this._item.style.display !== 'none') {
        this._itemDisplay = this._item.style.display;
      } else {
        this._item.style.setProperty('display', this._itemDisplay, this._itemDisplayPriority);
      }
    } else {
      this._updateOverlayStyle('display', 'none');
      this._shadowRoot.appendChild(this._shadowItem);
    }
  }

  // Get the item's current style attributes.
  var itemStyle = window.getComputedStyle(this._item);

  // Ensure that we have the correct z-index.
  this._updateOverlayStyle('z-index', (itemStyle.zIndex === 'auto' ? 0 : itemStyle.zIndex) + 1);

  // Completely cover the original element.
  if (this._overlayPosition === Overlayer.OverlayPositions.Fill) {
    // Set size. Has to be done before the overlay's display is changed, otherwise the overlay's size will be included
    // in calculation.

    this._updateOverlayStyle('width', this._item.offsetWidth + 'px');
    if (this._item.offsetHeight > 30) {
      var height = 0;
      if (this._clipToWindow) {
        var clientRectangle = this._item.getBoundingClientRect();
        height = Math.min(window.innerHeight, clientRectangle.bottom) - clientRectangle.top;
      } else {
        height = this._item.offsetHeight;
      }

      this._updateOverlayStyle('height', height + 'px');
    } else {
      this._updateOverlayStyle('height', '30px');
    }

    // Set the other styles on the overlay to match the item.
    for (var i = 0, len = Overlayer.StylesToCopy.length; i < len; i++) {
      this._updateOverlayStyle(Overlayer.StylesToCopy[i], itemStyle[Overlayer.StylesToCopy[i]]);
    }
  } else {
    // blend in with the original element.

    this._updateOverlayStyle('position', 'absolute');

    // Vertical styling.
    if (this._overlayPosition === Overlayer.OverlayPositions.TopLeft ||
        this._overlayPosition === Overlayer.OverlayPositions.Top ||
        this._overlayPosition === Overlayer.OverlayPositions.TopRight) {
      this._updateOverlayStyle('top', this._item.offsetTop + 'px');
    } else if (this._overlayPosition === Overlayer.OverlayPositions.Left ||
               this._overlayPosition === Overlayer.OverlayPositions.Center ||
               this._overlayPosition === Overlayer.OverlayPositions.Right) {
      this._updateOverlayStyle('top',
                               (this._item.offsetTop + (this._item.offsetHeight - this._overlay.offsetHeight) / 2) +
                               'px');
    } else if (this._overlayPosition === Overlayer.OverlayPositions.BottomLeft ||
               this._overlayPosition === Overlayer.OverlayPositions.Bottom ||
               this._overlayPosition === Overlayer.OverlayPositions.BottomRight) {
      this._updateOverlayStyle('top',
                               (this._item.offsetTop + this._item.offsetHeight - this._overlay.offsetHeight) + 'px');
    }

    // Horizontal styling.
    if (this._overlayPosition === Overlayer.OverlayPositions.TopLeft ||
        this._overlayPosition === Overlayer.OverlayPositions.Left ||
        this._overlayPosition === Overlayer.OverlayPositions.BottomLeft) {
      this._updateOverlayStyle('left', this._item.offsetLeft);
    } else if (this._overlayPosition === Overlayer.OverlayPositions.Top ||
               this._overlayPosition === Overlayer.OverlayPositions.Center ||
               this._overlayPosition === Overlayer.OverlayPositions.Bottom) {
      this._updateOverlayStyle('left',
                               (this._item.offsetLeft + (this._item.offsetWidth - this._overlay.offsetWidth) / 2) +
                               'px');
    } else if (this._overlayPosition === Overlayer.OverlayPositions.TopRight ||
               this._overlayPosition === Overlayer.OverlayPositions.Right ||
               this._overlayPosition === Overlayer.OverlayPositions.BottomRight) {
      this._updateOverlayStyle('left',
                               (this._item.offsetLeft + this._item.offsetWidth - this._overlay.offsetWidth) + 'px');
    }
  }

  // Rehide the item.
  if (this._overlayPosition === Overlayer.OverlayPositions.Fill) {
    // Handle when the Shadow DOM is not available.
    if (!this._usingShadowRoot) {
      this._item.style.setProperty('display', 'none', 'important');
    } else {
      this._shadowRoot.removeChild(this._shadowItem);
    }
  }
};

/**
 * Removes the current overlay.
 */
Overlayer.prototype.removeOverlay = function () {
  if (!this._overlay) return;

  // Restore the overlay.
  this._shadowRoot.removeChild(this._overlay);

  // Handle when the Shadow DOM is not available.
  if (!this._usingShadowRoot) {
    this._item.style.setProperty('display', this._itemDisplay, this._itemDisplayPriority);
  } else {
    this._shadowRoot.appendChild(this._shadowItem);
  }

  this._overlay = this._overlayPosition = null;
};

/**
 * Cleanup this class so that it leaves no trace in the DOM and is eligible for cleanup.
 */
Overlayer.prototype.cleanup = function () {
  Overlayer._stopObserver();
  this.removeOverlay();

  // Fix for non-shadow DOM.
  if (!this._usingShadowRoot) {
    this._item.classList.remove('messageGuardOverlay');
  } else {
    this._shadowRoot.removeChild(this._styleElement);
  }

  // Unregister this overlayer.
  var index = Overlayer._overlayers.indexOf(this);
  if (index !== -1) Overlayer._overlayers.splice(index, 1);

  Overlayer._startObserver();
};

module.exports = Overlayer;