/**
 * Controller that can be used on any page.
 * @module
 * @requires frontend/controller-base
 * @requires frontend/generic/read-overlay-manager
 * @requires frontend/generic/compose-overlay-manager
 */
"use strict";

var ControllerBase        = require('../controller-base'),
    ReadOverlayManager    = require('./read-overlay-manager'),
    ComposeOverlayManager = require('./compose-overlay-manager');

/**
 * Creates a controller suitable for all pages.
 * @constructor
 * @extends module:frontend/controller-base
 * @alias module:frontend/generic/controller
 */
function Controller() {
  ControllerBase.call(this, 'textarea,[contentEditable]');

  // Set the overlays we want to use.
  this.constructors.read = ReadOverlayManager;
  this.constructors.compose = ComposeOverlayManager;
}

Controller.prototype = new ControllerBase();
Controller.prototype.constructor = Controller;

module.exports = Controller;