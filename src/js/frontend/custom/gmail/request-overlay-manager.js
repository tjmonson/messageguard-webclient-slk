/**
 * Generic read overlay manager.
 * @module
 * @requires frontend/overlay-manager-base
 * @requires common/message-type
 */
"use strict";

var OverlayManagerBase = require('../../overlay-manager-base'),
    encoder            = require('../../../common/encoder'),
    MessageType        = require('../../../common/message-type'),
    PackageWrapper     = require('./package-wrapper'),
    PageScanner        = require('./page-scanner'),
    MouseEvents        = require('./mouse-events');

var jQuery = require('jquery');

//noinspection JSClosureCompilerSyntax
/**
 * Create a read overlay manager for the given item.
 * @param {!Element} item Item that is being overlayed.
 * @constructor
 * @extends module:frontend/overlay-manager-base
 * @alias module:frontend/generic/read-overlay-manager
 */
function RequestOverlayManager(item, windowUUID, controller) {
  OverlayManagerBase.call(this, item, windowUUID, controller);
  this.overlayURL = 'custom/gmail/gmail-request.html';

  // Change the lock icon in the subject line.
  var subjectLine = jQuery('.hP:first');
  if (subjectLine.length) {
    if (PackageWrapper.isWrappedRequest(subjectLine.text())) {
      subjectLine.text(PackageWrapper.unwrapRequest(subjectLine.text()));
      var requestLabel = document.createElement('div');
      requestLabel.className = 'messageGuardRequestDiv';
      requestLabel.innerText = 'Decryption Request';
      subjectLine.before(requestLabel);
    }
  }

  // Hide the encryption-not supported icons from Gmail.
  jQuery('span[role="button"].bcU', this.item).hide();
}

RequestOverlayManager.prototype = Object.create(OverlayManagerBase.prototype);
RequestOverlayManager.prototype.constructor = RequestOverlayManager;

/**
 * Setup and begin operating.
 */
RequestOverlayManager.prototype.setup = function () {
  OverlayManagerBase.prototype.setup.call(this);
  this.placeOverlay();
};

RequestOverlayManager.prototype.sendRequestResponse = function(responseData) {

  var responsePackage = JSON.stringify(responseData);

  var packageDiv = '<div style="font-size: 9px;">' +
                  '---Begin MessageGuard Decryption Response Package---' +
                  '<br><br>' +
                  '<div name="response-package">' +
                  encoder.convert(responsePackage, 'utf8', 'base64') +
                  '</div>' +
                  '<br><br>' +
                  '---End MessageGuard Decryption Response Package---' +
                  '<br><br>' +
                   '</div>';

    var requestBody = '<div name="responseEmail">' +
                    '<div name="messageGuardThreadId" style="display:none;">[mg-id:' + responseData.threadId + ']</div>' + 
                    'MessageGuard Decryption Response' +
                    '<br><br>' +
                    'Please open this email on the device you have MessageGuard installed on. Opening this will allow you to open an encrypted message ' + 
                    responseData.sender + ' sent you earlier.' +
                    '<br><br>' +
                    packageDiv +
                    '</div>'

  //this.sendEmail(responseData.requester, requestSubject, requestBody, true);
  this.sendThreadEmail(requestBody);
};

/**
 * Called when the overlay is ready to communicate.
 */
RequestOverlayManager.prototype.onOverlayReady = function () {
  var self = this;
  this.registerMessageHandler(MessageType.SIZING_INFO, false, function (data) {
    jQuery(this.item).height(data + 40).css({overflow: "hidden"});
  });

  this.registerMessageHandler(MessageType.SEND_REQUEST_RESPONSE, false, function(data) {

    self.sendRequestResponse(data);
  });

  this.windowResizeFunction = function () {
    this.postMessage(MessageType.SIZING_INFO);
  }.bind(this);
  jQuery(window).on('resize', this.windowResizeFunction);

  var requestData = self.unwrapRequestPackage(jQuery(this.item));
  this.postMessage(MessageType.SEND_CONTENTS, {
    messageId: requestData.messageId,
    requester: requestData.requester,
    keyData: requestData.keyData,
    userId:  PageScanner.getEmailAddress(),
    urlThreadId: PageScanner.getEmailThreadId()
  });


  jQuery(this.item).addClass('messageGuardRequestOverlayed');

  this.postMessage(MessageType.SIZING_INFO);
};

RequestOverlayManager.prototype.unwrapRequestPackage = function(html) {
    var encodedData = jQuery('div[name="request-package"]', html).text();
    var decodedData = encoder.convert(encodedData, 'base64', 'utf8');
    return JSON.parse(decodedData);
};

/**
 * Cleanup this overlay manager.
 */
RequestOverlayManager.prototype.cleanup = function () {
  OverlayManagerBase.prototype.cleanup.call(this);
  jQuery(window).off('resize', this.windowResizeFunction);
};

RequestOverlayManager.prototype.sendEmail = function (to, subject, body, autoSend) {

  var onMessageOpened = function (composeContainer) {
    var toField      = jQuery(composeContainer).find('textarea[name="to"]'),
        subjectField = jQuery(composeContainer).find('input[name="subjectbox"]'),
        bodyArea     = jQuery(composeContainer).find('[contenteditable]:last'); // :last not necessary at the moment,
                                                                                // just a precaution.

    if (typeof to == 'object') {
      toField.text(to.join(', '));
      setTimeout(function () {
        toField.blur();
        subjectField.focus();
        MouseEvents.simulateMouseClick(subjectField[0]);
      }, 200);
    } else {
      toField.text(to);
    }

    subjectField.val(subject);
    bodyArea.html(body + bodyArea.html());

    if(autoSend) {
      var sendButton = jQuery('[role="button"]:contains(Send)');
      MouseEvents.simulateMouseClick(sendButton);
    }
  }

  var clickComposeAndWait = function () {
    return new Promise(function (resolve) {

      var composeWindowSelector = '.nH.Hd',
          initialComposeWindows = jQuery(composeWindowSelector);

      var lookForNewComposeWindow = function () {
        var currentComposeWindows = jQuery(composeWindowSelector),
            newComposeWindows     = currentComposeWindows.not(initialComposeWindows);

        if (newComposeWindows.length == 0) {
          // We haven't seen a new one yet, wait another quarter-second.
          setTimeout(lookForNewComposeWindow, 100);
          return;
        }

        resolve(newComposeWindows.first());
      };

      // Open a new compose window.
      var composeButton = jQuery('[role="button"]:contains(COMPOSE)');
      MouseEvents.simulateMouseClick(composeButton);

      // Wait for the new window to open.
      lookForNewComposeWindow();
    });
  };

  var maximizedComposeBackground = jQuery('.aSs');

  var shrinkCurrentWindow;

  if (maximizedComposeBackground.css('visibility') == 'visible') {

    // We'll need to minimize the current compose window, and then after that we can
    // click "Compose".

    shrinkCurrentWindow = new Promise(function (resolve) {

      // Like lookForNewComposeWindow, this function repeatedly checks until the
      // compose background is hidden. before resolving this promise.
      var waitForMinimized = function () {
        if (maximizedComposeBackground.css('visibility') == 'visible') {
          setTimeout(waitForMinimized, 100);
          return;
        }

        resolve();
      };

      // Click to minimize the compose window, and wait for it to get minimized.
      MouseEvents.simulateMouseClick(maximizedComposeBackground);
      waitForMinimized();
    });
  } else {
    shrinkCurrentWindow = Promise.resolve();
  }

  shrinkCurrentWindow.then(function () {
    return clickComposeAndWait().then(onMessageOpened);
  });
};

module.exports = RequestOverlayManager;
