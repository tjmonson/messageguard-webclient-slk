/**
 * Generic read overlay manager.
 * @module
 * @requires frontend/overlay-manager-base
 * @requires common/message-type
 */
"use strict";

var OverlayManagerBase = require('../../overlay-manager-base'),
    encoder            = require('../../../common/encoder'),
    Time               = require('../../../common/time'),
    MessageType        = require('../../../common/message-type'),
    PackageWrapper     = require('./package-wrapper'),
    PageScanner        = require('./page-scanner'),
    MouseEvents        = require('./mouse-events');

var jQuery = require('jquery');

//noinspection JSClosureCompilerSyntax
/**
 * Create a read overlay manager for the given item.
 * @param {!Element} item Item that is being overlayed.
 * @constructor
 * @extends module:frontend/overlay-manager-base
 * @alias module:frontend/generic/read-overlay-manager
 */
function ReadOverlayManager(item, windowUUID, controller) {
  OverlayManagerBase.call(this, item, windowUUID, controller);
  this.overlayURL = 'custom/gmail/gmail-read.html';

  // Change the lock icon in the subject line.
  var subjectLine = jQuery('.hP:first');
  if (subjectLine.length) {
    if (PackageWrapper.isWrappedSubject(subjectLine.text())) {
      subjectLine.text(PackageWrapper.unwrapSubject(subjectLine.text()));
      var lockButton = document.createElement('div');
      lockButton.className = 'messageGuardLockDiv';
      lockButton.innerText = 'Encrypted';
      subjectLine.before(lockButton);

      var expirationLabel = document.createElement('div');
      expirationLabel.className = 'messageGuardExpirationLabel';
      expirationLabel.innerText = 'Calculating expiration...';
      jQuery('.hP:first').before(expirationLabel);
    }
  }

  // Hide the encryption-not supported icons from Gmail.
  jQuery('span[role="button"].bcU', this.item).hide();
}

ReadOverlayManager.prototype = Object.create(OverlayManagerBase.prototype);
ReadOverlayManager.prototype.constructor = ReadOverlayManager;

/**
 * Setup and begin operating.
 */
ReadOverlayManager.prototype.setup = function () {
  OverlayManagerBase.prototype.setup.call(this);
  this.placeOverlay();
};

/*ReadOverlayManager.prototype.getParticipants = function() {
  var self = this;

  return new Promise(function(resolve) {
    var retrieveParticipants = function(div) {
      var initialRecipients = div.innerHTML.match(/([a-zA-Z0-9._-]+@[a-zA-Z0-9._-]+\.[a-zA-Z0-9._-]+)/gi);
      var recipients = [];
      initialRecipients.forEach(function(item) {
        if(!recipients.includes(item))
          recipients.push(item);
      });
      resolve(recipients);
    };
    var participantsDiv = document.querySelector('div.ajA.SK');
    
    if(!participantsDiv) {
      MouseEvents.simulateMouseClick(jQuery('img.ajz[role=button]'));
      setTimeout(function() {
        participantsDiv = document.querySelector('div.ajA.SK');
        retrieveParticipants(participantsDiv);
      },200);
    }
    else 
      retrieveParticipants(participantsDiv);
  });
};*/

ReadOverlayManager.prototype.getSubject = function() {
  return document.querySelector('.hP').innerHTML;
};

ReadOverlayManager.prototype.getPreamble = function() {
  //return document.querySelector('div[name="preamble"]').innerHTML;
  return ''; //race condition getting this right now
};

ReadOverlayManager.prototype.getThreadMetadata = function () {
  var metadata = {};
  metadata.subject = this.getSubject().replace("[MessageGuard] ", "");
  metadata.preamble = this.getPreamble();
  metadata.urlThreadId = PageScanner.getEmailThreadId();
  return metadata;
};

ReadOverlayManager.prototype.sendMessageKeyRequest = function(requestData) {
  var requestPackage = JSON.stringify({messageId: requestData.messageId, requester: PageScanner.getEmailAddress(), 
    publicKey: requestData.requesterKey, keyData: requestData.keyData});
  var packageDiv = '<div style="font-size: 9px;">' +
                  '---Begin MessageGuard Decryption Request Package---' +
                  '<br><br>' +
                  '<div name="request-package\">' +
                   encoder.convert(requestPackage, 'utf8', 'base64') +
                   '</div>' +
                  '<br><br>' +
                  '---End MessageGuard Decryption Request Package---' +
                  '<br><br>' +
                   '</div>';
    var  requestBody = '<div name="requestEmail">' +
                    '<div name="messageGuardThreadId" style="display:none;">[mg-id:' + requestData.keyData.threadId + ']</div>' + 
                    'MessageGuard Decryption Request' +
                    '<br><br>' +
                    PageScanner.getEmailAddress() + ' received a message they cannot decrypt yet. They need more information from your MessageGuard tool to decrypt their message.' +
                    '<br><br>' +
                    'Please open this email on the device you have MessageGuard installed on.' +
                    '<br><br>' +
                    packageDiv +
                    '</div>';
                
  //this.sendEmail(requestData.originalSender, requestSubject, requestBody, true);
  this.sendThreadEmail(requestBody);
};

/**
 * Called when the overlay is ready to communicate.
 */
ReadOverlayManager.prototype.onOverlayReady = function () {
  var self = this;
  this.registerMessageHandler(MessageType.SIZING_INFO, false, function (data) {
    jQuery(this.item).height(data + 40).css({overflow: "hidden"});
  });

  this.registerMessageHandler(MessageType.CANNOT_COMPOSE, false, function(data) {
    jQuery(".ip.iq").hide();
  });

  this.registerMessageHandler(MessageType.REQUEST_DECRYPTION_KEY, false, function(data) {
    self.controller.refreshSLKOverlay();
    self.sendMessageKeyRequest(data);
  });

  this.registerMessageHandler(MessageType.HIDE_REPLY_FORWARD, false, function(data) {
    jQuery('.ip.iq').hide();
  });

  this.registerMessageHandler(MessageType.KEY_EXPIRATION_DATA, false, function(data) {
    self.updateExpirationUI(data);
    self.addThreadMakeUnreadableBtn(data);
  });

  this.registerMessageHandler(MessageType.THREAD_KEY_DESTROYED, false, function(data) {
    self.controller.refreshReadOverlays();
    var makeUnreadableBtn = document.querySelector('#make-unreadable-btn');
    if(makeUnreadableBtn)
        makeUnreadableBtn.remove();
    self.clearExpirationLabel();

    /*if(data.fromModal) {
        document.querySelector('#thread-' + data.threadId).remove();
        self.tryRemoveModal();
    }*/

    self.postGetThreadExpiration();

    self.controller.refreshSLKOverlay(data.threadId);

    setTimeout(function() {
        self.sendKeyDestroyedNotice(data.threadId);
    }, 250);

  });

  this.registerMessageHandler(MessageType.THREAD_KEY_PERSISTED, false, function(data) {
    self.controller.refreshReadOverlays();
    /*if(data.fromModal) {
        document.querySelector('#thread-' + data.threadId).remove();
        self.tryRemoveModal();
    }*/
    self.postGetThreadExpiration();
  });

  this.windowResizeFunction = function () {
    this.postMessage(MessageType.SIZING_INFO);
  }.bind(this);
  jQuery(window).on('resize', this.windowResizeFunction);

  var contents = {};
  contents.package = PackageWrapper.unwrapPackage(jQuery(this.item).html());
  contents.userId = PageScanner.getEmailAddress();
  contents.urlThreadId = PageScanner.getEmailThreadId();
  contents.threadMetadata = self.getThreadMetadata();

  this.postMessage(MessageType.SEND_CONTENTS, contents);

  jQuery(this.item).addClass('messageGuardReadOverlayed');

  this.postMessage(MessageType.SIZING_INFO);
};

ReadOverlayManager.prototype.updateExpirationUI = function(expirationData) {
  //Check for label first
  var self = this;

  this.clearIntervals();
  //Check for label first
  var expirationLabel = jQuery('.messageGuardExpirationLabel', jQuery('.ha'));
  if (!expirationLabel.length) {
      expirationLabel = document.createElement('div');
      expirationLabel.className = 'messageGuardExpirationLabel';
      expirationLabel = jQuery(expirationLabel);
      jQuery('.hP:first').before(expirationLabel);
  }

  if(expirationData.privateKeyDestroyed) {
    expirationLabel.text("Unreadable");
  }
  else {
    expirationLabel.text(Time.getExpirationText(expirationData.created, expirationData.lifespan));

    if(expirationLabel.text() != "Unreadable") {

        var utcTimeLeft = (expirationData.created + expirationData.lifespan) - Time.getUTCTime();
        var secLeft = Math.floor(utcTimeLeft / 1000) % 60;
        var initialWait = 60 - secLeft + 1;
        
        self.expirationInterval = window.setInterval(function() {
            expirationLabel.text(Time.getExpirationText(expirationData.created, expirationData.lifespan));
        }, 1000);
    }
  }
};

ReadOverlayManager.prototype.addThreadMakeUnreadableBtn = function(data) {
    var self = this;

    if(!data.privateKeyDestroyed && !Time.isExpired(data.created, data.lifespan) && !document.querySelector('#make-unreadable-btn')) {
        var threadBar = document.querySelector(".ha");
        var btn = document.createElement('button');
        btn.innerText = 'Make Unreadable';
        btn.style.float = 'right';
        btn.className += 'btn btn-success';
        btn.id = 'make-unreadable-btn';

        btn.addEventListener('click', function(e) {
            self.postMessage(MessageType.DESTROY_THREAD_KEY, {threadId: data.threadId, userId: PageScanner.getEmailAddress(), fromModal: false});
            btn.innerText = 'Working...';
            btn.className += " disabled";
        });

        threadBar.appendChild(btn);
    }
};

ReadOverlayManager.prototype.clearIntervals = function() {
    if(this.expirationTimeout) {
        clearTimeout(this.expirationTimeout);
        this.expirationTimeout = null;
    }
    if(this.expirationInterval) {
        clearInterval(this.expirationInterval);
        this.expirationInterval = null;
    }
};

/**
 * Cleanup this overlay manager.
 */
ReadOverlayManager.prototype.cleanup = function () {
  OverlayManagerBase.prototype.cleanup.call(this);
  jQuery(window).off('resize', this.windowResizeFunction);
};

ReadOverlayManager.prototype.resendContents =  function() {
  var self = this;
  this.postMessage(MessageType.SEND_CONTENTS, {
    package:        PackageWrapper.unwrapPackage(jQuery(this.item).html()),
    userId:         PageScanner.getEmailAddress(),
    urlThreadId:    PageScanner.getEmailThreadId(),
    threadMetadata: self.getThreadMetadata()
  });
};

ReadOverlayManager.prototype.clearExpirationLabel = function() {

    this.clearIntervals();
    var label = jQuery(".messageGuardExpirationLabel", this.item);
    if(label)
        label.text("Unreadable");
};

ReadOverlayManager.prototype.sendKeyDestroyedNotice = function(threadId) {
  var destroyedPackage = JSON.stringify({threadId: threadId, destroyedBy: PageScanner.getEmailAddress()});
  var packageDiv = '<div style="font-size: 9px;">' +
                  '---Begin MessageGuard Key Destruction Notice Package---' +
                  '<br><br>' +
                  '<div name="destruction-package">' +
                   encoder.convert(destroyedPackage, 'utf8', 'base64') +
                   '</div>' +
                  '<br><br>' +
                  '---End MessageGuard Key Destruction Notice Package---' +
                  '<br><br>' +
                   '</div>';
  var requestBody = '<div name="keyDestructionEmail">' +
                    '<div name="messageGuardThreadId" style="display:none;">[mg-id:' + threadId + ']</div>' + 
                    'MessageGuard Short-Lived Key Destruction Notice' +
                    '<br><br>' +
                    PageScanner.getEmailAddress() + ' destroyed their key for this encrypted thread. This means ' + PageScanner.getEmailAddress() +
                    ' will no longer be able to read any encrypted message related to this thread.'  +
                    '<br><br>' +
                    'Please open this email on the device you have MessageGuard installed on so MessageGuard can be updated with this information.' +
                    '<br><br>' +
                    packageDiv +
                    '</div>';
                
  //this.sendEmail(requestData.originalSender, requestSubject, requestBody, true);
  this.sendThreadEmail(requestBody);
};

ReadOverlayManager.prototype.postGetThreadExpiration = function() {
    var newURL = location.href;
    var urlPieces = newURL.split("/");
    var urlThreadId = urlPieces[urlPieces.length - 1].split("?")[0];

    this.postMessage(MessageType.GET_THREAD_EXPIRATION, {urlThreadId: urlThreadId, currentURL: newURL, userId: PageScanner.getEmailAddress()});
};


module.exports = ReadOverlayManager;
