/**
 * Generic read overlay manager.
 * @module
 * @requires frontend/overlay-manager-base
 * @requires common/message-type
 */
"use strict";

var OverlayManagerBase = require('../../overlay-manager-base'),
    encoder            = require('../../../common/encoder'),
    MessageType        = require('../../../common/message-type'),
    PackageWrapper     = require('./package-wrapper'),
    PageScanner        = require('./page-scanner'),
    MouseEvents        = require('./mouse-events');

var jQuery = require('jquery');

//noinspection JSClosureCompilerSyntax
/**
 * Create a read overlay manager for the given item.
 * @param {!Element} item Item that is being overlayed.
 * @constructor
 * @extends module:frontend/overlay-manager-base
 * @alias module:frontend/generic/read-overlay-manager
 */
function DestroyKeyOverlayManager(item, windowUUID, controller) {
  OverlayManagerBase.call(this, item, windowUUID, controller);
  this.overlayURL = 'custom/gmail/gmail-destroy-key.html';

  // Change the lock icon in the subject line.
  var subjectLine = jQuery('.hP:first');
  if (subjectLine.length) {
    if (PackageWrapper.isWrappedResponse(subjectLine.text())) {
      subjectLine.text(PackageWrapper.unwrapResponse(subjectLine.text()));
      var resonseLabel = document.createElement('div');
      resonseLabel.className = 'messageGuardResponseDiv';
      resonseLabel.innerText = 'Decryption Response';
      subjectLine.before(resonseLabel);
    }
  }

  // Hide the encryption-not supported icons from Gmail.
  jQuery('span[role="button"].bcU', this.item).hide();
}

DestroyKeyOverlayManager.prototype = Object.create(OverlayManagerBase.prototype);
DestroyKeyOverlayManager.prototype.constructor = DestroyKeyOverlayManager;

/**
 * Setup and begin operating.
 */
DestroyKeyOverlayManager.prototype.setup = function () {
  OverlayManagerBase.prototype.setup.call(this);
  this.placeOverlay();
  jQuery('.ip.iq').hide();
};

/**
 * Called when the overlay is ready to communicate.
 */
DestroyKeyOverlayManager.prototype.onOverlayReady = function () {
  var self = this;
  this.registerMessageHandler(MessageType.SIZING_INFO, false, function (data) {
    jQuery(this.item).height(data + 40).css({overflow: "hidden"});
  });


  this.registerMessageHandler(MessageType.REFRESH_READ_OVERLAYS, false, function() {
    self.controller.refreshReadOverlays();
  });

  this.windowResizeFunction = function () {
    this.postMessage(MessageType.SIZING_INFO);
  }.bind(this);
  jQuery(window).on('resize', this.windowResizeFunction);

  var destructionData = self.unwrapDestructionPackage(jQuery(this.item));
  this.postMessage(MessageType.SEND_CONTENTS, {
    destructionData: destructionData,
    userId:  PageScanner.getEmailAddress()
  });


  jQuery(this.item).addClass('messageGuardDestroyOverlayed');

  this.postMessage(MessageType.SIZING_INFO);
};

DestroyKeyOverlayManager.prototype.unwrapDestructionPackage = function(html) {
    var encodedData = jQuery('div[name="destruction-package"]', html).text();
    var decodedData = encoder.convert(encodedData, 'base64', 'utf8');
    return JSON.parse(decodedData);
};

/**
 * Cleanup this overlay manager.
 */
DestroyKeyOverlayManager.prototype.cleanup = function () {
  OverlayManagerBase.prototype.cleanup.call(this);
  jQuery(window).off('resize', this.windowResizeFunction);
};


module.exports = DestroyKeyOverlayManager;
