/**
 * Generic read overlay manager.
 * @module
 * @requires frontend/overlay-manager-base
 * @requires common/message-type
 */
"use strict";

var OverlayManagerBase = require('../../overlay-manager-base'),
    encoder            = require('../../../common/encoder'),
    Time               = require('../../../common/time'),
    MessageType        = require('../../../common/message-type'),
    urls               = require('../../../common/urls'),
    PackageWrapper     = require('./package-wrapper'),
    PageScanner        = require('./page-scanner'),
    MouseEvents        = require('./mouse-events');

var jQuery = require('jquery');

var threadIdRegex = /\[mg-id:[a-z0-9]{8}-[a-z0-9]{4}-[a-z0-9]{4}-[a-z0-9]{4}-[a-z0-9]{12}\]/;

var debugOverlay = false;

var CHECK_EXPIRED_INTERVAL_SEC = 10;

var gmailBoxViewURLComponents = [
    "#starred", 
    "#imp", 
    "#sent", 
    "#drafts", 
    "#label",
    "#chats",
    "#all",
    "#spam",
    "#trash",
    "#"];

//noinspection JSClosureCompilerSyntax
/**
 * Create a read overlay manager for the given item.
 * @param {!Element} item Item that is being overlayed.
 * @constructor
 * @extends module:frontend/overlay-manager-base
 * @alias module:frontend/generic/read-overlay-manager
 */
function SLKOverlayManager(item, windowUUID, controller) {
  OverlayManagerBase.call(this, item, windowUUID, controller);
  this.overlayURL = 'custom/gmail/slk-overlay.html';
  this.threadIds = [];
  this.boxRowData = {};
  this.expirationUpdateIntervals = {};
}

SLKOverlayManager.prototype = Object.create(OverlayManagerBase.prototype);
SLKOverlayManager.prototype.constructor = SLKOverlayManager;

/**
 * Setup and begin operating.
 */
SLKOverlayManager.prototype.setup = function () {
  this.placeOverlay();
};

SLKOverlayManager.prototype.isBoxView = function(url) {

    if(url.includes("#inbox")) {
        var hrefPieces = url.split("/");
        var lastPiece = hrefPieces[hrefPieces.length - 1].split("?")[0];

        if(lastPiece == "#inbox")
            return true;
        else
            return false;     
    }

    for(var i = 0; i < gmailBoxViewURLComponents.length; i++) {
        if(url.includes(gmailBoxViewURLComponents[i]))
            return true
    }
    return false;
};

SLKOverlayManager.prototype.getWindowLocationHash = function() {
    return window.location.hash.split('?')[0];
};

SLKOverlayManager.prototype.hashChanged = function (newURL) {

    var self = this;

    this.clearIntervals();
    if(!this.threadUpdateInterval) {
        this.threadUpdateInterval = window.setInterval(function() {
            // Display general UI
            self.scanBoxesForSLKThreads();
            self.removeReadThreadIds();
            self.currentURL = newURL;
        }, 500);
    }

};

SLKOverlayManager.prototype.scanBoxesForSLKThreads = function() {
    var boxRows = document.querySelectorAll('.zA');
    var self = this;

    var newThreadIds = [];
    for(var i = 0; i < boxRows.length; i++) {
        var emailBodySpan = jQuery('.y2', boxRows[i]);
        var subjectSpan = jQuery('.bog', boxRows[i])
        if(threadIdRegex.test(emailBodySpan.text())) {
            var idStr = emailBodySpan.text().match(threadIdRegex)[0];
            idStr = idStr.split(':')[1].replace(']','');
            emailBodySpan.text(emailBodySpan.text().replace(threadIdRegex, ''));

            var expirationLabel = document.createElement('div');
            expirationLabel.className = 'messageGuardExpirationLabel';
            expirationLabel.innerText = "Calculating...";
            subjectSpan.before(expirationLabel);

            this.boxRowData[idStr] = boxRows[i];
            if(!self.threadIds.includes(idStr))
                self.threadIds.push(idStr);
            newThreadIds.push(idStr);
        }
    }
    if(newThreadIds.length) {
        this.postMessage(MessageType.GET_MULTIPLE_EXPIRATION_DATA, {
            userId: PageScanner.getEmailAddress(),
            threadIds: newThreadIds
        });
    }
};

SLKOverlayManager.prototype.addBoxViewExpirationUI = function(data) {
    var self = this;
    var keyAttributes = data.keyAttributes;
    var originalThreadIds = data.originalThreadIds;
    originalThreadIds.forEach(function(threadId) {
        var rowElement = self.boxRowData[threadId];
        var expirationLabel = jQuery(".messageGuardExpirationLabel", rowElement);
        var keyData = keyAttributes[threadId];
        if(keyData) {
            if(keyData.privateKeyDestroyed) {
                expirationLabel.text('Unreadable');
                self.clearThreadInterval(threadId);
            }
            else {
                expirationLabel.text(Time.getExpirationText(keyData.created, keyData.lifespan));
                self.clearThreadInterval(threadId);
                self.expirationUpdateIntervals[threadId] = setInterval(function() {
                    expirationLabel.text(Time.getExpirationText(keyData.created, keyData.lifespan));
                }, 1000);
            }

        }
        else {
            expirationLabel.text("Unopened");
        }     
    });
};

SLKOverlayManager.prototype.removeReadThreadIds = function() {
    var readPreviews = document.querySelectorAll('.iA.g6');
    readPreviews.forEach(function(item) {
        item.innerText = item.innerText.replace(threadIdRegex, '');
    });
};

SLKOverlayManager.prototype.clearThreadInterval = function(threadId) {
    if(this.expirationUpdateIntervals[threadId])
        clearInterval(this.expirationUpdateIntervals[threadId]);
};

/**
 * Called when the overlay is ready to communicate.
 */
SLKOverlayManager.prototype.onOverlayReady = function () {
  var self = this;

  this.registerMessageHandler(MessageType.SEND_THREAD_EXPIRATION, false, function (data) {
    if(!data) {
        return;
    }
    if(self.currentURL == data.currentURL) {
        self.updateThreadExpirationUI(data);
        self.addThreadMakeUnreadableBtn(data);
    }
  });

  this.registerMessageHandler(MessageType.THREAD_KEY_PERSISTED, false, function(data) {
    self.controller.refreshReadOverlays();
    if(data.fromModal) {
        document.querySelector('#thread-' + data.threadId).remove();
        self.tryRemoveModal();
    }
    if(!self.isBoxView(location.href)) 
        self.postGetThreadExpiration();
  });

  this.registerMessageHandler(MessageType.SEND_MULTIPLE_EXPIRATION_DATA, false, function(data) {
    self.addBoxViewExpirationUI(data);
  });

  this.registerMessageHandler(MessageType.THREAD_KEY_DESTROYED, false, function(data) {
        self.controller.refreshReadOverlays();
        var makeUnreadableBtn = document.querySelector('#make-unreadable-btn');
        if(makeUnreadableBtn)
            makeUnreadableBtn.remove();
        self.clearExpirationLabel();

        if(data.fromModal) {
            document.querySelector('#thread-' + data.threadId).remove();
            self.tryRemoveModal();
        }

        if(!self.isBoxView(location.href))
            self.postGetThreadExpiration();

        setTimeout(function() {
            self.sendKeyDestroyedNotice(data.threadId, data.onThread, data.threadMetadata);
        }, 250);


  });

  this.registerMessageHandler(MessageType.SEND_EXPIRED_DATA, false, function(expiredData) {
    if(expiredData && expiredData.length > 0)
        self.addExpirationModal(expiredData);
  });

  setInterval(function() {
    self.postMessage(MessageType.GET_EXPIRED_DATA, PageScanner.getEmailAddress());
  },5000);

  this.hashChanged(window.location.href);
};

SLKOverlayManager.prototype.updateThreadExpirationUI = function(expirationData) {

  var self = this;

  this.clearIntervals();
  //Check for label first
  var expirationLabel = document.querySelector('.messageGuardExpirationLabel');
  if (!expirationLabel) {
      expirationLabel = this.createExpirationLabel(expirationData);
      jQuery('.hP:first').before(expirationLabel);
  }
};

SLKOverlayManager.prototype.createExpirationLabel = function(keyData) {
    var expirationLabel = document.createElement('div');
    expirationLabel.className = 'messageGuardExpirationLabel';

    if(keyData.privateKeyDestroyed) 
        expirationLabel.innerText = "Unreadable";
    else {
        expirationLabel.innerText = Time.getExpirationText(keyData.created, keyData.lifespan);

        if(expirationLabel.innerText != "Unreadable") {

            var utcTimeLeft = (keyData.created + keyData.lifespan) - Time.getUTCTime();
            var secLeft = Math.floor(utcTimeLeft / 1000) % 60;
            var initialWait = 60 - secLeft + 1;
            
            self.expirationTimeout = window.setTimeout(function() {
                expirationLabel.innerText = Time.getExpirationText(keyData.created, keyData.lifespan);
                self.expirationInterval = window.setInterval(function() {
                    expirationLabel.innerText = Time.getExpirationText(keyData.created, keyData.lifespan);
                }, 60 * 1000);
            }, initialWait * 1000);
        }
    }
};

SLKOverlayManager.prototype.clearExpirationLabel = function() {

    this.clearIntervals();
    var label = document.querySelector(".messageGuardExpirationLabel");
    if(label)
        label.innerText = "Unreadable";
};

SLKOverlayManager.prototype.addThreadMakeUnreadableBtn = function(data) {
    var self = this;

    if(!data.privateKeyDestroyed && !Time.isExpired(data.created, data.lifespan) && !document.querySelector('#make-unreadable-btn')) {
        var threadBar = document.querySelector(".ha");
        var btn = document.createElement('button');
        btn.innerText = 'Make Unreadable';
        btn.style.float = 'right';
        btn.className += 'btn btn-success';
        btn.id = 'make-unreadable-btn';

        btn.addEventListener('click', function(e) {
            self.postMessage(MessageType.DESTROY_THREAD_KEY, {threadId: data.threadId, userId: PageScanner.getEmailAddress(), fromModal: false});
            btn.innerText = 'Working...';
            btn.className += " disabled";
        });

        threadBar.appendChild(btn);
    }
};

SLKOverlayManager.prototype.placeOverlay = function () {
  var self = this;

  if (this.slkOverlayIFrame) {
    return;
  }

  this.slkOverlayIFrame = document.createElement('iframe');

  this.slkOverlayIFrame.src = urls.getOverlayUrl(this.overlayURL, {window_uuid: this.windowUUID});
  if(debugOverlay) {
    this.slkOverlayIFrame.style.cssText = 'background-color:green;height:20px !important;width:20px !important;z-index:5;position:fixed;top:10px;left:10px;';
  }
  else {
    this.slkOverlayIFrame.style.display = 'none';
    this.slkOverlayIFrame.width = '0';
    this.slkOverlayIFrame.height = '0';
  }

    this.slkOverlayIFrame.tabIndex = '-1';
    this.slkOverlayIFrame.title = 'slk-overlay-iframe'; // For screen readers that might see this.

  // Try every half-second to add the key manager iframe, until document.body is available.
  var tryIntervalID = null,
      tryAdd        = (function () {

        if (document.body) {
          window.clearInterval(tryIntervalID);
          document.body.appendChild(this.slkOverlayIFrame);
        }
    }).bind(this);

  this.slkOverlayIFrame.addEventListener('load', function () {
    this.channel = new MessageChannel();
    this.messagePort = this.channel.port1;
    this.messagePort.onmessage = this.handleMessage.bind(this);

    self.slkOverlayIFrame.contentWindow.postMessage('initiate connection', '*', [self.channel.port2]);
    self.onOverlayReady();
  }.bind(this), false);

  tryIntervalID = window.setInterval(tryAdd, 0.5 * 1000);
};

SLKOverlayManager.prototype.removeExpirationModal = function() {
    var modal = document.querySelector("#expiration-modal");
    if(modal)
        modal.remove();
};

SLKOverlayManager.prototype.addExpirationModal = function(data) {

    var self = this;

    if(document.querySelector("#expiration-modal"))
        return;

    var modal = document.createElement('div');
    modal.id = "expiration-modal";
    modal.className += 'container';
    modal.style.cssText = 'background-color:white;z-index:50;position:fixed;left: 50%;  top: 30%;transform: translate(-50%, -50%);padding: 15px;' +
                            'border-style:solid;border-width: 5px;border-color: #B33A3A;';

    var modalHtml = '<div class="row text-center">' +
                        '<h1>MessageGuard</h1>' +
                    '</div>' +
                    '<div class="row">' +
                        '<p style="text-indent: 50px;">' +
                            'One or more MessageGuard protected email threads expired. Use the information and options below to manage these messages.' +
                        '</p>' +
                    '</div>';

    modal.innerHTML = modalHtml;

    //for loop adding to html with thread function

    console.log(data);
    for(var i = 0; i < data.length; i++) {
        modal.appendChild(self.expiredThreadHtml(data[i], i + 1));
    }

    document.body.appendChild(modal);

};

SLKOverlayManager.prototype.expiredThreadHtml = function(threadData, threadNum) {

    var self = this;

    var threadHtml = '<div class="row">' +
                        '<h2>Message Thread #--threadNum Summary</h2>' +
                        '<p>Subject: --subject</p>' + 
                        '<p>Participants: --participants</p>' +
                        '<p>Started: --started</p>' +
                      '</div>' +
                       '<div class="row text-center" style="margin-top:10px;">' +
                            '<div id="request-buttons" class="btn-toolbar" role="toolbar" style="text-align: center;">' +
                                '<div class="btn-group" role="group" style="float: none !important;">' +
                                    '<button id="modal-make-thread-unreadable-btn" type="button" class="btn btn-success text-center">Make Thread Unreadable</button>' +
                                '</div>' +
                                '<div class="btn-group" role="group" style="float: none !important;display:none;">' +
                                    '<button id="go-to-thread-btn" type="button" class="btn btn-warning text-center">Go to Thread</button>' +
                                '</div>' +
                                '<div class="btn-group" role="group" style="float: none !important;">' +
                                    '<button id="modal-keep-thread" type="button" class="btn btn-danger text-center">Keep Thread</button>' +
                                '</div>' +
                            '</div>' +
                       '</div>';


    threadHtml = threadHtml.replace("--threadNum", '' + threadNum);
    threadHtml = threadHtml.replace("--subject", threadData.metadata.subject);


    var recipients = threadData.metadata.recipients;
    var participants = '';
    for(var i = 0; i < recipients.length; i++) {
        participants += recipients[i];
        if ( i != recipients.length - 1 )
            participants += ', ';
    }

    if(!participants.includes(PageScanner.getEmailAddress()))
        participants += ", " + PageScanner.getEmailAddress();
    threadHtml = threadHtml.replace("--participants", participants);


    threadHtml = threadHtml.replace("--started", new Date(threadData.created).toDateString());

    var div = document.createElement('div');
    div.innerHTML = threadHtml;
    div.id = "thread-" + threadData.metadata.threadId;


    // Button event handlers
    var makeUnreadableBtn = div.querySelector("#modal-make-thread-unreadable-btn");
    makeUnreadableBtn.addEventListener('click', function() {

        if(threadData.metadata.urlThreadId) {
            window.location.replace("https://mail.google.com/mail/u/0/#inbox/" + threadData.metadata.urlThreadId);
            self.postMessage(MessageType.DESTROY_THREAD_KEY, {
                threadId: threadData.metadata.threadId,
                userId: PageScanner.getEmailAddress(),
                fromModal: true,
                onThread: true
            });
            makeUnreadableBtn.innerText = 'Working...'
            keepBtn.className += ' disabled';
            makeUnreadableBtn.className += ' disabled';
        }
        else {
            self.postMessage(MessageType.DESTROY_THREAD_KEY, {
                threadId: threadData.metadata.threadId,
                userId: PageScanner.getEmailAddress(),
                fromModal: true,
                onThread: false
            });            
        }

    });

    div.querySelector("#go-to-thread-btn").addEventListener('click', function() {

    });

    var keepBtn = div.querySelector("#modal-keep-thread");
    keepBtn.addEventListener('click', function() {
        self.postMessage(MessageType.PERSIST_THREAD_KEY, {
            threadId: threadData.metadata.threadId,
            userId:   PageScanner.getEmailAddress(),
            fromModal: true
        });
        keepBtn.innerText = 'Working...';
        keepBtn.className += ' disabled';
        makeUnreadableBtn.className += ' disabled';
    });

    return div;
};

SLKOverlayManager.prototype.tryRemoveModal = function () {
    var modal = document.querySelector('#expiration-modal');
    
    if(modal && modal.childNodes.length == 2)
        modal.remove();
};

/**
 * Cleanup this overlay manager.
 */
SLKOverlayManager.prototype.cleanup = function () {
  OverlayManagerBase.prototype.cleanup.call(this);
  jQuery(window).off('resize', this.windowResizeFunction);
};

SLKOverlayManager.prototype.clearIntervals = function() {
    if(this.expirationTimeout) {
        clearTimeout(this.expirationTimeout);
        this.expirationTimeout = null;
    }
    if(this.expirationInterval) {
        clearInterval(this.expirationInterval);
        this.expirationInterval = null;
    }
};

SLKOverlayManager.prototype.postGetThreadExpiration = function() {
    var newURL = location.href;
    var urlPieces = newURL.split("/");
    var urlThreadId = urlPieces[urlPieces.length - 1].split("?")[0];

    this.postMessage(MessageType.GET_THREAD_EXPIRATION, {urlThreadId: urlThreadId, currentURL: newURL, userId: PageScanner.getEmailAddress()});
};

SLKOverlayManager.prototype.refresh = function(threadId) {

    this.refreshKnownThreads();

    if(threadId) {
        var modalThreadElement = document.querySelector('#thread-' + threadId);
        if(modalThreadElement) {
            modalThreadElement.remove();
            this.tryRemoveModal();
        }
    }
};

SLKOverlayManager.prototype.refreshKnownThreads = function() {
    console.log("Refreshing known threads");
    console.log(this.threadIds);
    this.postMessage(MessageType.GET_MULTIPLE_EXPIRATION_DATA, {
        userId: PageScanner.getEmailAddress(),
        threadIds: this.threadIds
    });
};

SLKOverlayManager.prototype.getDestroyedNoticeBody = function(threadId, addThreadId) {
  var destroyedPackage = JSON.stringify({threadId: threadId, destroyedBy: PageScanner.getEmailAddress()});
  var packageDiv = '<div style="font-size: 9px;">' +
                  '---Begin MessageGuard Key Destruction Notice Package---' +
                  '<br><br>' +
                  '<div name="destruction-package">' +
                   encoder.convert(destroyedPackage, 'utf8', 'base64') +
                   '</div>' +
                  '<br><br>' +
                  '---End MessageGuard Key Destruction Notice Package---' +
                  '<br><br>' +
                   '</div>';
  var requestBody = '<div name="keyDestructionEmail">';
  if(addThreadId) {
      requestBody += '<div name="messageGuardThreadId" style="display:none;">[mg-id:' + threadId + ']</div>';
  }
      requestBody += 'MessageGuard Short-Lived Key Destruction Notice' +
                    '<br><br>' +
                    PageScanner.getEmailAddress() + ' destroyed their key for this encrypted thread. This means ' + PageScanner.getEmailAddress() +
                    ' will no longer be able to read any encrypted message related to this thread.'  +
                    '<br><br>' +
                    'Please open this email on the device you have MessageGuard installed on so MessageGuard can be updated with this information.' +
                    '<br><br>' +
                    packageDiv +
                    '</div>';

    return requestBody;
};

SLKOverlayManager.prototype.sendKeyDestroyedNotice = function(threadId, onThread, metadata) {
    
    if(onThread)
        this.sendThreadEmail(this.getDestroyedNoticeBody(threadId, true));
    else {
        this.sendEmail(metadata.recipients.toString(), metadata.subject, this.getDestroyedNoticeBody(threadId, false), true);
    }
};

SLKOverlayManager.prototype.sendEmail = function (to, subject, body, autoSend) {

  var onMessageOpened = function (composeContainer) {
    var toField      = jQuery(composeContainer).find('textarea[name="to"]'),
        subjectField = jQuery(composeContainer).find('input[name="subjectbox"]'),
        bodyArea     = jQuery(composeContainer).find('[contenteditable]:last'); // :last not necessary at the moment,
                                                                                // just a precaution.

    if (typeof to == 'object') {
      toField.text(to.join(', '));
      setTimeout(function () {
        toField.blur();
        subjectField.focus();
        MouseEvents.simulateMouseClick(subjectField[0]);
      }, 200);
    } else {
      toField.text(to);
    }

    subjectField.val(subject);
    bodyArea.html(body + bodyArea.html());

    if(autoSend) {
      var sendButton = jQuery('[role="button"]:contains(Send)');
      MouseEvents.simulateMouseClick(sendButton);
    }
  }

  var clickComposeAndWait = function () {
    return new Promise(function (resolve) {

      var composeWindowSelector = '.nH.Hd',
          initialComposeWindows = jQuery(composeWindowSelector);

      var lookForNewComposeWindow = function () {
        var currentComposeWindows = jQuery(composeWindowSelector),
            newComposeWindows     = currentComposeWindows.not(initialComposeWindows);

        if (newComposeWindows.length == 0) {
          // We haven't seen a new one yet, wait another quarter-second.
          setTimeout(lookForNewComposeWindow, 100);
          return;
        }

        resolve(newComposeWindows.first());
      };

      // Open a new compose window.
      var composeButton = jQuery('[role="button"]:contains(COMPOSE)');
      MouseEvents.simulateMouseClick(composeButton);

      // Wait for the new window to open.
      lookForNewComposeWindow();
    });
  };

  var maximizedComposeBackground = jQuery('.aSs');

  var shrinkCurrentWindow;

  if (maximizedComposeBackground.css('visibility') == 'visible') {

    // We'll need to minimize the current compose window, and then after that we can
    // click "Compose".

    shrinkCurrentWindow = new Promise(function (resolve) {

      // Like lookForNewComposeWindow, this function repeatedly checks until the
      // compose background is hidden. before resolving this promise.
      var waitForMinimized = function () {
        if (maximizedComposeBackground.css('visibility') == 'visible') {
          setTimeout(waitForMinimized, 100);
          return;
        }

        resolve();
      };

      // Click to minimize the compose window, and wait for it to get minimized.
      MouseEvents.simulateMouseClick(maximizedComposeBackground);
      waitForMinimized();
    });
  } else {
    shrinkCurrentWindow = Promise.resolve();
  }

  shrinkCurrentWindow.then(function () {
    return clickComposeAndWait().then(onMessageOpened);
  });
};

OverlayManagerBase.prototype.sendThreadEmail = function(body) {
  jQuery('.ip.iq').show();

  this.controller.suppressEncryptedReply = true;

  var self = this;
  //Hide the reply/forward area.
  var added = false;
  var sent = false;
  var observer; 
  observer = new MutationObserver(function(mutations) {
    mutations.forEach(function(mutation) {
      var composeArea = document.querySelector('.Am.aO9.Al.editable.LW-avf');
      if (!added && composeArea) {
        composeArea.innerHTML = body;
        added = true;
      }
      var sendBtn = jQuery("div[role='button']:contains('Send unencrypted')");
      if (added && !sent && composeArea && sendBtn.length) {
        var hasBody = composeArea.innerHTML.includes(body);
        if(hasBody) {
            setTimeout(function() {
                MouseEvents.simulateMouseClick(sendBtn);
            },500);
            sent = true;
            setTimeout(function() {
                self.controller.suppressEncryptedReply = false;
            },1500);
            observer.disconnect();      
        }  
      }
    });
  });

  var config = {subtree: true, childList: true};
  observer.observe(document.querySelector('.Bu'),config);

  MouseEvents.simulateMouseClick(jQuery('.ams.bkH'));
};

module.exports = SLKOverlayManager;
