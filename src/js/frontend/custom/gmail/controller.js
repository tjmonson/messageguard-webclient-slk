/**
 * Controller that can be used with Gmail.
 * @module
 * @requires frontend/controller-base
 * @requires frontend/generic/read-overlay-manager
 * @requires frontend/generic/compose-overlay-manager
 */
"use strict";

var ControllerBase        = require('../../controller-base'),
    ReadOverlayManager    = require('./read-overlay-manager'),
    ComposeOverlayManager = require('./compose-overlay-manager'),
    RequestOverlayManager = require('./request-overlay-manager'),
    ResponseOverlayManager = require('./response-overlay-manager'),
    DestroyKeyOverlayManager = require('./destroy-key-overlay-manager');

var jQuery = require('jquery');

var PackageWrapper = require('./package-wrapper'),
    Tutorial       = require('./tutorial');

var styleString = require('fs').readFileSync(__dirname + '/../../../../../build/staging/sass/css/messageguard.css',
                                             'utf8');

/**
 * Creates a controller suitable for gmail.
 * @constructor
 * @extends module:frontend/controller-base
 * @alias module:frontend/generic/controller
 */
function Controller() {
  // Insert a style sheet into the head.
  var style = document.createElement('style');
  style.type = 'text/css';
  if (style.styleSheet) {
    style.styleSheet.cssText = styleString;
  } else {
    style.appendChild(document.createTextNode(styleString));
  }
  (document.head || document.getElementsByTagName('head')[0]).appendChild(style);

  ControllerBase.call(this, '.I5', undefined, 'div[name="encryptedEmail"],div[name="requestEmail"],' + 
    'div[name="responseEmail"],div[name="keyDestructionEmail"]');

  // Set the overlays we want to use.
  this.constructors.read = ReadOverlayManager;
  this.constructors.compose = ComposeOverlayManager;
  this.constructors.request = RequestOverlayManager;
  this.constructors.response = ResponseOverlayManager;
  this.constructors.destroyKey = DestroyKeyOverlayManager;
}

Controller.prototype = new ControllerBase();
Controller.prototype.constructor = Controller;

/**
 * Start the controller. If both skipReadOverlays and skipComposeOverlays are true, ControllerBase will handle watching
 * for moved and removed overlays.
 */
Controller.prototype.start = function () {
  ControllerBase.prototype.start.call(this);

  // Grab subject items and annotate them.
  this.observer.observe({added: true, modified: true, subtree: true}, '.y6>span:first-child', function (item) {
    if (PackageWrapper.isWrappedSubject(item.innerText)) {
      item.innerText = PackageWrapper.unwrapSubject(item.innerText);
      var encryptedLabel = document.createElement('div');
      encryptedLabel.className = 'messageGuardLockDiv';
      encryptedLabel.innerText = 'Encrypted';
      jQuery(item).before(encryptedLabel);
    }
    else if (PackageWrapper.isWrappedRequest(item.innerText)) {
      item.innerText = PackageWrapper.unwrapRequest(item.innerText);
      var requestLabel = document.createElement('div');
      requestLabel.className = 'messageGuardRequestDiv';
      requestLabel.innerText = 'Decryption Request';
      jQuery(item).before(requestLabel);
    }
    else if (PackageWrapper.isWrappedResponse(item.innerText)) {
      item.innerText = PackageWrapper.unwrapResponse(item.innerText);
      var responseLabel = document.createElement('div');
      responseLabel.className = 'messageGuardResponseDiv';
      responseLabel.innerText = 'Decryption Response';
      jQuery(item).before(responseLabel);
    }
  }.bind(this));

  // Run after everything is finished.
  var tutorial = new Tutorial(this.overlayManagers);
  window.setTimeout(function () {
    tutorial.initialRun();
  }, 10);
};

/**
 * Scan the given node for read packages and overlay them.
 * @param {!Element} node Node to scan.
 */
Controller.prototype.scanForReadPackage = function (node) {
  var encryptedPackage = node.querySelector('div[name="package"]');
  if (encryptedPackage) {
    this.addOverlayManager(node, this.constructors.read);
    return;
  }

  var requestPackage = node.querySelector('div[name="request-package"]');
  if(requestPackage) {
    this.addOverlayManager(node, this.constructors.request);
    return;
  }

  var responsePackage = node.querySelector('div[name="response-package"]');
  if(responsePackage) {
    this.addOverlayManager(node, this.constructors.response);
    return;
  }

  var destructionPackage = node.querySelector('div[name="destruction-package"]');
  if(destructionPackage) {
    this.addOverlayManager(node, this.constructors.destroyKey);
    return;
  }
};

Controller.prototype.refreshReadOverlays = function () {
  this.overlayManagers.forEach(function(manager) {

    if(manager.constructor.name === "ReadOverlayManager") {
      manager.resendContents();
    }
  });
};

Controller.prototype.refreshSLKOverlay = function (threadId) {
  this.slkOverlayManager.refresh(threadId);
};


module.exports = Controller;
