/**
 * Mixin for allowing windows to communicate using message channels.
 * @module
 */
"use strict";

var MessageType = {
  UNLISTED:                "unlisted",
  OVERLAY_READY:           "overlay_ready",
  MESSAGE_ENCRYPTED:       "message_encrypted",
  CANCEL:                  "cancel",
  CHANGE_BACKGROUND_COLOR: "change_background_color",
  DISCARD_DRAFT:           "discard_draft",
  SAVE_DRAFT:              "save_draft",
  COMPOSE_INSTALL_PROMPT:  "compose_install_prompt",
  GET_RECIPIENTS:          'get_recipients',
  GET_CONTENTS:            'get_contents',
  SEND_CONTENTS:           'send_contents',
  FOCUS_CHANGED:           'focus_changed',
  SIZING_INFO:             'sizing_info',
  TUTORIAL_MASK_PLACE:     'tutorial_mask_place',
  TUTORIAL_MASK_REMOVE:    'tutorial_mask_remove',
  REQUEST_DECRYPTION_KEY:  'request_decryption_key',
  HIDE_REPLY_FORWARD:      'hide_reply_forward',
  SEND_REQUEST_RESPONSE:   'send_request_response',
  REFRESH_READ_OVERLAYS:   'refresh_read_overlays',
  KEY_EXPIRATION_DATA:     'key_expiration_data',
  GET_THREAD_EXPIRATION:   'get_thread_expiration',
  SEND_THREAD_EXPIRATION:  'send_thread_expiration',
  DESTROY_THREAD_KEY:      'destroy_thread_key',
  THREAD_KEY_DESTROYED:    'thread_key_destroyed',
  GET_METADATA:            'get_metadata',
  GET_EXPIRED_DATA:        'get_expired_data',
  SEND_EXPIRED_DATA:       'send_expired_data',
  PERSIST_THREAD_KEY:      'persist_thread_key',
  THREAD_KEY_PERSISTED:    'thread_key_persisted',
  CANNOT_COMPOSE:          'cannot_compose',
  GET_MULTIPLE_EXPIRATION_DATA:   'get_multiple_expiration_data',
  SEND_MULTIPLE_EXPIRATION_DATA:  'send_multiple_expiration_data'
};

module.exports = MessageType;
