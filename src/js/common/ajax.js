var assign = require('lodash.assign');

var defaultOptions = {
  type:            'GET',
  responseType:    'text',
  contentType:     false,
  withCredentials: false,
  data:            '',
  success:         null,
  error:           null
};

// Adapted from http://stackoverflow.com/a/22816079/3403756
function Ajax(options) {
  var ajaxOptions = {};

  assign(ajaxOptions, defaultOptions);
  assign(ajaxOptions, options);

  var xhr = new XMLHttpRequest();
  xhr.open(ajaxOptions.type, ajaxOptions.url, true);

  if (ajaxOptions.contentType) {
    xhr.setRequestHeader('Content-type', ajaxOptions.contentType);
  }

  xhr.responseType = ajaxOptions.responseType;
  xhr.withCredentials = ajaxOptions.withCredentials;

  xhr.onload = function () {
    // Successful request
    if (xhr.status == 200) {
      if (typeof ajaxOptions.success == 'function') {
        ajaxOptions.success(xhr.response, xhr);
      }
    } else {
      if (typeof ajaxOptions.error == 'function') {
        ajaxOptions.error(xhr.status, xhr.response, xhr);
      }
    }
  };

  xhr.onerror = function () {
    if (typeof ajaxOptions.error == 'function') {
      ajaxOptions.error(xhr.status, xhr.response, xhr);
    }
  };

  xhr.send(ajaxOptions.data);
}

// Verb-specific helper-methods
['get', 'post', 'put', 'patch', 'delete'].forEach(function (verb) {
  Ajax[verb] = function (options) {
    options['type'] = verb.toUpperCase();
    return Ajax(options);
  };
});

module.exports = Ajax;
