/**
 * Functions for working with time
 */
var expireTextPrefix = "Expires in: ";

function getUTCTime(date) {
    if(date == null) {
        date = new Date();
    }
    return Date.UTC(date.getUTCFullYear(),date.getUTCMonth(), date.getUTCDate() , 
        date.getUTCHours(), date.getUTCMinutes(), date.getUTCSeconds(), date.getUTCMilliseconds());
}

function isExpired(utctime, lifespan) {
    var nowUTC = getUTCTime();
    var difference = nowUTC - utctime;
    return difference >= lifespan;
}

function pluralize(num, word) {
    if(num == 1) 
        return word;
    else
        return word + 's';
}

function getExpirationText(created, lifespan, doExpirationDate) {
    
    if(isExpired(created, lifespan))
        return "Expired";

    if(doExpirationDate) {
        return "Expires on: " + new Date(created + lifespan).toString();
    }
    else {
        var nowUTC = getUTCTime();
        var timeLeft = (created + lifespan) - nowUTC;

        var totalSecondsLeft = Math.floor(timeLeft / 1000);
        var secondsLeft =  totalSecondsLeft % 60;
        
        var totalMinutesLeft = Math.floor(totalSecondsLeft / 60);
        var minutesLeft = totalMinutesLeft % 60;

        var totalHoursLeft = Math.floor(totalMinutesLeft / 60);
        var hoursLeft = totalHoursLeft % 24;

        var daysLeft = Math.floor(totalHoursLeft / 24);

        if(daysLeft) {
            if(daysLeft >= 2)
                return expireTextPrefix + daysLeft + " days";
            else
                return expireTextPrefix + "1 day " + hoursLeft + pluralize(hoursLeft, "hour");
        }
        else if (hoursLeft) {
            if(hoursLeft >= 2)
                return expireTextPrefix + hoursLeft + " hours";
            else
                return expireTextPrefix + "1 hour " + minutesLeft + pluraize(minutesLeft);
        }
            
        else if (minutesLeft) {
            if(minutesLeft >= 2)
                return expireTextPrefix + minutesLeft + " min";
            else
                return expireTextPrefix + " 1 min " + secondsLeft + " sec";
        }
        else if (secondsLeft)
            return expireTextPrefix + secondsLeft + " sec";
        //return "Expires in: " + daysLeft + "d:" + hoursLeft + "h:" + minutesLeft + "m";
    }
}

module.exports = {
  getUTCTime: getUTCTime,
  isExpired: isExpired,
  getExpirationText: getExpirationText
};
