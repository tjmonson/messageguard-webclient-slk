var Errors = require('../../errors'),
    UUID   = require('../../uuid');

// Function to issue messages from this worker thread. Will be provided by
// key-management/communicator/receiver/worker.js on startup, via the
// module.exports.injectRequester() function below. The receiver/worker.js module
// ensures that injectRequester() is called before any other key management code
// might run and need to use these storage objects.
var issueStorageRequest = null;

function makeRequest(storageName, methodName, args) {
  if (!issueStorageRequest) {
    throw new Errors.StorageError('Cannot invoke worker object functions before the worker has fully initialized them.');
  }

  return issueStorageRequest(storageName, {
    methodName: methodName,
    args:       Array.prototype.slice.call(args)
  });
}

function WorkerStorage(storageName) {
  this.storageName = storageName;
}

WorkerStorage.prototype._makeRequest = function (methodName, args) {
  return makeRequest(this.storageName, methodName, args);
};

WorkerStorage.prototype.getItem = function () {
  return this._makeRequest('getItem', arguments);
};

WorkerStorage.prototype.setItem = function () {
  return this._makeRequest('setItem', arguments);
};

WorkerStorage.prototype.removeItem = function () {
  return this._makeRequest('removeItem', arguments);
};

WorkerStorage.prototype.getKeys = function () {
  return this._makeRequest('getKeys', arguments);
};

WorkerStorage.prototype.clear = function () {
  return this._makeRequest('clear', arguments);
};

function WorkerCookies() {}

WorkerCookies.prototype._makeRequest = makeRequest.bind(null, 'cookies');

WorkerCookies.prototype.getCookie = function () {
  return this._makeRequest('getCookie', arguments);
};

WorkerCookies.prototype.setCookie = function () {
  return this._makeRequest('setCookie', arguments);
};


function WorkerEvents() {
  this.listeners = {};
};

WorkerEvents.prototype._makeRequest = makeRequest.bind(null, 'eventEmitter');

WorkerEvents.prototype._registerCallback = function (cb, once) {
  var callbackUUID = this._getCallbackUUID(cb);

  if (!callbackUUID) {
    callbackUUID = UUID();

    var self = this;
    this.listeners[callbackUUID] = {
      originalCB: cb, // We need to store the original callback so as to look up its UUID in the future.
      fire:       function (event) {
        if (once) {
          delete self.listeners[callbackUUID];
        }

        cb(event);
      }
    }
  }

  return callbackUUID;
};

WorkerEvents.prototype._getCallbackUUID = function (cb) {
  var uuids = Object.keys(this.listeners);
  for (var i = 0; i < uuids.length; i++) {
    if (this.listeners[uuids[i]].originalCB === cb) {
      return uuids[i];
    }
  }

  return null;
};

WorkerEvents.prototype.emit = function () {
  return this._makeRequest('emit', arguments);
};

WorkerEvents.prototype.on = function (eventName, cb) {
  var callbackUUID = this._registerCallback(cb, false);

  return this._makeRequest('on', [eventName, callbackUUID]);
};

WorkerEvents.prototype.once = function (eventName, cb) {
  var callbackUUID = this._registerCallback(cb, true);

  return this._makeRequest('once', [eventName, callbackUUID]);
};

WorkerEvents.prototype.off = function (eventName, cb) {
  var callbackUUID = this._getCallbackUUID(cb);

  if (!callbackUUID) {
    return Promise.resolve();
  }

  delete this.listeners[callbackUUID];

  return this._makeRequest('off', [eventName, callbackUUID]);
};

WorkerEvents.prototype.processForwardedEvent = function (callbackUUID, data) {
  if (callbackUUID in this.listeners) {
    this.listeners[callbackUUID].fire(data);
  }
};

module.exports = {
  local:           new WorkerStorage('local'),
  session:         new WorkerStorage('session'),
  cookies:         new WorkerCookies(),
  eventEmitter:    new WorkerEvents(),
  injectRequester: function (requesterFunc) {
    issueStorageRequest = requesterFunc;
  }
};
