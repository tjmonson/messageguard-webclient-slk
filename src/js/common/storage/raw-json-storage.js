var format = require('util').format;

var BaseStorage    = require('./base-storage'),
    Errors         = require('../errors'),
    StorageObjects = require('./storage-objects');

/**
 *
 * Central storage module for key management. This is necessary because:
 *
 *   1) Kango tries to manage storage itself by JSON-encoding and -decoding all values placed
 *      in localStorage. This interferes with other code we have going on, and we don't want
 *      to worry about making sure everything that goes in localStorage is JSON-parsable.
 *
 * To address this, all reads and writes to storage pass through a JSON-encode/-decode
 * process, to ensure that everything in localStorage is JSON-decodable.
 */


function BaseJsonStorage(storageType) {
  if (storageType in BaseJsonStorage._storageObjects) {
    BaseStorage.call(this, storageType);
  } else {
    throw new Errors.StorageError('Invalid storageType: only [local, session] are allowed.');
  }
}

BaseJsonStorage.prototype = Object.create(BaseStorage.prototype);
BaseJsonStorage.constructor = BaseJsonStorage;

BaseJsonStorage._storageObjects = {
  local:   StorageObjects.local,
  session: StorageObjects.session
};

BaseJsonStorage.prototype._getRawKeys = function () {
  return this._getStorage().getKeys();
};

BaseJsonStorage.prototype._getStorage = function () {
  return BaseJsonStorage._storageObjects[this.storageType];
};

/**
 * Retrieves an item from storage.
 * Undoes the JSON-encoding that was performed during setItem.
 *
 * @param {string} key
 * @returns {*}
 */
BaseJsonStorage.prototype._getItem = function (key) {
  return this._getStorage().getItem(key).then(function (valueString) {
    if (!valueString) {
      return null;
    }

    try {
      return JSON.parse(valueString);
    } catch (e) {
      return null;
    }
  });
};

/**
 * Stores an item in storage.
 * Performs JSON-encoding. to make kango happy.
 *
 * @param {string} key
 * @param {*} value
 */
BaseJsonStorage.prototype._setItem = function (key, value) {
  return this._getStorage().setItem(key, JSON.stringify(value));
}

/**
 * Removes an item from storage.
 *
 * @param {string} key
 */
BaseJsonStorage.prototype._removeItem = function (key) {
  return this._getStorage().removeItem(key);
};

function LocalJsonStorage() {
  BaseJsonStorage.call(this, 'local');
}

function SessionJsonStorage() {
  BaseJsonStorage.call(this, 'session');
}

LocalJsonStorage.prototype = Object.create(BaseJsonStorage.prototype);
SessionJsonStorage.prototype = Object.create(BaseJsonStorage.prototype);

module.exports = {
  local:   new LocalJsonStorage(),
  session: new SessionJsonStorage()
};
