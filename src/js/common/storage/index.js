var RawJsonStorage       = require('./raw-json-storage'),
    SessionCookieStorage = require('./session-cookie-storage'),
    SyncedMemoryStorage  = require('./synced-memory-storage'),
    urls                 = require('../urls');

module.exports = {
  local:          RawJsonStorage.local,
  session:        RawJsonStorage.session,
  managedSession: SessionCookieStorage
};

// If we're running as a bookmarklet, use synced memory storage,
// since session cookie storage uses cookies that could be leaked
// to our server.
//
// For the moment though, just used session cookie storage everywhere.
if (!urls.isExtension && false) {
  module.exports.managedSession = SyncedMemoryStorage;
}

// Ensure that if we change how storage is implemented, clients won't try and read
// bad data. Only for development purposes.
var STORAGE_VERSION = '0.1';
module.exports.local.getItem('storage-version').then(function (version) {
  if (version !== STORAGE_VERSION) {
    return Promise.all([
                         module.exports.local.clear(),
                         module.exports.session.clear(),
                         module.exports.managedSession.clear(),
                       ]).then(function () {
      return module.exports.local.setItem('storage-version', STORAGE_VERSION);
    });
  }
}).then(function () {
  module.exports.managedSession.initialize();
});
