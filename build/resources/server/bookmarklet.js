/**
 * @param id
 * @param url
 * @param callback
 * @param isStylesheet
 */
function insertResource(id, url, callback, isStylesheet) {
  if (isStylesheet) {
    var element = document.createElement("link");
    if (id) element.id = id;
    element.rel = "stylesheet";
    element.type = "text/css";
    element.href = url;
  } else {
    var element = document.createElement("script");
    if (id) element.id = id;
    element.language = "javascript";
    element.type = "text/javascript";
    element.src = url;
  }

  if (callback) {
    element.onload = element.onreadystatechange = (function () {
      var callbackCalled = false;
      return function () {
        if (callbackCalled) return;
        if ((!this.readyState || this.readyState == "loaded" || this.readyState == "complete")) {
          callbackCalled = true;
          callback();
        }
      };
    })();
  }

  document.head.appendChild(element);
}
/**
 *
 * @param urls
 * @param callback
 * @param areStylesheets
 */
function ensureResources(urls, callback, areStylesheets) {
  var urlLoaded = (function () {
    var scriptsToBeLoaded = urls.length;
    return function () { if ((--scriptsToBeLoaded) == 0) callback(); };
  })();

  for (var i = 0; i < urls.length; i++) {
    insertResource(null, urls[i], urlLoaded, areStylesheets);
  }
}
/**
 * Insert the file.
 */
if (!document.getElementById('MessageGuard')) {
  insertResource('MessageGuard', '@Server/js/bookmarklet.js');
}
