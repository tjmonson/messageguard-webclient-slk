/* global performance: true */

var Controller     = require('../../src/js/frontend/generic/controller'),
    ControllerBase = require('../../src/js/frontend/controller-base'),
    urls           = require('../../src/js/frontend/urls');

/**
 * Performance API polyfill.
 */
(function () {
  if ("performance" in window == false) {
    window.performance = {};
  }
  Date.now = (Date.now || function () {  // thanks IE8
    return new Date().getTime();
  });
  if ("now" in window.performance == false) {
    var nowOffset = Date.now();
    if (performance.timing && performance.timing.navigationStart) {
      nowOffset = performance.timing.navigationStart
    }
    window.performance.now = function now() {
      return Date.now() - nowOffset;
    }
  }

})();

/**
 * @typedef {Object} Task
 * @property {string} type Type of element to create.
 * @property {string} content Content of the task.
 * @property {Number} count Number of elements to create.
 */

/**
 * Tasks to run.
 * @type {{before: Task[], after: Task[]}}
 */
var tasks = {
  before: [
    {type: 'textarea', content: '', count: 5},
    {type: 'contentEditable', content: '', count: 5},
    {type: 'div', content: '|mg|dGVzdA==|', count: 5},
  ],
  after:  [
    {type: 'textarea', content: '', count: 5},
    {type: 'contentEditable', content: '', count: 5},
    {type: 'div', content: '|mg|dGVzdA==|', count: 5},
  ]
};

/**
 * Total number of before writes.
 * @type {number}
 */
var beforeTaskCount = 0, i;
for (i = 0; i < tasks.before.length; i++)
  beforeTaskCount += tasks.before[i].count;

/**
 * Total number of after writes.
 * @type {number}
 */
var afterTaskCount = 0;
for (i = 0; i < tasks.after.length; i++)
  afterTaskCount += tasks.after[i].count;

/**
 * Log arguments to screen.
 */
function log() {
  // console.log.apply(console, arguments);
  var elem = document.getElementById('results');
  var text = elem.textContent + "\n" + Array.prototype.slice.call(arguments).join(', ');
  elem.textContent = text;
}

/**
 * Create a field of the given type and with the given content.
 * @param {string} type Type of the field to create.
 * @param {string} content Content to insert into the field.
 */
function writeField(type, content) {
  var element;
  switch (type) {
    case 'textarea':
      element = document.createElement('textarea');
      element.value = content;
      break;

    case 'contentEditable':
      element = document.createElement('div');
      element.contentEditable = 'true';
      element.innerHTML = content;
      break;

    case 'div':
      element = document.createElement('div');
      element.innerHTML = content;
      break;
  }
  if (element) {
    document.body.appendChild(element);
  }
}

/**
 * Run the given set of write tasks.
 * @param {Task[]} tasks Set of tasks to run.
 */
function runTasks(tasks) {
  for (var i = 0; i < tasks.length; i++) {
    var task = tasks[i];
    for (var j = 0; j < task.count; j++) {
      writeField(task.type, task.content);
    }
  }
}


/**
 * @typedef {Object} Timing
 * @property {string} name Name of the timing.
 * @property {Number} start Start time for some event.
 * @property {Number} content End time for some event.
 */

/**
 * Timings for the various things we want to test.
 * @type {Object}
 * @prop {Timing} initialBatch Adding the initial set of items without a controller.
 * @prop {Timing} setupController Setting up the controller.
 * @prop {Timing} initialBatchOverlays Time to load overlays for the initial set of items.
 * @prop {Timing} secondBatch Adding the second set of items.
 * @prop {Timing} secondBatchOverlays Time to load overlays for the second set of items.
 */
var timings = {
  initialBatch:         {name: 'initialBatch', start: 0, end: 0},
  setupController:      {name: 'setupController', start: 0, end: 0},
  initialBatchOverlays: {name: 'initialBatchOverlays', start: 0, end: 0},
  secondBatch:          {name: 'secondBatch', start: 0, end: 0},
  secondBatchOverlays:  {name: 'secondBatchOverlays', start: 0, end: 0}
};

/**
 * Tracks the current callback.
 * @type {Function}
 */
var currentCallback = null;

/**
 * Instrument the controller so we can known when overlays have been setup. Also creates the controller.
 */
function setupController() {
  urls.setup(false);

  var originalFunction = ControllerBase.prototype.updateManagers;
  ControllerBase.prototype.updateManagers = function () {
    originalFunction.apply(this, arguments);
    currentCallback();
  };

  var controller = new Controller();
  controller.start();
};

/**
 * Called when the overlays for the first batch are finished loading.
 */
function overlaysForInitialBatchLoaded() {
  timings.initialBatchOverlays.end = performance.now();
  printResult(timings.initialBatchOverlays);

  currentCallback = overlaysForSecondBatchLoaded;
  timings.secondBatch.start = timings.secondBatchOverlays.start = performance.now();
  runTasks(tasks.after);
  timings.secondBatch.end = performance.now();
  printResult(timings.secondBatch);

  if (afterTaskCount === 0) {
    overlaysForSecondBatchLoaded();
  }
}

/**
 * Called when the overlays for the second batch are finished loading.
 */
function overlaysForSecondBatchLoaded() {
  timings.secondBatchOverlays.end = performance.now();
  printResult(timings.secondBatchOverlays);

  log('Finished test');
};


/**
 * Run the test.
 */
function runTest() {
  log('Starting test');
  var start = performance.now();

  // Time adding elements to the page.
  timings.initialBatch.start = performance.now();
  runTasks(tasks.before);
  timings.initialBatch.end = performance.now();
  printResult(timings.initialBatch);

  // Time how long it takes to setup the controller and overlay the first batch of items.
  currentCallback = overlaysForInitialBatchLoaded;
  timings.setupController.start = timings.initialBatchOverlays.start = performance.now();
  setupController();
  timings.setupController.end = performance.now();
  printResult(timings.setupController);

  if (beforeTaskCount === 0) {
    overlaysForInitialBatchLoaded();
  }
}

/**
 * Print the results of our testing.
 * @param {Timing} timing Timing to print.
 */
function printResult(timing) {
  log(timing.name, timing.end - timing.start);
}

// Watch the start button. When click run the test.
document.getElementById('start').addEventListener('click', function () {
  runTest();
});